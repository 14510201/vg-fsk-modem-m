// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemConnectionOwner.hpp                            :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <boost/interprocess/shared_memory_object.hpp>
#include "FskModemApp.hpp"
#include "FskModemIpcQueues.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem connection, owner of the queues and shared memory. </summary>
	class FskModemConnectionOwner : FskModemIpcQueues
	{
	private:

		/// <summary>	(Immutable) The connection version. </summary>
		const std::string								Version = "2.00.00";
		

		/// <summary>	Shared memmory remover. </summary>
		struct ShmRemover
		{
			ShmRemover(const char* shmName) : name(shmName) { boost::interprocess::shared_memory_object::remove(name); }
			~ShmRemover() { boost::interprocess::shared_memory_object::remove(name); }
			const char* name;
		};

		FskModemApp&									app;
		ShmRemover*										pShmRemover;
		boost::interprocess::mapped_region*				pShmMappedRegion;

	public:
		FskModemConnectionOwner(void) = delete;
		FskModemConnectionOwner(FskModemApp& app);
		~FskModemConnectionOwner(void);

		int							Create(int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity);
		int							ReceiveDemodIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait = true);
		int							SendDemodPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait = true);
		void						SetVersion(std::string version);
		int							ReceiveCommand(user_api::SvcFskModemCommand_t& cmd, bool wait = true);
		int  						SendResponse(user_api::SvcFskModemResponse_t& resp, bool wait = true);
		int							ReceiveModPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait = true);
		int							SendModIqFrame(void* pFrame, uint16_t lengthX8, bool wait = true);

	protected:
		void						SetConnectionVersion(std::string version);
	};
}