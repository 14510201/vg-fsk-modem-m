// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemIpcQueues.hpp                                  :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <stdint.h>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include "SvcFskModemCmdRespApi.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem IPC queues. </summary>
	class FskModemIpcQueues
	{
	protected:
		
		/// <summary>	(Immutable) General constants. </summary>
		static const int									MaxModDemodQEntrySizeX8 = 640;
		const int											MaxModDemodQCapacity = 2048;
		const int											MaxCommandQCapacity = 64;
		const int											MaxResponseQCapacity = 64;
		const int											WaitTimeoutMs = 300;


		/// <summary>	Queues impemented. </summary>
		enum QueueId_t
		{
			CommandsToModem = 0,
			ResponsesFromModem,

			// group the mod/demod Qs together
			DemodIqFramesToModem,
			DemodPktsFromModem,
			ModPktsToModem,
			ModIqFramesFromModem,

			// must be last
			QueueCount
		};
		const QueueId_t										FirstModDemodQId = QueueId_t::DemodIqFramesToModem;
		const QueueId_t										LastModDemodQId = QueueId_t::ModIqFramesFromModem;


	private:
		/// <summary>	(Immutable) General constants. </summary>
		static const int									MaxStringLengthX8 = 15;
		static const uint32_t								SyncWord = 0x78125634;


		/// <summary>	Shared memory queue entry. </summary>
		struct SharedMemoryQueueEntry_t
		{
			SharedMemoryQueueEntry_t(void) 
			{
				tag = 0;
			}
			uint16_t										tag;
		};


		/// <summary>	Shared memory queue entry : Command. </summary>
		struct SharedMemoryQueueCommandEntry_t : SharedMemoryQueueEntry_t
		{
			user_api::SvcFskModemCommand_t					cmd;
		};


		/// <summary>	Shared memory queue entry : Response. </summary>
		struct SharedMemoryQueueResponseEntry_t : SharedMemoryQueueEntry_t
		{
			user_api::SvcFskModemResponse_t					resp;
		};


		/// <summary>	Shared memory queue entry : ModDemod. </summary>
		struct SharedMemoryQueueModDemodEntry_t : SharedMemoryQueueEntry_t
		{
			SharedMemoryQueueModDemodEntry_t(void)
			{
				lengthX8 = 0;
			}
			uint16_t										lengthX8;
			uint8_t											data[MaxModDemodQEntrySizeX8];
		};


		/// <summary>	Shared memory queue. </summary>
		struct SharedMemoryQueue_t
		{
			SharedMemoryQueue_t(int capacity) : semAvailable(0), semFreespace(capacity), capacity(capacity)
			{	
				wrIndex = 0; 
				rdIndex = 0;
			}
			boost::interprocess::interprocess_semaphore		semAvailable;
			boost::interprocess::interprocess_semaphore		semFreespace;
			const uint16_t									capacity;
			uint16_t										wrIndex;
			uint16_t										rdIndex;
			uint32_t										offsX8EntryArray;
		};


		/// <summary>	Shared memory block. This is the format of the shared memory block. </summary>
		struct SharedMemoryBlock_t
		{
			SharedMemoryBlock_t(int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity) :
				syncWord(SyncWord),
				qCommandsToModem(qCommandCapacity),
				qResponsesFromModem(qResponseCapacity),
				qDemodIqFramesToModem(qIqFrameCapacity),
				qDemodPktsFromModem(qPktCapacity),
				qModPktsToModem(qPktCapacity),
				qModIqFramesFromModem(qIqFrameCapacity)
			{
				version[0] = '\0';
				connectionVersion[0] = '\0';
			}
			const uint32_t									syncWord;
			char											version[MaxStringLengthX8 + 1];
			char											connectionVersion[MaxStringLengthX8 + 1];
			uint32_t										qOffsX8Table[QueueId_t::QueueCount];
			SharedMemoryQueue_t								qCommandsToModem;
			SharedMemoryQueue_t								qResponsesFromModem;
			SharedMemoryQueue_t								qDemodIqFramesToModem;
			SharedMemoryQueue_t								qDemodPktsFromModem;
			SharedMemoryQueue_t								qModPktsToModem;
			SharedMemoryQueue_t								qModIqFramesFromModem;
		};


		/// <summary>	Localized pointers. </summary>
		SharedMemoryQueue_t*								qPtrTable[QueueId_t::QueueCount];
		SharedMemoryQueueEntry_t*							qEntryArrayPtrTable[QueueId_t::QueueCount];


		// ctor/dtor protected to prevent outsite instantiation of this class,
		// we want this to be derived only.
	protected:
		FskModemIpcQueues(void) {}
		~FskModemIpcQueues(void) {}

		SharedMemoryBlock_t*								pShmBlock;


		int						GetBlockSizeX8(int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity);
		int 					Create(void* pSharedMem, int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity);
		int						Attach(void* pSharedMem);
		std::string				GetVersion(void);
		void					SetVersion(std::string& version);
		std::string				GetConnectionVersion(void);
		void					SetConnectionVersion(std::string& version);
		int						AddCommandEntry(user_api::SvcFskModemCommand_t& cmd, bool wait = true);
		int						GetNextCommandEntry(user_api::SvcFskModemCommand_t& cmd, bool wait = true);
		int						AddResponseEntry(user_api::SvcFskModemResponse_t& resp, bool wait = true);
		int						GetNextResponseEntry(user_api::SvcFskModemResponse_t& resp, bool wait = true);
		int						AddModDemodEntry(QueueId_t qId, void* pData, uint16_t lengthX8, bool wait = true);
		int						AddModDemodEntry(QueueId_t qId, void* pData, uint16_t lengthX8, uint16_t tag, bool wait = true);
		int						GetNextModDemodEntry(QueueId_t qId, void* pData, uint16_t maxLengthX8, bool wait = true);
		int						GetNextModDemodEntry(QueueId_t qId, void* pData, uint16_t maxLengthX8, uint16_t* pTag, bool wait = true);
		inline int				CheckQFreespace(QueueId_t qId, bool wait = true);
		inline int				CheckQAvailable(QueueId_t qId, bool wait = true);
	};
}