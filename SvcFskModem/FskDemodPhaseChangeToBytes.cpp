// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskDemodPhaseChangeToBytes.cpp                         :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Processes phase change data to align to the preamble, find the sync 
//      word, and frame the bytes encoded.
//      
//      This looks for PeakTroughDetectThresh preamble peak-trough patterns
//      before moving on.
//
// -----------------------------------------------------------------------------

#include <cmath>
#include <string>
#include "FskDemodPhaseChangeToBytes.hpp"

namespace svc_fsk_modem
{
	using namespace std;

	/// <summary>	Constructor. </summary>
	///
	/// <param name="diag">			   	[in] The diagnostic exporter. </param>
	/// <param name="chanNumber">	   	The channel number. </param>
	/// <param name="bitTimeInSamples">	The bit time in samples. </param>
	/// <param name="syncWord">		   	The synchronize word. </param>
	FskDemodPhaseChangeToBytes::FskDemodPhaseChangeToBytes(DiagnosticsDemod& diag, int chanNumber, float bitTimeInSamples,
		uint32_t syncWord) : diag(diag), chanNumber(chanNumber), bitTimeInSamples(bitTimeInSamples), syncWord(syncWord)
	{
		// determine min and max sample spacing between peaks and troughs. This is based on the bit
		// time in samples (which may be fractional).
		ptSpacingMin = (int)floor(bitTimeInSamples);
		ptSpacingMax = (int)ceil(bitTimeInSamples);

		// On top of this, we may not have a sample at the exact peak or trough, adding a +/- 0.5 
		// sample error on each end. This means our range is one less on the min and one more on
		// the max.
		ptSpacingMin--;
		ptSpacingMax++;

		// reset to init
		Reset();
	}


	/// <summary>	Process phase data. </summary>
	///
	/// <param name="byteAccum"> 	[in,out] Vector to accumulate the bytes. </param>
	/// <param name="pPhaseData">	[in] Ptr to the phase data array. </param>
	/// <param name="length">	 	Length of the phase data array. </param>
	void FskDemodPhaseChangeToBytes::Process(std::vector<uint8_t>* pByteAccum, float* pPhaseData, int length)
	{
		bool					captureSampleFlag;

		// run through the data
		for (int idx = 0; idx < length; idx++)
		{
			captureSampleFlag = false;
			switch (state)
			{
			case SeekPreamblePeak:

				// when we find a peak log it and see if the spacing from 
				// the last through is ok. If not reset the preamble search.
				// Move on to look for next trough.
				if (pPhaseData[idx] < lastPhaseChange)
				{
					if (!PeakTroughSampleSpacingOk(lastTroughLoc))
					{
						preambleCycles = 0;
						peakAvgAccum = 0;
						troughAvgAccum = 0;
					}
					peakAvgAccum += pPhaseData[idx];
					lastPeakLoc = curSampleLoc;
					gapEndLoc = curSampleLoc + PeakTroughPostGapSampleCount;
					state = SeekPreamblePostPeakGap;
					diag.ExportIqSampleMarker((uint16_t)curSampleLoc, chanNumber);
				}
				lastPhaseChange = pPhaseData[idx];
				break;


			case SeekPreamblePostPeakGap:
				if (curSampleLoc == gapEndLoc)
				{
					state = SeekPreambleTrough;
				}
				break;


			case SeekPreambleTrough:

				// when we find a trough log it and see if the spacing from 
				// the last peak is ok. If not reset the preamble search.
				// If we found enogh peak/trough patterns, move on to align to
				// the sync word. Otherwise move on to look for next peak.
				if (pPhaseData[idx] > lastPhaseChange)
				{
					if (!PeakTroughSampleSpacingOk(lastPeakLoc))
					{
						preambleCycles = 0;
						peakAvgAccum = 0;
						troughAvgAccum = 0;
					}
					else
					{
						preambleCycles++;
						troughAvgAccum += pPhaseData[idx];
					}
					lastTroughLoc = curSampleLoc;
					gapEndLoc = curSampleLoc + PeakTroughPostGapSampleCount;

					// done?
					if (preambleCycles == PeakTroughDetectThresh)
					{
						// calculate the average between peak/trough
						ptAverage = (peakAvgAccum + troughAvgAccum) / (2 * PeakTroughDetectThresh);
						diag.ExportIqPdDecisionThreshold(ptAverage, chanNumber);

						// set the next data sample point
						nextDataSamplePoint = curSampleLoc + bitTimeInSamples;

						// move on to align sync word
						bitAccum = 0;
						state = AlignSync;
					}
					else
					{
						state = SeekPreamblePostTroughGap;
					}
					diag.ExportIqSampleMarker((uint16_t)curSampleLoc, chanNumber);
				}
				lastPhaseChange = pPhaseData[idx];
				break;


			case SeekPreamblePostTroughGap:
				if (curSampleLoc == gapEndLoc)
				{
					state = SeekPreamblePeak;
				}
				break;

			default:
				if (curSampleLoc == floor(nextDataSamplePoint))
				{
					captureSampleFlag = true;
					nextDataSamplePoint += bitTimeInSamples;
					diag.ExportIqSampleMarker((uint16_t)curSampleLoc, chanNumber);
				}
				break;
			}


			// capture bits
			if (captureSampleFlag)
			{
				// accumulate bits
				bitAccum <<= 1;
				if (pPhaseData[idx] > ptAverage)
				{
					bitAccum |= 1;
				}

				// align and form bytes
				switch (state)
				{
				case AlignSync:

					// look for the sync word and it's bit inverse
					bitsRemaining = 8;
					bitAccum &= 0xffffff;
					if (bitAccum == syncWord)
					{
						bitAccum = 0;
						dataInvesionFlag = false;
						state = PackBytes;
					}
					else if (bitAccum == ~syncWord)
					{
						bitAccum = 0;
						dataInvesionFlag = true;
						state = PackBytes;
					}
					break;


				case PackBytes:

					// pack and store
					if (--bitsRemaining == 0)
					{
						pByteAccum->push_back((uint8_t)(bitAccum & 0xff));
						bitsRemaining = 8;
					}
					break;
				}
			}

			// inc cur sample loc
			curSampleLoc++;
		}
	}


	/// <summary>	Determine if the Peak/Trough sample spacing ok. </summary>
	///
	/// <param name="lastLoc">	The last location. </param>
	///
	/// <returns>	True if spacing is ok, false if not. </returns>
	bool FskDemodPhaseChangeToBytes::PeakTroughSampleSpacingOk(int lastLoc)
	{
		if (((curSampleLoc - lastLoc) >= ptSpacingMin) && ((curSampleLoc - lastLoc) <= ptSpacingMax))
		{
			return true;
		}
		return false;
	}


	/// <summary>	Resets this object. </summary>
	void FskDemodPhaseChangeToBytes::Reset(void)
	{
		state = SeekPreamblePeak;
		curSampleLoc = 0;
		lastPhaseChange = -100;
		peakAvgAccum = 0;
		lastPeakLoc = 0;
		troughAvgAccum = 0;
		lastTroughLoc = 0;
		preambleCycles = 0;
	}
}