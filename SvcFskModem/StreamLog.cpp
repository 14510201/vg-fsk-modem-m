// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : StreamLog.cpp                                          :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides basic stream based logging. Log can be to stdout or to 
//      a file.
//
// -----------------------------------------------------------------------------

//#include <chrono>
#include <ctime>
#include <cmath>
#include <string>
#include <boost/format.hpp>
#include "StringUtilities.hpp"
#include "StreamLog.hpp"

using std::string;
using std::flush;
using std::ostream;

namespace svc_fsk_modem
{
    namespace log
    {

        /// <summary>	Static mutex instance. </summary>
        std::mutex   StreamLog::Locked_t::mutAccess;


        /// <summary>	Default constructor. </summary>
        StreamLog::StreamLog()
            : fileStream()
            , outStream(std::cout)
            , lastTimestamp(GetClockTickUS())
            , addTimestamp(false)
            , lastLineEndedWithLF(true)
        {}


        /// <summary>	Constructor. Stream logging. </summary>
        ///
        /// <param name="stream">	[in,out] The stream. </param>
        StreamLog::StreamLog(ostream& stream)
            : fileStream()
            , outStream(stream)
            , lastTimestamp(GetClockTickUS())
            , addTimestamp(false)
            , lastLineEndedWithLF(true)
        {}


        /// <summary>	Constructor. File logging. </summary>
        ///
        /// <exception cref="std::runtime_error">	Raised when a unable to open the file. </exception>
        ///
        /// <param name="filename">	Filename of the file. </param>
        StreamLog::StreamLog(string filename)
            : fileStream(filename)
            , outStream(this->fileStream)
            , lastTimestamp(GetClockTickUS())
            , addTimestamp(false)
            , lastLineEndedWithLF(true)
        {
            if (!outStream.get())
            {
                throw std::runtime_error(str(boost::format("Unable to open file '%1%' for logging.") % filename));
            }
        }


        /// <summary>	Assigns the given file for logging. </summary>
        ///
        /// <param name="filename">	Filename of the file. </param>
        void StreamLog::Assign(const std::string filename)
        {
            if (fileStream.is_open())
            {
                fileStream.close();
            }
            fileStream.open(filename);
            outStream = fileStream;
        }


        /// <summary>	Assigns the given stream for logging. </summary>
        ///
        /// <param name="stream">	[in,out] The stream. </param>
        void StreamLog::Assign(std::ostream& stream)
        {
            if (fileStream.is_open())
            {
                fileStream.close();
            }
            outStream = stream;
        }


        /// <summary>	Control timestamp. </summary>
        ///
        /// <param name="add">	True to add. </param>
        ///
        /// <returns>	A reference to a StreamLog. </returns>
        StreamLog& StreamLog::AddTimestamp(bool add)
        {
            addTimestamp = add;
            return (*this);
        }


        /// <summary>	Logs without timestamp. </summary>
        ///
        /// <param name="text">   	The text to log. </param>
        /// <param name="doFlush">	Flush log? </param>
        void StreamLog::Log(const string& text, const LogFlush_t doFlush) const
        {
            if (doFlush == LogFlush_t::Yes)
            {
                outStream.get() << text << flush;
            }
            else
            {
                outStream.get() << text;
            }
        }


        /// <summary>	Logs with timestamp. </summary>
        ///
        /// <param name="text">   	The text to log. </param>
        /// <param name="doFlush">	Flush log?. </param>
        void StreamLog::LogWithTimestamp(const string& text, const LogFlush_t doFlush)
        {
            if (text.length() == 0) return;
            uint64_t timestamp;
            timestamp = GetClockTickUS();
            int64_t Delta = timestamp - lastTimestamp;
            lastTimestamp = timestamp;
            // Split text into lines based on LF.
            auto lines = string_utilities::SplitString::split(text, '\n');
            bool originalTextEndsInLF = (text[text.length() - 1] == '\n');
            bool showTimestamp = lastLineEndedWithLF;
            lastLineEndedWithLF = originalTextEndsInLF;
            string textToAdd;
            size_t index = 0;
            for (auto&& line : lines)
            {
                if (showTimestamp)
                {
                    showTimestamp = false;
                    textToAdd = str(boost::format("[%1%] %2%") % DeltaStr(Delta) % line);
                }
                else
                {
                    if (index == 0)
                    {
                        textToAdd = line;
                    }
                    else
                    {
                        textToAdd = str(boost::format("                 %1%") % line);
                    }
                }
                if ((index < (lines.size() - 1)) || originalTextEndsInLF)
                {
                    textToAdd = textToAdd + '\n';
                }
                outStream.get() << textToAdd;
                index++;
            }
            if (doFlush == LogFlush_t::Yes)
            {
                outStream.get() << flush;
            }
        }


        /// <summary>	Create a delta time string. </summary>
        ///
        /// <param name="delta">	The delta. </param>
        ///
        /// <returns>	A std::string. </returns>
        std::string StreamLog::DeltaStr(const int64_t delta) const
        {
            if (delta < 1000)
            {
                return (str(boost::format("%|14d|") % delta));
            }
            int us = delta % 1000;
            int ms = (delta / 1000) % 1000;
            if (delta < 1000000)
            {
                return (str(boost::format("%|10d|,%|03d|") % ms % us));
            }
            int s = (delta / 1000000);
            return (str(boost::format("%|6d|,%|03d|,%|03d|") % s % ms % us));
        }


        /// <summary>	Create time string. </summary>
        ///
        /// <param name="timestamp">	The timestamp value. </param>
        ///
        /// <returns>	A std::string. </returns>
        std::string StreamLog::TimeStr(const uint64_t timestamp) const
        {
            double ts_secs;
            double ts_remainder;
            ts_remainder = modf((timestamp / 1000000.0), &ts_secs);
            int hours = ts_secs / (60 * 60);
            int mins = (ts_secs - (hours * 60 * 60)) / 60;
            double secs = (ts_secs - (hours * 60 * 60) - (mins * 60)) + ts_remainder;
            return (str(boost::format("%|02|:%|02|:%|09.6f|") % hours % mins % secs));
        }


        /// <summary>
        ///     This overload allows us to handle int's as the input.
        /// </summary>
        ///
        /// <param name="log">          The log.</param>
        /// <param name="input">        Integer.</param>
        ///
        /// <returns> Reference to ourself </returns>
        StreamLog& operator<<(StreamLog& log, const int input)
        {
            auto convInput = str(boost::format("%1%") % input).c_str();
            if (log.AddTimestamp())
            {
                log.LogWithTimestamp(convInput);
            }
            else
            {
                log.Log(convInput);
            }
            return log;
        }


        /// <summary>
        ///     This overload allows us to handle strings as the input.
        /// </summary>
        ///
        /// <param name="log">          The log.</param>
        /// <param name="input">        String instance.</param>
        ///
        /// <returns> Reference to ourself </returns>
        StreamLog& operator<<(StreamLog& log, const std::string input)
        {
            if (log.AddTimestamp())
            {
                log.LogWithTimestamp(input);
            }
            else
            {
                log.Log(input);
            }
            return log;
        }


        /// <summary>
        ///     This overload allows us to handle outstreams as the input.
        /// </summary>
        ///
        /// <param name="log">          The log.</param>
        /// <param name="input">        Outstream instance.</param>
        ///
        /// <returns> Reference to ourself </returns>
        StreamLog& operator<<(StreamLog& log, const std::ostream* pInput)
        {
            std::stringstream ss;
            ss << pInput->rdbuf();
            if (log.AddTimestamp())
            {
                log.LogWithTimestamp(ss.str());
            }
            else
            {
                log.Log(ss.str());
            }
            return log;
        }


        /// <summary>
        ///     Stream manipulators. This overload allows us to process stream manipulators such as endl
        ///     and flush.  They show up as funciton pointers which we need to call passing in our stream
        ///     object.
        /// </summary>
        ///
        /// <param name="log">  The log.</param>
        /// <param name="pf">   [in,out] Pointer to stream manipulator function.</param>
        ///
        /// <returns> Reference to ourself </returns>
        StreamLog& operator<<(StreamLog& log, std::ostream& (*manipulator)(std::ostream&))
        {
            /// <summary>	Constructor. </summary>
            ///
            /// <param name="parameter1">	The first parameter. </param>
            manipulator(log.outStream.get());
            if (manipulator == (std::basic_ostream<char>&(*)(std::basic_ostream<char>&)) & std::endl)
            {
                log.lastLineEndedWithLF = true;
            }
            return log;
        }


        /// <summary>   Gets clock tick in us. </summary>
        ///
        /// <returns>   Ticks in us </returns>
        uint64_t StreamLog::GetClockTickUS(void)
        {
            struct timespec now;
            clock_gettime(CLOCK_MONOTONIC, &now);

            // Calculate microseconds
            return (((uint64_t) now.tv_sec) * 1000000) + (((uint64_t) now.tv_nsec) / 1000);
        }
    }
} 