// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskCrc.hpp                                             :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <cstdint>
namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: FskCrc, either whiten or de-whiten data.
	/// </summary>
	class FskCrc
	{
	protected:


		/// <summary>	(Immutable) General constants. </summary>
		static const uint16_t		Seed;
		static const uint16_t		Table[];

	public:
		FskCrc(void) {}
		~FskCrc(void) {}

		bool						Check(uint8_t* pData, int lengthX8, uint16_t compareCrc);
		uint16_t					Calculate(uint8_t* pData, int lengthX8);
		int							CrcSizeBits(void) { return 16; }
	};
}