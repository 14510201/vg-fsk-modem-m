// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpEvtHost.hpp                                  :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file.
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemOp.hpp"
#include "FskModemEvents.hpp"
#include "FskModemEventChannel.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem base event-host op functionality. </summary>
	class FskModemOpEvtHost : public FskModemOp

	{
	public:
		FskModemOpEvtHost(void) = delete;
		FskModemOpEvtHost(FskModemApp& app, FskModemConnectionOwner& qConnect)
			: FskModemOp(app, qConnect) {}
		~FskModemOpEvtHost(void);

	protected:

		/// <summary>	Defines an alias representing the event channel identifier. </summary>
		typedef int EventChannelId_t;

		FskModemEventChannel*				CreateEventChannel(EventChannelId_t evtChanId);
		void								PostEvent(EventChannelId_t evtChanId, events::Event_t* pEvt);
		bool								NextEvent(EventChannelId_t evtChanId, events::Event_t*& pEvt, bool wait = true);
		bool								NextEvent(EventChannelId_t evtChanId, events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait = true);

	private:
		std::map<EventChannelId_t, FskModemEventChannel*>		evtChannelMap;
	};
}