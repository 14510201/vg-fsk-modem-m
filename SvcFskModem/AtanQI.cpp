// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : AtanQI.cpp			                                   :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Calculate Atan(Q/I) using a lookup table.
//
// -----------------------------------------------------------------------------

#include <cmath>
#include "AtanQI.hpp"

namespace svc_fsk_modem
{
	using namespace std;


	/// <summary>	(Immutable) Atan(Q/I) table. </summary>
	#include "AtanQITable.h"
	const int			AtanQI::MaxI = ATANQITABLE_I_MAX;
	const int			AtanQI::MaxQ = ATANQITABLE_Q_MAX;


	/// <summary>	Return Atan(Q/I). </summary>
	///
	/// <param name="iq">	The iq pair. </param>
	///
	/// <returns>	Atan(Q/I). </returns>
	inline float AtanQI::AtanLookup(IqPair_t iq)
	{
		// lookup in the table based on I and Q (enforce max I/Q indicies)
		uint16_t iIndex = (uint16_t)round(abs(iq.i * S3p6Multiplier));
		uint16_t qIndex = (uint16_t)round(abs(iq.q * S3p6Multiplier));
		iIndex = (iIndex > AtanQI::MaxI-1) ? AtanQI::MaxI-1 : iIndex;
		qIndex = (qIndex > AtanQI::MaxQ-1) ? AtanQI::MaxQ-1 : qIndex;
		float lkupValue = atanQITable[iIndex][qIndex];

		// If the signs of i and q differ, the final result is negative.
		return ((iq.i < 0) != (iq.q < 0)) ? -1 * lkupValue : lkupValue;
	}


	/// <summary>	Get Atan(Q/I). </summary>
	///
	/// <param name="iq">	The iq pair. </param>
	///
	/// <returns>	Atan(Q/I). </returns>
	float AtanQI::Atan(IqPair_t iq)
	{
		return AtanLookup(iq);
	}


	/// <summary>	Get Atan(Q/I) for a series of IQ data. </summary>
	///
	/// <param name="iq">	The iq pair. </param>
	void AtanQI::Atan(IqPair_t* pIq, float* pBufr, int length)
	{
		for (int idx = 0; idx < length; idx++)
		{
			pBufr[idx] = AtanLookup(pIq[idx]);
		}
	}
}