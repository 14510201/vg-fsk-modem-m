// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemCore.hpp                                       :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <boost/thread.hpp>
#include "FskModemConnectionOwner.hpp"
#include "FskModemApp.hpp"
#include "FskModemOp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem core functionality. </summary>
	class FskModemCore
	{
	private:

		struct OpThread_t
		{
			OpThread_t(FskModemOp* pOp) : pOp(pOp)
			{
				pThread = new boost::thread(&FskModemOp::RunAsThread, pOp);
			}
			~OpThread_t()
			{
				delete pOp;
				delete pThread;
			}

			FskModemOp*					pOp;
			boost::thread*				pThread;
		};

		FskModemApp&					app;

		boost::condition_variable		condShutdown;
		boost::mutex					mutShutdown;
		bool							flagShutdown;
		FskModemConnectionOwner			qConnect;
		std::vector<OpThread_t*>		opThreads;

	public:
		FskModemCore(void) = delete;
		FskModemCore(FskModemApp& app);
		~FskModemCore(void);

		void							Run(void);
		void							WaitShutdown(void);
		void							Shutdown(void);
	};
}