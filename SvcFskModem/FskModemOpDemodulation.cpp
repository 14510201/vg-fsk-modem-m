// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpDemodulation.cpp                             :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK demodulation functionality.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <boost/format.hpp>
#include "FskModemOpDemodulation.hpp"
#include "DiagnosticsDemod.hpp"
#include "Utilities.hpp"


namespace svc_fsk_modem
{
	using namespace std;

	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">	[in,out] The application. </param>
	FskModemOpDemodulation::FskModemOpDemodulation(FskModemApp& app, FskModemConnectionOwner& qConnect,
		FskModemEventChannel* pEvtChan) : FskModemOpEvtNode(app, qConnect, pEvtChan)
	{
		diagGlobalEnable = false;
	}


	/// <summary>	Destructor. </summary>
	FskModemOpDemodulation::~FskModemOpDemodulation(void)
	{
		// release demod chans
		ReleaseChannels();

		// delete buffers
		if (pIqEncFrameBufr) delete pIqEncFrameBufr;
		if (pIqBufr) delete pIqBufr;
	}


	/// <summary>	The demodulation operation. </summary>
	void FskModemOpDemodulation::Run(void)
	{
		int										lengthBytes;
		int										maxSampleCount;
		int										sampleCount;
		FskDemodChannel::PacketDesc_t*			pPktDesc;


		// log start
		app.log.LockedLog() << "FSK Demodulation started" << endl;

		// create our buffers
		pIqEncFrameBufr = new uint8_t[EncIqBufferSizeBytes];
		maxSampleCount = (EncIqBufferSizeBytes * 8) / 20;
		pIqBufr = new IqPair_t[maxSampleCount];

		// create dianostic exporter
		DiagnosticsDemod diagnostics("/tmp/diagDemod.xs");

		// set the initial configuration
		UpdateConfiguration(diagnostics);

		// set running and enter run loop
		running = true;
		events::Event_t* pEvtPending = nullptr;
		while (!stop)
		{	
			// check for events, do not wait. Handle all.
			while (NextEvent(pEvtPending, false))
			{
				switch (pEvtPending->type)
				{
				case events::EventType_t::ConfigurationChanged:
					HandleEvtConfigurationChanged((events::EventConfigurationChanged_t*)pEvtPending, diagnostics);
					break;

				case events::EventType_t::DemodDiagnosticsUpdate:
					HandleEvtDiagnosticsUpdate((events::EventDemodDiagnosticsUpdate_t*)pEvtPending, diagnostics);
					break;

				case events::EventType_t::StatusRequest:
					HandleEvtStatusRequest((events::EventStatusRequest_t*)pEvtPending, diagnostics);
					break;
				}

				// destroy event
				delete pEvtPending;
				pEvtPending = nullptr;
			}

			// receive IQ frames and process
			if ((lengthBytes = qConnect.ReceiveDemodIqFrame(pIqEncFrameBufr, EncIqBufferSizeBytes)) >= 0)
			{
				// decode
				sampleCount = DecodeIqFrame((uint32_t*)pIqEncFrameBufr, lengthBytes / sizeof(uint32_t), pIqBufr);

				// export for remote diagnostics
				diagnostics.ExportIqOriginal(pIqBufr, sampleCount);

				// demod the channels
				for (auto pChDemod : chanDemodMap)
				{
					// demod chan
					pPktDesc = pChDemod.second->Demodulate(pIqBufr, sampleCount);
					
					// if we got a packet, send
					if (pPktDesc)
					{
						if(qConnect.SendDemodPacket(pPktDesc->pPkt, pPktDesc->pktLengthX8, pPktDesc->chanNumber, false))
						{
							utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.packetsDropped);
						}
					}
				}
			}
		}

		// close diagnostics
		diagnostics.Close();

		// no longer running
		running = false;
	}


	/// <summary>	Event Handler: configuration changed. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpDemodulation::HandleEvtConfigurationChanged(events::EventConfigurationChanged_t* pEvt, DiagnosticsDemod& diagnostics)
	{
		// update our config to the latest
		UpdateConfiguration(diagnostics);

		// post our confirmation
		PostEvent(new events::EventDemodConfigured_t());
	}


	/// <summary>	Event Handler: diagnostics update. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpDemodulation::HandleEvtDiagnosticsUpdate(events::EventDemodDiagnosticsUpdate_t* pEvt, DiagnosticsDemod& diagnostics)
	{
		// update our diagnotics enable(s).
		diagGlobalEnable = pEvt->diags.enable;
		
		//  apply if changed
		if (diagGlobalEnable ^ diagnostics.Started())
		{
			if (diagGlobalEnable)
			{
				diagnostics.Start();
				app.log.LockedLog() << "FSK-Demod diagnostics started" << endl;
			}
			else
			{
				diagnostics.Close();
				app.log.LockedLog() << "FSK-Demod diagnostics stopped" << endl;
			}
		}

		// post our confirmation
		PostEvent(new events::EventDemodDiagnosticsUpdateComplete_t());
	}


	/// <summary>	Event Handler: status request. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpDemodulation::HandleEvtStatusRequest(events::EventStatusRequest_t* pEvt, DiagnosticsDemod& diagnostics)
	{
		// create our status
		user_api::DemodulationStatus_t status;
		status.diagnostics.enable = diagGlobalEnable;
		status.counters = statusCounters;

		// if instructed to clear counters, do so now.
		if (pEvt->clearCounters)
		{
			statusCounters.Reset();
		}

		// post our report
		PostEvent(new events::EventDemodStatusReport_t(pEvt->clearCounters, status));
	}


	/// <summary>	Updates the configuration. </summary>
	///
	/// <param name="diagnostics">	[in] The diagnostics instance. </param>
	void FskModemOpDemodulation::UpdateConfiguration(DiagnosticsDemod& diagnostics)
	{
		// release demod chans
		ReleaseChannels();

		// update our active params id then pull the configuration params
		chanParams.parmCommon = app.pAppCfg->CommonParameters();
		chanParams.parmDemod = app.pAppCfg->DemodulationParameters();
		chanParams.maxSampleCount = EncIqBufferSizeBytes;

		app.log.LockedLog() << "FSK-Demod using fs=" << chanParams.parmCommon .sampleRate << " fd=" << chanParams.parmCommon.frequencyDeviation << " br=" << 
			chanParams.parmCommon.bitrate << endl;
		
		// create the per-channel demod map based on the active configuration
		for (auto entry : chanParams.parmCommon.chanMap)
		{
			chanDemodMap[entry.first] = new FskDemodChannel(app, diagnostics, statusCounters, entry.first, &chanParams);
		}
	}


	/// <summary>	Release all channel instances (if any). </summary>
	void FskModemOpDemodulation::ReleaseChannels(void)
	{
		// destroy demod chans
		for (auto pChDemod : chanDemodMap)
		{
			delete pChDemod.second;
		}
		chanDemodMap.clear();
	}


	/// <summary>	Decodes an iq frame. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised when bad params found. </exception>
	///
	/// <param name="pEncFrame">	[in] The encoded frame, as 32b words. </param>
	/// <param name="lengthX32">   	The length. </param>
	/// <param name="pDecFrame">	[out] The decoded frame. </param>
	///
	/// <returns>	Sample count. </returns>
	int FskModemOpDemodulation::DecodeIqFrame(uint32_t* pEncFrame, int lengthX32, IqPair_t* pDecFrame)
	{
		uint64_t				working64;
		int						workingBitsAvail;
		int						encIdx;
		int						sampleIdx;
		int						sampleCount;
		int						samplesRemaining;
		uint16_t				dataI, dataQ;


		// check params
		if (!pEncFrame || !pDecFrame)
		{
			throw std::runtime_error(str(boost::format("Bad params %1%[%2%]") % (strrchr("/" __FILE__, '/') + 1) % __LINE__));
		}


		// prep. Samples are 20b, number of samples is the bit length of the encoded
		// frame / 20.
		encIdx = 0;
		sampleIdx = 0;
		sampleCount = (lengthX32 * 32) / 20;
		samplesRemaining = sampleCount;
		workingBitsAvail = 20;
		working64 = 0;

		// decode
		while (samplesRemaining--)
		{
			// shift working64
			working64 <<= 20;
			workingBitsAvail -= 20;

			// bring in more data if needed
			if (workingBitsAvail < 20)
			{
				working64 |= ((uint64_t)pEncFrame[encIdx++]) << (32 - workingBitsAvail);
				workingBitsAvail += 32;
			}

			// take top 20b of working64 as I[19:10], Q[9:0], decode the S3.6 format.
			pDecFrame[sampleIdx].i = DecodeS36(working64 >> 54);
			pDecFrame[sampleIdx++].q = DecodeS36((working64 >> 44) & 0x3ff);
		};

		return sampleCount;
	}


	/// <summary>	Decodes the s3.6 format. </summary>
	///
	/// <param name="value">	The encoded value. </param>
	///
	/// <returns>	The decoded float. </returns>
	inline float FskModemOpDemodulation::DecodeS36(uint16_t value)
	{
		// sign extend and interpret as signed
		int16_t valueSignExtended = (int16_t)(value & 0x200) ? (value | 0xfc00) : value;
		return (float) valueSignExtended / 64.0;
	}
}