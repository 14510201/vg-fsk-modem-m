// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : AppConfiguration.hpp                                   :
// Date Created       : 2024 JAN 20                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <map>
#include <list>
#include "FilterIir.hpp"
#include "SvcFskModemCmdRespApi.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: App Configuration. </summary>
	class AppConfiguration
	{
	public:

		/// <summary>	Available configuration. </summary>
		struct AvailConfiguration_t
		{
			AvailConfiguration_t(std::string name, std::string description) : name(name), description(description) {}
			std::string										name;
			std::string										description;
		};


		/// <summary>	Modulation gaussian pulse shaping. </summary>
		struct ModulationGaussianPulseShaping_t
		{
			float											bt;
			std::vector<std::vector<float>>					preRollTransitions;
			std::vector<std::vector<float>>					postRollTransitions;
			std::vector<std::vector<float>>					bitTransitions;
		};


		/// <summary>	Modulation parameters. </summary>
		struct ModulationParameters_t
		{
			int												upsample;
			float											defaultOutputLevel;
			float											pulseShapingBt;
			FilterIir::FilterDefinition_t					preDecimationLpf;
			std::map<int, ModulationGaussianPulseShaping_t>	pulseShapingMap;
		};


		/// <summary>	Demodulation parameters. </summary>
		struct DemodulationParameters_t
		{
			FilterIir::FilterDefinition_t					sigDetectLpf;
			FilterIir::FilterDefinition_t					primaryLpf;
		};

		
		/// <summary>	Common parameters. </summary>
		struct CommonParameters_t
		{
			float											sampleRate;
			float											bitrate;
			float											frequencyDeviation;
			uint32_t										syncWord;
			int												syncWordLengthBits;
			int												preambleLengthBits;
			std::map<int, float>							chanMap;
		};


	protected:

		/// <summary>	Configuration map entry. </summary>
		struct ConfigEntry_t
		{	
			std::string										name;
			std::string										description;
			CommonParameters_t								parmCommon;
			DemodulationParameters_t						parmDemodulation;
			ModulationParameters_t							parmModulation;
		};

		std::map<std::string, ConfigEntry_t>				cfgMap;
		std::string											defaultCfgName;
		ConfigEntry_t*										pActiveConfigEntry;
		user_api::ModulationConfiguration_t					activeModConfig;

	public:
		AppConfiguration(void);
		~AppConfiguration(void) {};
		void												Configuration(std::string cfgName, user_api::ModulationConfiguration_t& modConfig);
		void												Configuration(std::string& cfgName, std::string& cfgDescription, user_api::ModulationConfiguration_t& modConfig);
		CommonParameters_t									CommonParameters(void) { return pActiveConfigEntry->parmCommon; }
		DemodulationParameters_t							DemodulationParameters(void) { return pActiveConfigEntry->parmDemodulation; }
		ModulationParameters_t								ModulationParameters(void);
		std::list<AvailConfiguration_t>						ConfigurationList(void);

		static float										ChannelCenterFreq(std::map<int, float>& chanMap, int chanNumber);
		static ModulationGaussianPulseShaping_t				ModulationPulseShaping(std::map<int, ModulationGaussianPulseShaping_t>& pulseShapingMap, float bt);


	protected:
		void												ImportMatlabDna(void);
	};
}