// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpEvtHost.cpp                                  :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Base class for event-host ops.
//
// -----------------------------------------------------------------------------

#include <boost/format.hpp>
#include "FskModemOpEvtHost.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Destructor. </summary>
	FskModemOpEvtHost::~FskModemOpEvtHost(void)
	{
		// clean up
		for (auto chan : evtChannelMap)
		{
			delete chan.second;
		}
	}


	/// <summary>	Creates an event channel. </summary>
	///
	/// <param name="evtChanId">	Identifier for the event channel. </param>
	///
	/// <returns>	Null if it fails, else the new event channel. </returns>
	FskModemEventChannel* FskModemOpEvtHost::CreateEventChannel(EventChannelId_t evtChanId)
	{
		// ensure this request was not already made
		auto chan = evtChannelMap.find(evtChanId);
		if (chan != evtChannelMap.end())
		{
			throw std::runtime_error(str(boost::format("Only one event channel per ID allowed (%d)") % evtChanId));
		}

		// create / add new event channel
		auto pEvtChan = new FskModemEventChannel();
		evtChannelMap[evtChanId] = pEvtChan;
		return pEvtChan;
	}


	/// <summary>	Posts an event to node. </summary>
	///
	/// <param name="evtChanId">	Identifier for the event channel. </param>
	/// <param name="pEvt">			[in] Prt to the event. </param>
	void FskModemOpEvtHost::PostEvent(EventChannelId_t evtChanId, events::Event_t* pEvt)
	{ 
		auto chan = evtChannelMap.find(evtChanId);
		if (chan != evtChannelMap.end())
		{
			chan->second->PostToNode(pEvt);
		}
	}


	/// <summary>	Next event from node. </summary>
	///
	/// <param name="evtChanId">	Identifier for the event channel. </param>
	/// <param name="pEvt">			[out] Ref to event ptr. </param>
	/// <param name="wait">			True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	bool FskModemOpEvtHost::NextEvent(EventChannelId_t evtChanId, events::Event_t*& pEvt, bool wait)
	{
		return NextEvent(evtChanId, pEvt, events::EventType_t::Empty, wait);
	}


	/// <summary>	Next event from node. </summary>
	///
	/// <param name="evtChanId"> 	Identifier for the event channel. </param>
	/// <param name="pEvt">		 	[out] Ref to event ptr. </param>
	/// <param name="reqEvtType">	Type of the event requested. </param>
	/// <param name="wait">		 	True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	bool FskModemOpEvtHost::NextEvent(EventChannelId_t evtChanId, events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait)
	{ 
		auto chan = evtChannelMap.find(evtChanId);
		if (chan != evtChannelMap.end())
		{
			return chan->second->NextEventFromNode(pEvt, wait);
		}
	}
}