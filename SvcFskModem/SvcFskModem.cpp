﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : SvcFskModem.cpp                                        :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Entry point for the service.
//
// -----------------------------------------------------------------------------


#include <cstring>
#include <chrono>
#include <iostream> 
#include <boost/interprocess/exceptions.hpp>
#include <boost/format.hpp>
#include <signal.h>
#include "StreamLog.hpp"
#include "FskModemApp.hpp"
#include "FskModemCore.hpp"
#include "SvcFskModem.hpp"

using namespace std;
using namespace svc_fsk_modem;
using namespace svc_fsk_modem::log;


/// <summary>	Global context pointers for use in the signal handler(s) </summary>
FskModemApp*			pApp = NULL;
FskModemCore*			pCore = NULL;


/// <summary>	Handle SIGTERM signal. This will gracefully shutdown. </summary>
///
/// <param name="signo">  	The signal. </param>
/// <param name="info">   	[in] If non-null, the sig info. </param>
/// <param name="context">	[in] If non-null, the context. </param>
void					SigTermHandler(int signo, siginfo_t* info, void* context)
{
	if(signo == SIGTERM)
	{
		if (pApp)
		{
			pApp->log << "SIGTERM received, shutting down..." << endl << flush;
		}
		if (pCore)
		{
			pCore->Shutdown();
		}
	}
}


/// <summary>	Main entry-point for this application. </summary>
///
/// <param name="argc">	The number of command-line arguments provided. </param>
/// <param name="argv">	An array of command-line argument strings. </param>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
int main(int argc, char* argv[])
{
	struct sigaction sigAct = { 0 };

	// create the app instance.
	FskModemApp app = FskModemApp();
	pApp = &app;

	try
	{
		// set log to file.
		app.log.Assign(str(boost::format("/tmp/%1%.log") % FskModemApp::AppName));

		// log start
		app.log.AddTimestamp(true);
		std::time_t timeNow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		app.log.LockedLog() << str(boost::format("%1% v%2% %3%") % FskModemApp::AppName % FskModemApp::AppVersion % "Started ") << ctime(&timeNow) << flush;

		// configure the app
		app.Configure();

		// create core, run it.
		FskModemCore core = FskModemCore(app);
		pCore = &core;

		// install signal handler for sig term
		sigAct.sa_flags = SA_SIGINFO;
		sigAct.sa_sigaction = &SigTermHandler;
		if (sigaction(SIGTERM, &sigAct, NULL) == -1) {
			app.log.LockedLog() << "Unable to install SIGTERM handler" << endl << flush;
			exit (EXIT_FAILURE);
		}

		// run the core
		core.Run();
		timeNow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		app.log.LockedLog() << "Done " << ctime(&timeNow) << flush;
	}
	catch (boost::interprocess::interprocess_exception const& ipe)
	{
		app.log.LockedLog() << endl << ipe.what() << endl;
		return (EXIT_FAILURE);
	}
	catch (exception& ex)
	{
		app.log.LockedLog() << endl << str(boost::format("Exception: %1%") % ex.what()).c_str() << endl << flush;
		return (EXIT_FAILURE);
	}
	catch (...)
	{
		app.log.LockedLog() << endl << "Exception: Unknown" << endl << flush;
		return (EXIT_FAILURE);
	}

	return 0;
}
