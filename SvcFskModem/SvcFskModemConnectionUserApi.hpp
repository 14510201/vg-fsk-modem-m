// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : SvcFskModemConnectionUserApi.hpp                       :
// Date Created       : 2024 JAN 28                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once
#include "SvcFskModemCmdRespApi.hpp"


namespace svc_fsk_modem
{
	/// <summary>	Forward class declaraion. </summary>
	class FskModemConnectionUser;

	// user_api namespace
	namespace user_api
	{
		/// <summary>
		/// 	Class: Fsk modem connection, user API of the queues and shared memory.
		/// </summary>
		class SvcFskModemConnectionUserApi
		{
		private:
			/// <summary>	The user connection instance. </summary>
			FskModemConnectionUser* pUserConnection;

		public:

			/// <summary>	Default constructor. </summary>
			SvcFskModemConnectionUserApi(void);


			/// <summary>	Destructor. </summary>
			~SvcFskModemConnectionUserApi(void);


			/// <summary>	Attach to the FSK modem service. </summary>
			///
			/// <returns>	Returns 0 upon success, -EFAULT if unable to attach. </returns>
			int							Attach(void);


			/// <summary>	Get the service version. </summary>
			///
			/// <returns>	The version string. </returns>
			std::string					GetVersion(void);


			/// <summary>	Get the connection version. </summary>
			///
			/// <returns>	The version string. </returns>
			std::string					GetConnectionVersion(void);


			/// <summary>	Send a command. </summary>
			///
			/// <param name="cmd"> 	[in] Ref to the command. </param>
			/// <param name="wait">	(Optional) True to wait, false to return immediately. Default = wait. </param>
			///
			/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
			int							SendCommand(user_api::SvcFskModemCommand_t& cmd, bool wait = true);


			/// <summary>	Receive a response. </summary>
			///
			/// <param name="resp">	[out] Ref for the response. </param>
			/// <param name="wait">	(Optional) True to wait, false to return immediately. Default = wait. </param>
			///
			/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
			int							ReceiveResponse(user_api::SvcFskModemResponse_t& resp, bool wait = true);


			/// <summary>
			/// 	Send an I/Q data frame for demodulation.
			/// </summary>
			///
			/// <exception cref="runtime_error">	Raised if not attached. </exception>
			///
			/// <param name="pFrame">  	[in] Pointer to the frame data. </param>
			/// <param name="lengthX8">	The length of the frame in bytes. </param>
			/// <param name="wait">	   	(Optional) True to wait, false to return immediately. Default = true. </param>
			///
			/// <returns>
			/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
			/// 	after timeout when waiting.
			/// </returns>
			int							SendDemodIqFrame(void* pFrame, uint16_t lengthX8, bool wait = true);


			/// <summary>	Receive a demod packet. </summary>
			///
			/// <exception cref="runtime_error">	Raised if not attached. </exception>
			///
			/// <param name="pPacket">	  	[out] Pointer to store the received packet. </param>
			/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pPacket. </param>
			/// <param name="pChanNumber">	[out] If non-null, the channel number will be stored. </param>
			/// <param name="wait">		  	(Optional) True to wait, false to return immediately. Default =
			/// 							true. </param>
			///
			/// <returns>
			/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
			/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT if
			/// 	bad  parameters.
			/// </returns>
			int							ReceiveDemodPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait = true);


			/// <summary>
			/// 	Send a packet for modulation.
			/// </summary>
			///
			/// <exception cref="runtime_error">	Raised if not attached. </exception>
			///
			/// <param name="pPacket">   	[in] Pointer to the packet data. </param>
			/// <param name="lengthX8">  	The length of the packet in bytes. </param>
			/// <param name="chanNumber">	The channel number to modulate on. </param>
			/// <param name="wait">		 	(Optional) True to wait, false to return immediately. Default =
			/// 							true. </param>
			///
			/// <returns>
			/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
			/// 	after timeout when waiting.
			/// </returns>
			int							SendModPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait = true);


			/// <summary>	Receive a modulated I/Q frame for transmission. </summary>
			///
			/// <exception cref="runtime_error">	Raised if not attached. </exception>
			///
			/// <param name="pFrame">	  	[out] Pointer to store the received frame. </param>
			/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pFrame. </param>
			/// <param name="wait">		  	(Optional) True to wait, false to return immediately. Default =
			/// 							true. </param>
			///
			/// <returns>
			/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
			/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT if
			/// 	bad  parameters.
			/// </returns>
			int							ReceiveModIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait = true);
		};
	}
}