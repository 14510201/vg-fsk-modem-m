// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemConnectionOwner.cpp                            :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Implement the ipc queues as the owner.
//
// -----------------------------------------------------------------------------

#include "FskModemConnectionOwner.hpp"
#include <boost/interprocess/mapped_region.hpp>

using namespace std;
using namespace boost::interprocess;

namespace svc_fsk_modem
{
	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">	[in,out] The application. </param>
	FskModemConnectionOwner::FskModemConnectionOwner(FskModemApp& app) : app(app), FskModemIpcQueues() 
	{
		pShmRemover = nullptr;
		pShmMappedRegion = nullptr;
	}


	/// <summary>	Destructor. </summary>
	FskModemConnectionOwner::~FskModemConnectionOwner(void)
	{
		if (pShmMappedRegion)
		{
			delete pShmMappedRegion;
		}
		if (pShmRemover)
		{
			delete pShmRemover;
		}
	}


	/// <summary>	Creates the connection as the owner. </summary>
	///
	/// <param name="qCommandCapacity"> 	The command queue capacity. </param>
	/// <param name="qResponseCapacity">	The response queue capacity. </param>
	/// <param name="qPktCapacity">			The capacity of each packet queue. </param>
	/// <param name="qIqFrameCapacity"> 	The capacity of each IQ frame queue. </param>
	///
	/// <returns>	Returns 0 upon success, -ENOMEM if unable to create. </returns>
	int	FskModemConnectionOwner::Create(int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity)
	{
		try
		{
			// We want to start from scratch each time, add the remover which will remove
			// an existing instance on creation and destruction.
			pShmRemover = new ShmRemover(app.AppConnectionName.c_str());

			// create a shared memory object.
			shared_memory_object shm(create_only, app.AppConnectionName.c_str(), read_write);

			// set size, which is based on our capacity
			shm.truncate(GetBlockSizeX8(qCommandCapacity, qResponseCapacity, qPktCapacity, qIqFrameCapacity));

			// map the whole shared memory in this process
			pShmMappedRegion = new mapped_region(shm, read_write);

			// create the Qs
			FskModemIpcQueues::Create(pShmMappedRegion->get_address(), qCommandCapacity, qResponseCapacity, qPktCapacity, qIqFrameCapacity);

			// set the connection version
			SetConnectionVersion((char*) Version.c_str());
		}
		catch (interprocess_exception& ex)
		{
			return -ENOMEM;
		}

		// success
		return 0;
	}


	/// <summary>	Sets the service's version. </summary>
	///
	/// <param name="version">	[in] Ref to the version string. </param>
	void FskModemConnectionOwner::SetVersion(std::string version)
	{
		FskModemIpcQueues::SetVersion(version);
	}
	
	
	/// <summary>	Sets the connection's version. </summary>
	///
	/// <param name="version">	[in] Ref to the version string. </param>
	void FskModemConnectionOwner::SetConnectionVersion(std::string version)
	{
		FskModemIpcQueues::SetConnectionVersion(version);
	}


	/// <summary>	Receive a command. </summary>
	///
	/// <param name="cmd"> 	[out] Ref for the command. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int	FskModemConnectionOwner::ReceiveCommand(user_api::SvcFskModemCommand_t& cmd, bool wait)
	{
		return FskModemIpcQueues::GetNextCommandEntry(cmd, wait);
	}


	/// <summary>	Send a response. </summary>
	///
	/// <param name="resp">	[in] Ref to the response. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int FskModemConnectionOwner::SendResponse(user_api::SvcFskModemResponse_t& resp, bool wait)
	{
		return FskModemIpcQueues::AddResponseEntry(resp, wait);
	}


	/// <summary>	Receive an I/Q frame for demodulation. </summary>
	///
	/// <param name="pFrame">	  	[out] Pointer to store the received frame. </param>
	/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pFrame. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT if
	/// 	bad  parameters.
	/// </returns>
	int	FskModemConnectionOwner::ReceiveDemodIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait)
	{
		// get from the correct Q
		return GetNextModDemodEntry(QueueId_t::DemodIqFramesToModem, pFrame, maxLengthX8, wait);
	}


	/// <summary>
	/// 	Send a demod packet.
	/// </summary>
	///
	/// <param name="pPacket">   	[in] Pointer to the packet data. </param>
	/// <param name="lengthX8">  	The length of the packet in bytes. </param>
	/// <param name="chanNumber">	The channel number the packet came from. </param>
	/// <param name="wait">		 	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout when waiting.
	/// </returns>
	int	FskModemConnectionOwner::SendDemodPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait)
	{
		// add to the correct Q. The tag is used to pass the channel number.
		return AddModDemodEntry(QueueId_t::DemodPktsFromModem, pPacket, lengthX8, (uint16_t) chanNumber, wait);
	}


	/// <summary>	Receive a packet for modulation. </summary>
	///
	/// <param name="pPacket">	  	[out] Pointer to store the received packet. </param>
	/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pPacket. </param>
	/// <param name="pChanNumber">	[out] If non-null, the channel number to modulate on will be stored. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT if
	/// 	bad  parameters.
	/// </returns>
	int	FskModemConnectionOwner::ReceiveModPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait)
	{
		uint16_t			tag;

		// get from the correct Q. The tag is used to pass the channel number.
		int retn = GetNextModDemodEntry(QueueId_t::ModPktsToModem, pPacket, maxLengthX8, &tag, wait);
		if (pChanNumber)
		{
			*pChanNumber = (uint8_t)tag;
		}
		return retn;
	}


	/// <summary>
	/// 	Send an I/Q modulation frame for transmission.
	/// </summary>
	///
	/// <param name="pFrame">  	[in] Pointer to the frame data. </param>
	/// <param name="lengthX8">	The length of the frame in bytes. </param>
	/// <param name="wait">	   	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout when waiting.
	/// </returns>
	int	FskModemConnectionOwner::SendModIqFrame(void* pFrame, uint16_t lengthX8, bool wait)
	{
		// add to the correct Q
		return AddModDemodEntry(QueueId_t::ModIqFramesFromModem, pFrame, lengthX8, wait);
	}
}