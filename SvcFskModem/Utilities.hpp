// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : Utilities.hpp                                          :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <stdint.h>
#include <limits>

namespace svc_fsk_modem
{
    namespace utilities
    {
        /// <summary>	Class: Increment and saturate. </summary>
        class IncAndSaturate
        {
        private:
            IncAndSaturate(void) {}

        public:

            /// <summary>	Increments the given value, saturating at max value. </summary>
            ///
            /// <typeparam name="T">	Generic type parameter. </typeparam>
            /// <param name="value">	[in,out] The value. </param>
            template<typename T>
            static void Inc(T& value)
            {
                constexpr T max_value{ std::numeric_limits<T>::max() };
                if (value != max_value)  value++;
            }
        };
    }
}

