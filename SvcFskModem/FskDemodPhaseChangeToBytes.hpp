// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskDemodPhaseChangeToBytes.hpp                         :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>
#include "DiagnosticsDemod.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: FSK Demodulation Phase Change to Bytes conversion
	/// </summary>
	class FskDemodPhaseChangeToBytes
	{
	protected:

		/// <summary>	(Immutable) Peak to trough detect count threshold to move on. </summary>
		const int						PeakTroughDetectThresh = 10;


		/// <summary>
		/// 	(Immutable) Once we find a peak or trough, we will skip samples before looking for the
		/// 	opposite.
		/// </summary>
		const int						PeakTroughPostGapSampleCount = 5;


		/// <summary>	State of processing. </summary>
		enum State_t
		{
			SeekPreamblePeak = 0,
			SeekPreamblePostPeakGap,
			SeekPreambleTrough,
			SeekPreamblePostTroughGap,
			AlignSync,
			PackBytes
		};

		DiagnosticsDemod&				diag;
		int								chanNumber;
		float							bitTimeInSamples;
		int								ptSpacingMin;
		int								ptSpacingMax;
		State_t							state;
		float							lastPhaseChange;
		float							peakAvgAccum;
		int								lastPeakLoc;
		float							troughAvgAccum;
		int								lastTroughLoc;
		float							ptAverage;
		int								preambleCycles;
		int								curSampleLoc;
		int								gapEndLoc;
		float							nextDataSamplePoint;
		uint32_t						syncWord;
		uint32_t						bitAccum;
		bool							dataInvesionFlag;
		int								bitsRemaining;


	public:
		FskDemodPhaseChangeToBytes(void) = delete;
		FskDemodPhaseChangeToBytes(DiagnosticsDemod& diag, int chanNumber, float bitTimeInSamples, uint32_t syncWord);
		~FskDemodPhaseChangeToBytes(void) {}

		void							Process(std::vector<uint8_t>* pByteAccum, float* pPhaseData, int length);
		void							Reset(void);

	protected:
		bool							PeakTroughSampleSpacingOk(int lastLoc);
	};
}