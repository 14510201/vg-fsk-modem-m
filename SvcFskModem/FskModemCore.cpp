// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemCore.cpp                                       :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK mod/demod functionality.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <thread>
#include "FskModemCore.hpp"
#include "FskModemOpDemodulation.hpp"
#include "FskModemOpModulation.hpp"
#include "FskModemOpCmdResp.hpp"

#include <boost/format.hpp>
using namespace std;

namespace svc_fsk_modem
{
	/// <summary>	Constructor. Instantiate the queues. </summary>
	///
	/// <param name="app">	[in,out] The application. </param>
	FskModemCore::FskModemCore(FskModemApp& app) : app(app), qConnect(FskModemConnectionOwner(app))
	{
		flagShutdown = false;
	}


	/// <summary>	Destructor. </summary>
	FskModemCore::~FskModemCore(void)
	{
		// destroy op threads
		for (auto pOp : opThreads)
		{
			delete pOp;
		}
		opThreads.clear();
	}


	/// <summary>	Runs the core object. </summary>
	void FskModemCore::Run(void)
	{
		// create the shared Qs, set our version
		if (qConnect.Create(app.AppConnectionCommandQCapacity, app.AppConnectionResponseQCapacity, 
			app.AppConnectionPktQCapacity, app.AppConnectionIqFrameQCapacity) < 0)
		{
			app.log.LockedLog() << "Error creating shared Qs" << endl;
			return;
		}
		qConnect.SetVersion((char*)app.AppVersion.c_str());
		app.log.LockedLog() << "Starting core operations" << endl;

		// launch controls/status (must be first, its opThreads[0] reference is used to get the status/control trackers).
		opThreads.push_back(new OpThread_t(new FskModemOpCmdResp(app, qConnect)));
		auto pOpCmdResp = (FskModemOpCmdResp*)opThreads[0]->pOp;

		// launch demodulation
		opThreads.push_back(new OpThread_t(new FskModemOpDemodulation(app, qConnect, 
			pOpCmdResp->RegisterForEvents(FskModemOpCmdResp::EventChannelId_t::Demodulation))));

		// launch modulation
		opThreads.push_back(new OpThread_t(new FskModemOpModulation(app, qConnect, 
			pOpCmdResp->RegisterForEvents(FskModemOpCmdResp::EventChannelId_t::Modulation))));

		// wait for shutdown
		WaitShutdown();

		// stop all threads then join
		for (auto pOpThread : opThreads)
		{
			pOpThread->pOp->Stop();
		}
		for (auto pOpThread : opThreads)
		{
			pOpThread->pThread->join();
		}

		// handle propagated exceptions (if any)
		for (auto pOpThread : opThreads)
		{
			auto pEx = pOpThread->pOp->GetException();
			if (pEx)
			{
				std::rethrow_exception(pEx);
			}
		}

		app.log.LockedLog() << "Ended core operations" << endl;
	}


	/// <summary>	Wait for shutdown while monitoring threads for exit. </summary>
	void FskModemCore::WaitShutdown(void)
	{
		boost::unique_lock<boost::mutex> lock(mutShutdown);
		while (!flagShutdown)
		{
			if (condShutdown.wait_for(lock, boost::chrono::milliseconds(1000)) == boost::cv_status::timeout)
			{
				// check all op threads for any no longer running
				for (auto pOpThread : opThreads)
				{
					if (!pOpThread->pOp->IsRunning())
					{
						flagShutdown = true;
					}
				}
			}
		}
	}


	/// <summary>	Signal shutdown of core operations. </summary>
	void FskModemCore::Shutdown(void)
	{
		{
			boost::lock_guard<boost::mutex> lock(mutShutdown);
			flagShutdown = true;
		}
		condShutdown.notify_all();
	}
}