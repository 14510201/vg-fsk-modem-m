// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOp.hpp                                         :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemConnectionOwner.hpp"
#include "FskModemApp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem base op functionality. </summary>
	class FskModemOp
	{
	protected:
		FskModemApp&					app;
		FskModemConnectionOwner&		qConnect;
		bool							running;
		bool							stop;
		std::exception_ptr				exceptPtr;

	public:
		FskModemOp(void) = delete;
		FskModemOp(FskModemApp& app, FskModemConnectionOwner& qConnect)
			: app(app), qConnect(qConnect) 
		{
			running = false;
			stop = false;
			exceptPtr = nullptr;
		}
		virtual ~FskModemOp(void) {};

		bool							IsRunning(void) { return running; }
		void							Stop(void) { stop = true; }
		void							RunAsThread(void) 
										{
											try { Run(); }
											catch (...) 
											{
												running = false;
												exceptPtr = std::current_exception();
												return;
											}
										};
		virtual void					Run(void) {};
		std::exception_ptr				GetException(void) { return exceptPtr; }
	};
}