// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : AppConfiguration.cpp                                   :
// Date Created       : 2024 JAN 20                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Contains the map of configurations, mod/demod params, and map of 
//      channel numbers to center frequencies.
//
// -----------------------------------------------------------------------------

#include <boost/format.hpp>
#include "AppConfiguration.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Default constructor. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised if there are no configurations defined. </exception>
	AppConfiguration::AppConfiguration(void)
	{
		// Import the matlab DNA
		ImportMatlabDna();

		// error if nothing defined
		if (cfgMap.size() == 0)
		{
			throw std::runtime_error("No configurations defined.");
		}

		// Select the default configuration
		auto entry = cfgMap.find(defaultCfgName);
		if (entry != cfgMap.end())
		{
			// set active config entry
			pActiveConfigEntry = &entry->second;

			// set mod config to the inits for the config
			activeModConfig.pulseShapingBt = pActiveConfigEntry->parmModulation.pulseShapingBt;
		}
		else
		{
			throw std::runtime_error(str(boost::format("Default configuration '%1%' unknown.") % defaultCfgName));
		}
	}


	/// <summary>	Sets a configuration. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised if config option not found. </exception>
	///
	/// <param name="cfgName">  	Name of the configuration. </param>
	/// <param name="modConfig">	[in] The modulation specific configuration. </param>
	void AppConfiguration::Configuration(std::string cfgName, user_api::ModulationConfiguration_t& modConfig)
	{
		// select the active config
		auto entry = cfgMap.find(cfgName);
		if (entry != cfgMap.end())
		{
			// set active
			pActiveConfigEntry = &entry->second;
			activeModConfig = modConfig;
		}
		else
		{
			throw std::runtime_error(str(boost::format("Configuration '%1%' unknown.") % cfgName));
		}
	}


	/// <summary>	Configurations the given configuration description. </summary>
	///
	/// <param name="cfgName">		 	[out] Name of the configuration. </param>
	/// <param name="cfgDescription">	[out] The configuration description. </param>
	/// <param name="modConfig">	 	[out] The modulation specific configuration. </param>
	void AppConfiguration::Configuration(std::string& cfgName, std::string& cfgDescription, user_api::ModulationConfiguration_t& modConfig)
	{
		// report the active configuration
		cfgDescription = pActiveConfigEntry->description;
		cfgName = pActiveConfigEntry->name;
		modConfig = activeModConfig;
	}


	/// <summary>	Get modulation parameters. </summary>
	///
	/// <returns>	The modulation parameters struct. </returns>
	AppConfiguration::ModulationParameters_t AppConfiguration::ModulationParameters(void)
	{ 
		// there are params that come from the mod config, overwrite them from the
		// default of the entry.
		auto act = pActiveConfigEntry->parmModulation;
		act.pulseShapingBt = activeModConfig.pulseShapingBt;

		// return
		return act; 
	}


	/// <summary>	Get the list of available configurations. </summary>
	///
	/// <returns>	A list of. </returns>
	std::list<AppConfiguration::AvailConfiguration_t> AppConfiguration::ConfigurationList(void)
	{
		std::list<AvailConfiguration_t> lstCfgs;
		for (auto cfg : cfgMap)
		{
			lstCfgs.emplace_back(cfg.second.name, cfg.second.description);
		}

		return lstCfgs;
	}


	/// <summary>	Channel Center frequency. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised when the channel is unknown. </exception>
	///
	/// <param name="chanMap">   	[in] The channel map. </param>
	/// <param name="chanNumber">	The channel number. </param>
	///
	/// <returns>	The center frequency of the channel. </returns>
	float AppConfiguration::ChannelCenterFreq(std::map<int, float>& chanMap, int chanNumber)
	{
		auto entry = chanMap.find(chanNumber);
		if (entry != chanMap.end())
		{
			return entry->second;
		}
		else
		{
			throw std::runtime_error(str(boost::format("Channel Map chan %1% unknown.") % chanNumber));
		}
	}


	/// <summary>	Modulation pulse shaping. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised if the desired BT value is unknown. </exception>
	///
	/// <param name="pulseShapingMap">	[in] The pulse shaping map. </param>
	/// <param name="bt">			  	The desired bt. </param>
	///
	/// <returns>	The modulation gaussian pulse shaping definition. </returns>
	AppConfiguration::ModulationGaussianPulseShaping_t 
		AppConfiguration::ModulationPulseShaping(std::map<int, ModulationGaussianPulseShaping_t>& pulseShapingMap, float bt)
	{
		auto entry = pulseShapingMap.find(bt);
		if (entry != pulseShapingMap.end())
		{
			return entry->second;
		}
		else
		{
			throw std::runtime_error(str(boost::format("Modulation pulse shaping bt=%1% unknown.") % bt));
		}
	}
}