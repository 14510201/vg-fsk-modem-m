// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskDemodChannel.hpp                                    :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemApp.hpp"
#include "FskModemConnectionOwner.hpp"
#include "IqPair.hpp"
#include "FilterIir.hpp"
#include "Nco.hpp"
#include "AtanQI.hpp"
#include "DiagnosticsDemod.hpp"
#include "FskDemodPhaseChangeToBytes.hpp"
#include "Whitening.hpp"
#include "FskCrc.hpp"


namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem demodulation functionality for a single channel. </summary>
	class FskDemodChannel
	{
	public:

		/// <summary>	Parameters. </summary>
		struct Parameters_t
		{
			AppConfiguration::CommonParameters_t		parmCommon;
			AppConfiguration::DemodulationParameters_t	parmDemod;
			int											maxSampleCount;
		};


		/// <summary>	Packet descriptor. </summary>
		struct PacketDesc_t
		{
			uint8_t*									pPkt;
			int											pktLengthX8;
			int											chanNumber;
		};

	private:

		/// <summary>	(Immutable) General constants. </summary>
		const int							MaxPacketLengthX8 = 255;
		const float							IqSignalDetectedThreshold = 0.2;
		const float							SignalDetectThreshold = 0.5;
		const float							Pi = 3.141592741012573;

		FskModemApp&						app;
		DiagnosticsDemod&					diag;
		user_api::DemodulationCounters_t&	statusCounters;
		int									chanNumber;
		Parameters_t*						pParams;
		float								chanCenterFreq;
		IqPair_t*							pIqBufrA;
		IqPair_t*							pIqBufrB;
		float*								pBufrA;
		float*								pBufrB;
		Nco*								pNcoSin;
		float*								pNcoSinSamples;
		Nco*								pNcoCos;
		float*								pNcoCosSamples;
		FilterIir*							pFilterAntiImage;
		FilterIir*							pFilterPhase;
		FilterIir*							pFilterSigDetect;
		float								prevPhase;
		AtanQI*								pAtanQI;
		uint8_t*							pPktBufr;
		PacketDesc_t						pktDesc;
		FskDemodPhaseChangeToBytes*			pPhaseChangeToBytes;
		std::vector<uint8_t>				demodBytesAccum;
		Whitening							dewhitener;
		FskCrc								crc;


	public:
		FskDemodChannel(void) = delete;
		FskDemodChannel(FskModemApp& app, DiagnosticsDemod& diag, user_api::DemodulationCounters_t& statusCounters, 
			int chanNumber, Parameters_t* pParams);
		~FskDemodChannel(void);


		PacketDesc_t*						Demodulate(IqPair_t* pData, int sampleCount);

	protected:
		float*								DifferentiatePhase(float* pPrevPhase, float* pSrcBufr, float* pDestBufr, int length);
		inline float						CorrectPhaseReversal(float diffPhase);
	};
}