// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskDemodChannel.cpp                                    :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK demodulation functionality for a given channel.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <cmath>
#include <boost/format.hpp>
#include "FskDemodChannel.hpp"
#include "Utilities.hpp"


namespace svc_fsk_modem
{
	using namespace std;


	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">			 	[in] The application. </param>
	/// <param name="diag">			 	[in] The diagnostic exporter. </param>
	/// <param name="statusCounters">	[in,out] Max number of samples to support. </param>
	/// <param name="chanNumber">	 	The channel number. </param>
	/// <param name="pParams">		 	[in,out] If non-null, options for controlling the operation. </param>
	FskDemodChannel::FskDemodChannel(FskModemApp& app, DiagnosticsDemod& diag, user_api::DemodulationCounters_t& statusCounters,
		int chanNumber, Parameters_t* pParams) : app(app), diag(diag), statusCounters(statusCounters), chanNumber(chanNumber), 
		pParams(pParams)
	{ 
		// lookup the channel center freq
		chanCenterFreq = AppConfiguration::ChannelCenterFreq(pParams->parmCommon.chanMap, chanNumber);

		app.log.LockedLog() << "FSK-DemodChan monitoring channel " << chanNumber << " (" << chanCenterFreq / 1000.0 << " kHz)" << endl;

		// create our buffers
		pIqBufrA = new IqPair_t[pParams->maxSampleCount];
		pIqBufrB = new IqPair_t[pParams->maxSampleCount];
		pNcoSinSamples = new float[pParams->maxSampleCount];
		pNcoCosSamples = new float[pParams->maxSampleCount];
		pBufrA = new float[pParams->maxSampleCount];
		pBufrB = new float[pParams->maxSampleCount];

		// create sin and cos NCOs, set to the channel freq
		pNcoSin = new Nco(pParams->parmCommon.sampleRate);
		pNcoSin->Frequency(abs(chanCenterFreq));
		pNcoCos = new Nco(pParams->parmCommon.sampleRate);
		pNcoCos->Frequency(abs(chanCenterFreq), 90);

		// log if NCOs are not exact
		if (pNcoSin->Frequency() != abs(chanCenterFreq))
		{
			app.log.LockedLog() << "WARN FSK-DemodChan channel " << chanNumber << " nco=" << pNcoSin->Frequency() << " (requested=" << 
				abs(chanCenterFreq) << "," << pNcoSin->FrequencyResolution() << ")" << endl;
		}

		// get the demod config params
		auto params = app.pAppCfg->DemodulationParameters();

		// create filters
		pFilterAntiImage = new FilterIir(&params.primaryLpf);
		pFilterPhase = new FilterIir(&params.primaryLpf);
		pFilterSigDetect = new FilterIir(&params.sigDetectLpf);

		// create AtanQI
		pAtanQI = new AtanQI();
		prevPhase = 0;

		// create phase change to bytes 
		pPhaseChangeToBytes = new FskDemodPhaseChangeToBytes(diag, chanNumber, (pParams->parmCommon.sampleRate / pParams->parmCommon.bitrate), pParams->parmCommon.syncWord);

		// create pkt buffer and init the desc chan number (we will reuse both)
		pPktBufr = new uint8_t[MaxPacketLengthX8];
		pktDesc.chanNumber = chanNumber;
	}


	/// <summary>	Destructor. </summary>
	FskDemodChannel::~FskDemodChannel(void)
	{
		// delete buffers
		if (pIqBufrA) delete pIqBufrA;
		if (pIqBufrB) delete pIqBufrB;
		if (pBufrA) delete pBufrA;
		if (pBufrB) delete pBufrB;
		if (pNcoSinSamples) delete pNcoSinSamples;
		if (pNcoCosSamples) delete pNcoCosSamples;
		if (pPktBufr) delete pPktBufr;
	}


	/// <summary>
	/// 	The demodulation operation for the channel. IQ sample data in and packets out.
	/// 	
	/// 	NOTE: the returned packet descriptor is valid only until the next call to this 
	/// 	function, as the buffer is reused.
	/// </summary>
	///
	/// <param name="pData">	  	[in] Ptr to the IQ data. </param>
	/// <param name="sampleCount">	Number of samples. </param>
	///
	/// <returns>	Ptr to a packet descriptor when there is one, null if no packet. </returns>
	FskDemodChannel::PacketDesc_t* FskDemodChannel::Demodulate(IqPair_t* pData, int sampleCount)
    {
		float					mixedISubI, mixedISubQ, mixedQSubI, mixedQSubQ;
		IqPair_t*				pIqDataActive;
		int						activeSampleCount;
		float					sigDetect;


		// complex mix channel to baseband. Results in pIqBufrA.
		pNcoSin->GetSamples(sampleCount, pNcoSinSamples);
		pNcoCos->GetSamples(sampleCount, pNcoCosSamples);
		for (int idx = 0; idx < sampleCount; idx++)
		{
			mixedISubI = pData[idx].i * pNcoCosSamples[idx];
			mixedISubQ = pData[idx].i * pNcoSinSamples[idx];
			mixedQSubI = pData[idx].q * pNcoCosSamples[idx];
			mixedQSubQ = pData[idx].q * pNcoSinSamples[idx];
			if (chanCenterFreq < 0)
			{
				pIqBufrA[idx].i = mixedISubI - mixedQSubQ;
				pIqBufrA[idx].q = mixedQSubI + mixedISubQ;
			}
			else
			{
				pIqBufrA[idx].i = mixedISubI + mixedQSubQ;
				pIqBufrA[idx].q = mixedQSubI - mixedISubQ;
			}
		}

		// export for remote diagnostics
		//diag.ExportIqMixed(pIqBufrA, sampleCount, chanNumber);

		// anti-image filter. Results in pIqBufrB.
		pFilterAntiImage->Filter(pIqBufrA, pIqBufrB, sampleCount);

		// export for remote diagnostics
		//diag.ExportIqAntiImage(pIqBufrB, sampleCount, chanNumber);

		// detect signal
		pIqDataActive = pIqBufrB;
		activeSampleCount = 0;
		for (int idx = 0; idx < sampleCount; idx++)
		{
			// whenever I/Q is over the threshold, add a 1 to the filter, else a 0.
			if ((abs(pIqDataActive->i) > IqSignalDetectedThreshold) || (abs(pIqDataActive->q) > IqSignalDetectedThreshold))
			{
				sigDetect = pFilterSigDetect->Filter((float)1.0);
			}
			else
			{
				sigDetect = pFilterSigDetect->Filter((float)0.0);
			}

			// trim data off the front of the buffer if no signal and we do not yet
			// have active samples. Trim data off the tail of the buffer if no
			// signal and we have already have active samples. Note that once we decide
			// to trim the tail we stop looking. 
			if (sigDetect >= SignalDetectThreshold)
			{
				activeSampleCount++;
			}
			else if(!activeSampleCount)
			{
				pIqDataActive++;
			}
			else
			{
				break;
			}
		}

		// process IQ data if we have it
		if (activeSampleCount)
		{
			// get the phase and differentiate. Result of diff into pBufrB.
			pAtanQI->Atan(pIqDataActive, pBufrA, activeSampleCount);
			DifferentiatePhase(&prevPhase, pBufrA, pBufrB, activeSampleCount);

			// filter the phase. Results in pBufrA.
			pFilterPhase->Filter(pBufrB, pBufrA, activeSampleCount);

			// export for remote diagnostics
			diag.ExportIqPhaseDiff(pBufrA, activeSampleCount, chanNumber);

			// convert phase change to bytes
			pPhaseChangeToBytes->Process(&demodBytesAccum, pBufrA, activeSampleCount);
		}

		// if signal no longer detected at the end of the IQ buffer we were given, 
		// process the bytes we accumulated (if any). Reset phase change to bytes
		// for the next frame. Return the frame.
		PacketDesc_t* pReturnPacketDesc = nullptr;
		if ((sigDetect < SignalDetectThreshold) && demodBytesAccum.size())
		{
			if (demodBytesAccum.size() <= MaxPacketLengthX8)
			{
				// copy to packet descriptor
				std::copy(demodBytesAccum.begin(), demodBytesAccum.end(), pPktBufr);
				pktDesc.pktLengthX8 = demodBytesAccum.size();

				// de-whiten
				dewhitener.Whiten(pPktBufr, pktDesc.pktLengthX8);

				// Once de-whitened, the first byte is the length of the payload. This
				// does not count the length byte itself nor the CRC. The CRC however 
				// does include the length byte. Override the length in the desc and 
				// set the desc pkt ptr to the beginning of the payload.
				pktDesc.pktLengthX8 = pPktBufr[0];
				pktDesc.pPkt = pPktBufr + 1;

				// validate CRC. If good, return the packet
				if (crc.Check(pPktBufr, pktDesc.pktLengthX8 + 1, (pPktBufr[pktDesc.pktLengthX8 + 1] << 8) | pPktBufr[pktDesc.pktLengthX8 + 2]))
				{
					pReturnPacketDesc = &pktDesc;
				}
			}

			// if pReturnPacketDesc is not nullptr the packet is good. If it is
			// nullptr at this point, the packet is bad.
			if (pReturnPacketDesc)
			{
				utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.packetsGood);

				// export for remote diagnostics
				diag.ExportDemodNewPacket(true, chanNumber);
			}
			else
			{
				utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.packetsBad);

				// export for remote diagnostics
				diag.ExportDemodNewPacket(false, chanNumber);
			}

			// cleanup 
			demodBytesAccum.clear();
			pPhaseChangeToBytes->Reset();
		}

		return pReturnPacketDesc;
	}


	/// <summary>	Differentiate the phase and correct for phase reversals. </summary>
	///
	/// <param name="pPrevPhase">	[in,out] The previous phase, will be updated upon return. </param>
	/// <param name="pSrcBufr">  	[in] Ptr to source bufr. </param>
	/// <param name="pDestBufr"> 	[out] Ptr to destination bufr. </param>
	/// <param name="length">	 	The length of the phase data. </param>
	///
	/// <returns>	pDestBufr. </returns>
	float* FskDemodChannel::DifferentiatePhase(float* pPrevPhase, float* pSrcBufr, float* pDestBufr, int length)
	{
		// differentiate, correct for phase reversals
		pDestBufr[0] = CorrectPhaseReversal(pSrcBufr[0] - *pPrevPhase);
		for (int idx = 1; idx < length; idx++)
		{
			pDestBufr[idx] = CorrectPhaseReversal(pSrcBufr[idx] - pSrcBufr[idx - 1]);
		}

		// update prev phase
		*pPrevPhase = pSrcBufr[length - 1];

		return pDestBufr;
	}


	/// <summary>	Correct phase reversal. </summary>
	///
	/// <param name="diffPhase">	The phase difference value. </param>
	///
	/// <returns>	The diffPhase, corrected for phase reversal if necessary. </returns>
	inline float FskDemodChannel::CorrectPhaseReversal(float diffPhase)
	{
		if (diffPhase > 1.5)
		{
			return (diffPhase - Pi);
		}
		else if(diffPhase < -1.5)
		{
			return (Pi + diffPhase);
		}
		else
		{
			return diffPhase;
		}
	}
}