// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FileExport.hpp     		                               :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once
#include <string>
#include <cstdio>
#include "IqPair.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: FileExport will write formatted data to a file. Each action will create a tagged
	/// 	container in the file.
	/// </summary>
	class FileExport
	{
	private:

		/// <summary>
		/// 	(Immutable) The write soft limit. Once we write an amount exceeding this level the export
		/// 	will stop. This is a fast/soft way to limit the file size without calling the OS.
		/// </summary>
		const int					FileWriteSoftLimit = 300 * 1024 * 1024;

		
		/// <summary>	(Immutable) Container marker. </summary>
		static const uint8_t		Marker = '@';


		/// <summary> Container data types. </summary>
		enum ContainerType_t : uint8_t
		{
			Unknown = 0,
			IqPair,
			Float,
			Uint32,
			Uint16,
			Uint8,
			Int32,
			Int16,
			Int8,
			Bool,
			Custom
		};


		/// <summary>
		/// 	The header for every container which describes the type, tag and number of entries in the
		/// 	container.
		/// </summary>
		struct ContainerHeader_t
		{
			ContainerHeader_t(void) {}
			ContainerHeader_t(uint16_t tag, ContainerType_t type, uint32_t count) :
				marker(Marker), tag(tag), type(type), count(count) {}
			uint8_t				marker;
			ContainerType_t		type;
			uint16_t			tag;
			uint32_t			count;
		};


		/// <summary>	Container: IqPairs. </summary>
		struct ContainerDescIqData_t : ContainerHeader_t
		{
			ContainerDescIqData_t(void) = delete;
			ContainerDescIqData_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, IqPair, count) {}
		};


		/// <summary>	Container: Floats. </summary>
		struct ContainerDescFloat_t : ContainerHeader_t
		{
			ContainerDescFloat_t(void) = delete;
			ContainerDescFloat_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Float, count) {}
		};


		/// <summary>	Container: Uint32s. </summary>
		struct ContainerDescUint32_t : ContainerHeader_t
		{
			ContainerDescUint32_t(void) = delete;
			ContainerDescUint32_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Uint32, count) {}
		};


		/// <summary>	Container: Uint16s. </summary>
		struct ContainerDescUint16_t : ContainerHeader_t
		{
			ContainerDescUint16_t(void) = delete;
			ContainerDescUint16_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Uint16, count) {}
		};


		/// <summary>	Container: Uint8s. </summary>
		struct ContainerDescUint8_t : ContainerHeader_t
		{
			ContainerDescUint8_t(void) = delete;
			ContainerDescUint8_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Uint8, count) {}
		};


		/// <summary>	Container: Int32s. </summary>
		struct ContainerDescInt32_t : ContainerHeader_t
		{
			ContainerDescInt32_t(void) = delete;
			ContainerDescInt32_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Int32, count) {}
		};


		/// <summary>	Container: Uint16s. </summary>
		struct ContainerDescInt16_t : ContainerHeader_t
		{
			ContainerDescInt16_t(void) = delete;
			ContainerDescInt16_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Int16, count) {}
		};


		/// <summary>	Container: Int8s. </summary>
		struct ContainerDescInt8_t : ContainerHeader_t
		{
			ContainerDescInt8_t(void) = delete;
			ContainerDescInt8_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Int8, count) {}
		};


		/// <summary>	Container: bool. </summary>
		struct ContainerDescBool_t : ContainerHeader_t
		{
			ContainerDescBool_t(void) = delete;
			ContainerDescBool_t(uint16_t tag, uint32_t count) :
				ContainerHeader_t(tag, Bool, count) {}
		};


		/// <summary>	Container: custom. </summary>
		struct ContainerDescCustom_t : ContainerHeader_t
		{
			ContainerDescCustom_t(void) = delete;
			ContainerDescCustom_t(uint16_t tag, uint32_t size, uint32_t count) :
				ContainerHeader_t(tag, Custom, count), size(size) {}
			uint32_t				size;
		};


	protected:
		std::string					filename;
		FILE*						pFile;
		bool						enabled;
		bool						userStarted;
		int							fileWriteCount;

		inline void					FileWrite(void* pBufr, size_t size, size_t count);
		void						InternalStop(void);

	public:
		FileExport(void) {};
		FileExport(const char* pFilename);
		~FileExport(void);

		void						Filename(const char* pFileName) { filename = std::string(pFileName); }
		std::string					Filename(void) { return filename; }
		bool						Enabled(void) { return enabled; }
		bool						Started(void) { return userStarted; }
		void						Start(void);
		void						Stop(void);
		void						Close(void);
		void						Export(uint16_t tag, IqPair_t data);
		void						Export(uint16_t tag, IqPair_t* pData, int count);
		void						Export(uint16_t tag, float data);
		void						Export(uint16_t tag, float* pData, int count);
		void						Export(uint16_t tag, uint32_t data);
		void						Export(uint16_t tag, uint32_t* pData, int count);
		void						Export(uint16_t tag, uint16_t data);
		void						Export(uint16_t tag, uint16_t* pData, int count);
		void						Export(uint16_t tag, uint8_t data);
		void						Export(uint16_t tag, uint8_t* pData, int count);
		void						Export(uint16_t tag, int32_t data);
		void						Export(uint16_t tag, int32_t* pData, int count);
		void						Export(uint16_t tag, int16_t data);
		void						Export(uint16_t tag, int16_t* pData, int count);
		void						Export(uint16_t tag, int8_t data);
		void						Export(uint16_t tag, int8_t* pData, int count);
		void						Export(uint16_t tag, bool data);
		void						Export(uint16_t tag, bool* pData, int count);
		void						Export(uint16_t tag, void* pCustom, int size);
		void						Export(uint16_t tag, void* pCustom, int size, int count);
	};
}