// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemIpcQueues.cpp                                  :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Implements and manages IPC queues located in shared memory. This class
//      is intended to be derived only, with the superclass being either the
//      owner or the client.
//
// -----------------------------------------------------------------------------

#include "FskModemIpcQueues.hpp"

using namespace boost::interprocess;

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Returns the block byte size required for the queues, based on the capacity. This does not
	/// 	alter any configuration or settings.
	/// </summary>
	///
	/// <param name="qCommandCapacity"> 	The command queue capacity. </param>
	/// <param name="qResponseCapacity">	The response queue capacity. </param>
	/// <param name="qPktCapacity">			The capacity of each packet queue. </param>
	/// <param name="qIqFrameCapacity"> 	The capacity of each IQ frame queue. </param>
	///
	/// <returns>	The block size required, in bytes. </returns>
	int	FskModemIpcQueues::GetBlockSizeX8(int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity)
	{
		return sizeof(SharedMemoryBlock_t) + (qCommandCapacity * sizeof(SharedMemoryQueueCommandEntry_t)) +
			(qResponseCapacity * sizeof(SharedMemoryQueueResponseEntry_t)) +
			((2 * (qPktCapacity + qIqFrameCapacity)) * sizeof(SharedMemoryQueueModDemodEntry_t));
	}


	/// <summary>	Creates the shared memory queues at the given location. </summary>
	///
	/// <param name="pSharedMem">			[in,out] If non-null, the shared memory. </param>
	/// <param name="qCommandCapacity"> 	The command queue capacity. </param>
	/// <param name="qResponseCapacity">	The response queue capacity. </param>
	/// <param name="qPktCapacity">			The capacity of each packet queue. </param>
	/// <param name="qIqFrameCapacity"> 	The capacity of each IQ frame queue. </param>
	///
	/// <returns>	0 if success, -EFAULT if pSharedMem is NULL or qCapacity is too big. </returns>
	int FskModemIpcQueues::Create(void* pSharedMem, int qCommandCapacity, int qResponseCapacity, int qPktCapacity, int qIqFrameCapacity)
	{
		// null check & capacity check
		if ((!pSharedMem) || 
			(qCommandCapacity > MaxCommandQCapacity) || (qResponseCapacity > MaxResponseQCapacity),
			(qPktCapacity > MaxModDemodQCapacity) || (qIqFrameCapacity > MaxModDemodQCapacity))
		{
			return -EFAULT;
		}

		// instantiate the shared memory structure in the shared mem.
		pShmBlock = new (pSharedMem) SharedMemoryBlock_t(qCommandCapacity, qResponseCapacity, qPktCapacity, qIqFrameCapacity);

		// configure localized qPtrTable
		qPtrTable[QueueId_t::CommandsToModem] = &pShmBlock->qCommandsToModem;
		qPtrTable[QueueId_t::ResponsesFromModem] = &pShmBlock->qResponsesFromModem;
		qPtrTable[QueueId_t::DemodIqFramesToModem] = &pShmBlock->qDemodIqFramesToModem;
		qPtrTable[QueueId_t::DemodPktsFromModem] = &pShmBlock->qDemodPktsFromModem;
		qPtrTable[QueueId_t::ModPktsToModem] = &pShmBlock->qModPktsToModem;
		qPtrTable[QueueId_t::ModIqFramesFromModem] = &pShmBlock->qModIqFramesFromModem;

		// configure the qOffsX8Table in shared mem, we cannot store pointers in the shared memory as they
		// will/can be different for each process.
		pShmBlock->qOffsX8Table[QueueId_t::CommandsToModem] = (uint8_t*)qPtrTable[QueueId_t::CommandsToModem] - (uint8_t*)pShmBlock;
		pShmBlock->qOffsX8Table[QueueId_t::ResponsesFromModem] = (uint8_t*)qPtrTable[QueueId_t::ResponsesFromModem] - (uint8_t*)pShmBlock;
		pShmBlock->qOffsX8Table[QueueId_t::DemodIqFramesToModem] = (uint8_t*)qPtrTable[QueueId_t::DemodIqFramesToModem] - (uint8_t*)pShmBlock;
		pShmBlock->qOffsX8Table[QueueId_t::DemodPktsFromModem] = (uint8_t*)qPtrTable[QueueId_t::DemodPktsFromModem] - (uint8_t*)pShmBlock;
		pShmBlock->qOffsX8Table[QueueId_t::ModPktsToModem] = (uint8_t*)qPtrTable[QueueId_t::ModPktsToModem] - (uint8_t*)pShmBlock;
		pShmBlock->qOffsX8Table[QueueId_t::ModIqFramesFromModem] = (uint8_t*)qPtrTable[QueueId_t::ModIqFramesFromModem] - (uint8_t*)pShmBlock;

		// configure the localized entry array pointers for eqch Q. They begin right after SharedMemoryBlock_t.
		uint8_t* pQEntry = (uint8_t*)(((uint8_t*)pSharedMem) + sizeof(SharedMemoryBlock_t));
		qEntryArrayPtrTable[QueueId_t::CommandsToModem] = (SharedMemoryQueueEntry_t*)pQEntry;
		pQEntry += qCommandCapacity * sizeof(SharedMemoryQueueCommandEntry_t);
		qEntryArrayPtrTable[QueueId_t::ResponsesFromModem] = (SharedMemoryQueueEntry_t*)pQEntry;
		pQEntry += qResponseCapacity * sizeof(SharedMemoryQueueResponseEntry_t);
		qEntryArrayPtrTable[QueueId_t::DemodIqFramesToModem] = (SharedMemoryQueueEntry_t*)pQEntry;
		pQEntry += qIqFrameCapacity * sizeof(SharedMemoryQueueModDemodEntry_t);
		qEntryArrayPtrTable[QueueId_t::DemodPktsFromModem] = (SharedMemoryQueueEntry_t*)pQEntry;
		pQEntry += qPktCapacity * sizeof(SharedMemoryQueueModDemodEntry_t);
		qEntryArrayPtrTable[QueueId_t::ModPktsToModem] = (SharedMemoryQueueEntry_t*)pQEntry;
		pQEntry += qPktCapacity * sizeof(SharedMemoryQueueModDemodEntry_t);
		qEntryArrayPtrTable[QueueId_t::ModIqFramesFromModem] = (SharedMemoryQueueEntry_t*)pQEntry;

		// configure the offsX8EntryArray for each Q in shared mem, we cannot store pointers in the shared memory as they
		// will/can be different for each process.
		qPtrTable[QueueId_t::CommandsToModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::CommandsToModem] - (uint8_t*)pShmBlock;
		qPtrTable[QueueId_t::ResponsesFromModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::ResponsesFromModem] - (uint8_t*)pShmBlock;
		qPtrTable[QueueId_t::DemodIqFramesToModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::DemodIqFramesToModem] - (uint8_t*)pShmBlock;
		qPtrTable[QueueId_t::DemodPktsFromModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::DemodPktsFromModem] - (uint8_t*)pShmBlock;
		qPtrTable[QueueId_t::ModPktsToModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::ModPktsToModem] - (uint8_t*)pShmBlock;
		qPtrTable[QueueId_t::ModIqFramesFromModem]->offsX8EntryArray = (uint8_t*)qEntryArrayPtrTable[QueueId_t::ModIqFramesFromModem] - (uint8_t*)pShmBlock;

		// success
		return 0;
	}


	/// <summary>	Attaches the given shared memory queues. </summary>
	///
	/// <param name="pSharedMem">	[in,out] If non-null, the shared memory. </param>
	///
	/// <returns>
	/// 	0 if success, -EFAULT if pSharedMem is NULL or -ENOKEY if the sync word is not found.
	/// </returns>
	int	FskModemIpcQueues::Attach(void* pSharedMem)
	{
		// null check & capacity check
		if (!pSharedMem)
		{
			return -EFAULT;
		}

		// set the ptr
		pShmBlock = (SharedMemoryBlock_t*)pSharedMem;

		// verify the sync word
		if (pShmBlock->syncWord != SyncWord)
		{
			return -ENOKEY;
		}

		// configure localized qPtrTable and pointers for each Q
		for (int idx = 0; idx < (int)QueueId_t::QueueCount; idx++)
		{
			qPtrTable[idx] = (SharedMemoryQueue_t*)((uint8_t*)pShmBlock + pShmBlock->qOffsX8Table[idx]);
			qEntryArrayPtrTable[idx] = (SharedMemoryQueueEntry_t*)((uint8_t*)pShmBlock + qPtrTable[idx]->offsX8EntryArray);
		}

		// success
		return 0;
	}


	/// <summary>	Get the service version. </summary>
	///
	/// <returns>	The version string. </returns>
	std::string FskModemIpcQueues::GetVersion(void)
	{
		// copy
		return std::string(pShmBlock->version);
	}


	/// <summary>	Sets the service's version. </summary>
	///
	/// <param name="version">	[in] Ref to the version string. </param>
	void FskModemIpcQueues::SetVersion(std::string& version)
	{
		// copy, force null term incase version string is bigger than our buffer
		strncpy(pShmBlock->version, version.c_str(), MaxStringLengthX8);
		pShmBlock->version[MaxStringLengthX8 - 1] = '\0';
	}


	/// <summary>	Get the connection's version. </summary>
	///
	/// <returns>	The version string. </returns>
	std::string FskModemIpcQueues::GetConnectionVersion(void)
	{
		// copy
		return std::string(pShmBlock->connectionVersion);
	}


	/// <summary>	Sets the connection's version. </summary>
	///
	/// <param name="version">	[in] Ref to the version string. </param>
	void FskModemIpcQueues::SetConnectionVersion(std::string& version)
	{
		// copy, force null term incase version string is bigger than our buffer
		strncpy(pShmBlock->connectionVersion, version.c_str(), MaxStringLengthX8);
		pShmBlock->version[MaxStringLengthX8 - 1] = '\0';
	}


	/// <summary>	Check queue for freespace. </summary>
	///
	/// <param name="qId"> 	The Q identifier. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	0 if freespace for an entry. -EAGAIN if waiting but none after timeout. -ENOMEM if not
	/// 	waiting and none.
	/// </returns>
	int FskModemIpcQueues::CheckQFreespace(QueueId_t qId, bool wait)
	{
		// wait or return with error based on wait flag when no freespace.
		if (wait)
		{
			// every second this will return to allow the calling thread to decide if it wants to keep waiting.
			if (!qPtrTable[qId]->semFreespace.timed_wait(boost::interprocess::microsec_clock::universal_time() +
				boost::posix_time::milliseconds(WaitTimeoutMs)))
			{
				return -EAGAIN;
			}
		}
		else if (!qPtrTable[qId]->semFreespace.try_wait())
		{
			return -ENOMEM;
		}
		return 0;
	}


	/// <summary>	Check queue for an entry available. </summary>
	///
	/// <param name="qId"> 	The Q identifier. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	0 if an entry is available. -EAGAIN if none (with or without waiting / timeout).
	/// </returns>
	int FskModemIpcQueues::CheckQAvailable(QueueId_t qId, bool wait)
	{
		// wait or return with 'try again' based on wait flag when nothing available.
		if (wait)
		{
			// every second this will return to allow the calling thread to decide if it wants to keep waiting.
			if (!qPtrTable[qId]->semAvailable.timed_wait(boost::interprocess::microsec_clock::universal_time() + 
				boost::posix_time::milliseconds(WaitTimeoutMs)))
			{
				return -EAGAIN;
			}
		}
		else if (!qPtrTable[qId]->semAvailable.try_wait())
		{
			return -EAGAIN;
		}
		return 0;
	}


	/// <summary>	Adds an entry to the command Q. This call can block or poll. </summary>
	///
	/// <param name="cmd"> 	[in] Ref of the command to add. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, or -EAGAIN after timeout.
	/// </returns>
	int FskModemIpcQueues::AddCommandEntry(user_api::SvcFskModemCommand_t& cmd, bool wait)
	{
		// check for freespace
		int retn;
		if ((retn = CheckQFreespace(QueueId_t::CommandsToModem)) < 0) return retn;

		// get the next entry
		SharedMemoryQueueCommandEntry_t* pEntry = 
			&((SharedMemoryQueueCommandEntry_t*) qEntryArrayPtrTable[QueueId_t::CommandsToModem])[qPtrTable[QueueId_t::CommandsToModem]->wrIndex];

		// move the contents
		memcpy((void*)&pEntry->cmd, &cmd, sizeof(user_api::SvcFskModemCommand_t));
		pEntry->tag = (uint16_t) -1;

		// commit and post
		qPtrTable[QueueId_t::CommandsToModem]->wrIndex = (qPtrTable[QueueId_t::CommandsToModem]->wrIndex + 1) % 
			qPtrTable[QueueId_t::CommandsToModem]->capacity;
		qPtrTable[QueueId_t::CommandsToModem]->semAvailable.post();

		// success
		return 0;
	}


	/// <summary>	Gets the next entry from the command Q. This call can block or poll. </summary>
	///
	/// <param name="cmd"> 	[out] Ref for the command. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int FskModemIpcQueues::GetNextCommandEntry(user_api::SvcFskModemCommand_t& cmd, bool wait)
	{
		// check for available entry
		int retn;
		if ((retn = CheckQAvailable(QueueId_t::CommandsToModem, wait)) < 0) return retn;

		// get the next availble entry
		SharedMemoryQueueCommandEntry_t* pEntry = 
			&((SharedMemoryQueueCommandEntry_t*) qEntryArrayPtrTable[QueueId_t::CommandsToModem])[qPtrTable[QueueId_t::CommandsToModem]->rdIndex];

		// move the contents
		memcpy(&cmd, (void*)&pEntry->cmd, sizeof(user_api::SvcFskModemCommand_t));

		// consume
		qPtrTable[QueueId_t::CommandsToModem]->rdIndex =
			(qPtrTable[QueueId_t::CommandsToModem]->rdIndex + 1) % qPtrTable[QueueId_t::CommandsToModem]->capacity;
		qPtrTable[QueueId_t::CommandsToModem]->semFreespace.post();

		// success
		return 0;
	}


	/// <summary>	Adds an entry to the response Q. This call can block or poll. </summary>
	///
	/// <param name="resp">	[in] Ref of the response to add. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout.
	/// </returns>
	int FskModemIpcQueues::AddResponseEntry(user_api::SvcFskModemResponse_t& resp, bool wait)
	{
		// check for freespace
		int retn;
		if ((retn = CheckQFreespace(QueueId_t::ResponsesFromModem)) < 0) return retn;

		// get the next entry
		SharedMemoryQueueResponseEntry_t* pEntry =
			&((SharedMemoryQueueResponseEntry_t*) qEntryArrayPtrTable[QueueId_t::ResponsesFromModem])[qPtrTable[QueueId_t::ResponsesFromModem]->wrIndex];

		// move the contents
		memcpy((void*)&pEntry->resp, &resp, sizeof(user_api::SvcFskModemResponse_t));
		pEntry->tag = (uint16_t)-1;

		// commit and post
		qPtrTable[QueueId_t::ResponsesFromModem]->wrIndex = (qPtrTable[QueueId_t::ResponsesFromModem]->wrIndex + 1) %
			qPtrTable[QueueId_t::ResponsesFromModem]->capacity;
		qPtrTable[QueueId_t::ResponsesFromModem]->semAvailable.post();

		// success
		return 0;
	}


	/// <summary>	Gets the next entry from the response Q. This call can block or poll. </summary>
	///
	/// <param name="resp">	[out] Ref for the response. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int FskModemIpcQueues::GetNextResponseEntry(user_api::SvcFskModemResponse_t& resp, bool wait)
	{
		// check for available entry
		int retn;
		if ((retn = CheckQAvailable(QueueId_t::ResponsesFromModem, wait)) < 0) return retn;

		// get the next availble entry
		SharedMemoryQueueResponseEntry_t* pEntry =
			&((SharedMemoryQueueResponseEntry_t*) qEntryArrayPtrTable[QueueId_t::ResponsesFromModem])[qPtrTable[QueueId_t::ResponsesFromModem]->rdIndex];

		// move the contents
		memcpy(&resp, (void*)&pEntry->resp, sizeof(user_api::SvcFskModemResponse_t));

		// consume
		qPtrTable[QueueId_t::ResponsesFromModem]->rdIndex =
			(qPtrTable[QueueId_t::ResponsesFromModem]->rdIndex + 1) % qPtrTable[QueueId_t::ResponsesFromModem]->capacity;
		qPtrTable[QueueId_t::ResponsesFromModem]->semFreespace.post();

		// success
		return 0;
	}


	/// <summary>
	/// 	Adds an entry to the mod/demod Q specified by qId. This call can block or poll.
	/// </summary>
	///
	/// <param name="qId">	   	The Q identifier. </param>
	/// <param name="pData">   	[in] Ptr to the data to add. </param>
	/// <param name="lengthX8">	The length of the data in bytes. </param>
	/// <param name="wait">	   	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout.
	/// </returns>
	int	FskModemIpcQueues::AddModDemodEntry(QueueId_t qId, void* pData, uint16_t lengthX8, bool wait)
	{
		return AddModDemodEntry(qId, pData, lengthX8, 0, wait);
	}


	/// <summary>
	/// 	Adds an entry to the mod/demod Q specified by qId. This call can block or poll.
	/// </summary>
	///
	/// <param name="qId">	   	The Q identifier. </param>
	/// <param name="pData">   	[in] Ptr to the data to add. </param>
	/// <param name="lengthX8">	The length of the data in bytes. </param>
	/// <param name="tag">	   	The tag to pass with the entry. </param>
	/// <param name="wait">	   	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout.
	/// </returns>
	int	FskModemIpcQueues::AddModDemodEntry(QueueId_t qId, void* pData, uint16_t lengthX8, uint16_t tag, bool wait)
	{
		// check parameters
		if (!pData || (lengthX8 > MaxModDemodQEntrySizeX8) || !((qId >= FirstModDemodQId) && (qId <= LastModDemodQId)))
		{
			return -EFAULT;
		}

		// check for freespace
		int retn;
		if ((retn = CheckQFreespace(qId)) < 0) return retn;

		// get the next entry
		SharedMemoryQueueModDemodEntry_t* pEntry = &((SharedMemoryQueueModDemodEntry_t*) qEntryArrayPtrTable[qId])[qPtrTable[qId]->wrIndex];

		// move the data and set length
		memcpy((void*)pEntry->data, pData, lengthX8);
		pEntry->lengthX8 = lengthX8;
		pEntry->tag = tag;

		// commit and post
		qPtrTable[qId]->wrIndex = (qPtrTable[qId]->wrIndex + 1) % qPtrTable[qId]->capacity;
		qPtrTable[qId]->semAvailable.post();

		// success
		return 0;
	}


	/// <summary>
	/// 	Gets the next entry from the mod/demod Q specified by qId. This call can block or poll.
	/// </summary>
	///
	/// <param name="qId">		  	The Q identifier. </param>
	/// <param name="pData">	  	[out] Ptr to the location to put the data. </param>
	/// <param name="maxLengthX8">	Max length to return in bytes. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout, -E2BIG if the entry length exceeds the max length, or -EFAULT
	/// 	if bad  parameters.
	/// </returns>
	int	FskModemIpcQueues::GetNextModDemodEntry(QueueId_t qId, void* pData, uint16_t maxLengthX8, bool wait)
	{
		uint16_t	discardTag;
		return GetNextModDemodEntry(qId, pData, maxLengthX8, &discardTag, wait);
	}


	/// <summary>
	/// 	Gets the next entry from the mod/demod Q specified by qId. This call can block or poll.
	/// </summary>
	///
	/// <param name="qId">		  	The Q identifier. </param>
	/// <param name="pData">	  	[out] Ptr to the location to put the data. </param>
	/// <param name="maxLengthX8">	Max length to return in bytes. </param>
	/// <param name="pTag">		  	[out] Ptr to store the tag. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout, -E2BIG if the entry length exceeds the max length, or -EFAULT
	/// 	if bad  parameters.
	/// </returns>
	int	FskModemIpcQueues::GetNextModDemodEntry(QueueId_t qId, void* pData, uint16_t maxLengthX8, uint16_t* pTag, bool wait)
	{
		// check parameters
		if (!pData || !((qId >= FirstModDemodQId) && (qId <= LastModDemodQId)) || !pTag)
		{
			return -EFAULT;
		}

		// check for available entry
		int retn;
		if ((retn = CheckQAvailable(qId, wait)) < 0) return retn;

		// get the next availble entry
		SharedMemoryQueueModDemodEntry_t* pEntry = &((SharedMemoryQueueModDemodEntry_t*) qEntryArrayPtrTable[qId])[qPtrTable[qId]->rdIndex];

		// move the data if it will fit
		if (pEntry->lengthX8 <= maxLengthX8)
		{
			memcpy(pData, (void*)pEntry->data, pEntry->lengthX8);

			// success, return the length
			retn = pEntry->lengthX8;
		}
		else
		{
			retn = -E2BIG;
		}

		// move the tag
		*pTag = pEntry->tag;

		// consume
		qPtrTable[qId]->rdIndex = (qPtrTable[qId]->rdIndex + 1) % qPtrTable[qId]->capacity;
		qPtrTable[qId]->semFreespace.post();

		// return val
		return retn;
	}
}