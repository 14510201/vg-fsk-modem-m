// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemApp.hpp                                        :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <cstring>
#include "StreamLog.hpp"
#include "AppConfiguration.hpp"
#include "FskModemApp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem service container. </summary>
	class FskModemApp
	{
	public:

		/// <summary>	(Immutable) General constants. </summary>
		static const std::string			AppName;
		static const std::string			AppVersion;
		static const std::string			AppConnectionName;
		static const std::uint16_t			AppConnectionCommandQCapacity;
		static const std::uint16_t			AppConnectionResponseQCapacity;
		static const std::uint16_t			AppConnectionPktQCapacity;
		static const std::uint16_t			AppConnectionIqFrameQCapacity;

		log::StreamLog						log;
		AppConfiguration*					pAppCfg;

	public:
		FskModemApp(void);
		~FskModemApp(void);
		void								Configure(void);
		void								ReportActiveConfiguration(void);
	};
}