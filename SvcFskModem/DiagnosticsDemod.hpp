// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : DiagnosticsDemod.hpp                                   :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file - Diagnostics for Demod
//
// -----------------------------------------------------------------------------

#pragma once
#include <cstdio>
#include "FileExport.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: Extend FileExport to write formatted data to a file with demod data. Each action 
	///     will create a taggedcontainer in the file.
	/// </summary>
	class DiagnosticsDemod : protected FileExport
	{
	private:

		/// <summary> Diag types. </summary>
		enum DiagType_t : uint8_t
		{
			Unknown = 0,
			DemodNewPacket,
			IqOriginal,
			IqMixed,
			IqAntiImage,
			PhaseDiff,
			SampleMarker,
			PdDecisionThreshold
		};

	public:
		DiagnosticsDemod(void) {}
		DiagnosticsDemod(const char* pFilename) : FileExport(pFilename) {}
		~DiagnosticsDemod(void) {}

		void						Filename(const char* pFileName) { FileExport::Filename(pFileName); }
		std::string					Filename(void) { return FileExport::Filename(); }
		bool						Enabled(void) { return FileExport::Enabled(); }
		bool						Started(void) { return FileExport::Started(); }
		void						Start(void) { FileExport::Start(); }
		void						Stop(void) { FileExport::Stop(); }
		void						Close(void) { FileExport::Close(); }


		/// <summary>	Export iq original. </summary>
		///
		/// <param name="pData">	[in] Ptr to the IQ data. </param>
		/// <param name="count">	Number of IQ data entries. </param>
		void						ExportIqOriginal(IqPair_t* pData, int count) { Export(CreateTag(IqOriginal, 0xff), pData, count); }


		/// <summary>	Export iq mixed. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the IQ data. </param>
		/// <param name="count">	 	Number of IQ data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqMixed(IqPair_t* pData, int count, uint8_t chanNumber) { Export(CreateTag(IqMixed, chanNumber), pData, count); }


		/// <summary>	Export iq anti image. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the IQ data. </param>
		/// <param name="count">	 	Number of IQ data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqAntiImage(IqPair_t* pData, int count, uint8_t chanNumber) { Export(CreateTag(IqAntiImage, chanNumber), pData, count); }


		/// <summary>	Export iq phase difference. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the data. </param>
		/// <param name="count">	 	Number of data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqPhaseDiff(float* pData, int count, uint8_t chanNumber) { Export(CreateTag(PhaseDiff, chanNumber), pData, count); }


		/// <summary>	Export iq sample marker. </summary>
		///
		/// <param name="location">  	The location value. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqSampleMarker(uint16_t location, uint8_t chanNumber) { Export(CreateTag(SampleMarker, chanNumber), location); }


		/// <summary>	Export iq pd decision threshold. </summary>
		///
		/// <param name="thresh">	 	The threshold value. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqPdDecisionThreshold(float thresh, uint8_t chanNumber) { Export(CreateTag(PdDecisionThreshold, chanNumber), thresh); }


		/// <summary>	Export demod new packet. </summary>
		///
		/// <param name="isGood">	 	True if the packet is good, false if not. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportDemodNewPacket(bool isGood, uint8_t chanNumber) { Export(CreateTag(DemodNewPacket, chanNumber), isGood); }

	protected:

		/// <summary>	Creates a tag. </summary>
		///
		/// <param name="type">	The type. </param>
		/// <param name="chan">	The channel. </param>
		///
		/// <returns>	The tag value. </returns>
		inline uint16_t				CreateTag(DiagType_t type, uint8_t chan) { return (((uint16_t)chan << 8) | type); }
	};
}