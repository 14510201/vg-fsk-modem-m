// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemApp.cpp                                        :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the base application / service support and context.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <ostream>
#include <cstring>
#include <boost/format.hpp>
#include "FskModemApp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	(Immutable) contants. </summary>
	const std::string FskModemApp::AppName = "SvcFskModem";
	const std::string FskModemApp::AppVersion = "1.05.00";
	const std::string FskModemApp::AppConnectionName = "svcfskmodem";
	const std::uint16_t FskModemApp::AppConnectionCommandQCapacity = 4;
	const std::uint16_t FskModemApp::AppConnectionResponseQCapacity = 8;
	const std::uint16_t FskModemApp::AppConnectionPktQCapacity = 128;
	const std::uint16_t FskModemApp::AppConnectionIqFrameQCapacity = 512;


	/// <summary>	Default constructor. </summary>
	FskModemApp::FskModemApp(void)
	{
		pAppCfg = nullptr;
	}


	/// <summary>	Destructor. </summary>
	FskModemApp::~FskModemApp(void)
	{
		// flush log
		log << std::flush;
	}


	/// <summary>	Configures the app. </summary>
	void FskModemApp::Configure(void)
	{
		// instantiate the configuration
		pAppCfg = new AppConfiguration();

		// report the active config
		ReportActiveConfiguration();
	}


	/// <summary>	Reports active configuration to the log. </summary>
	void FskModemApp::ReportActiveConfiguration(void)
	{
		std::string cfgName;
		std::string cfgDesc;
		user_api::ModulationConfiguration_t modCfg;
		pAppCfg->Configuration(cfgName, cfgDesc, modCfg);
		auto logLines = str(boost::format("Active configuration: %1%\n\t%2%\n\tMod BT = %3%") % cfgName % cfgDesc % modCfg.pulseShapingBt);
		log.LockedLog() << logLines << std::endl;
	}
}