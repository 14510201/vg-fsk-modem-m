// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : DiagnosticsMod.hpp                                     :
// Date Created       : 2024 JAN 11                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file - Diagnostics for Mod
//
// -----------------------------------------------------------------------------

#pragma once
#include <cstdio>
#include "FileExport.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: Extend FileExport to write formatted data to a file with mod data. Each action 
	///     will create a taggedcontainer in the file.
	/// </summary>
	class DiagnosticsMod : protected FileExport
	{
	private:

		/// <summary> Diag types. </summary>
		enum DiagType_t : uint8_t
		{
			Unknown = 0,
			Rates,
			ModNewPacket,
			EncapsulatedPacket,
			PulseShaping,
			IqPreDecimation,
			IqGenerated,
			IqChannelMixed,
			IqCombined
		};


		/// <summary>	Rates data format. </summary>
		struct Rates_t
		{
			Rates_t(void) = delete;
			Rates_t(float freqSample, float bitrateFsk, uint8_t upsample) :
				freqSample(freqSample), bitrateFsk(bitrateFsk), upsample(upsample) {}
			float					freqSample;
			float					bitrateFsk;
			uint8_t					upsample;
		};


	public:
		DiagnosticsMod(void) {}
		DiagnosticsMod(const char* pFilename) : FileExport(pFilename) {}
		~DiagnosticsMod(void) {}

		void						Filename(const char* pFileName) { FileExport::Filename(pFileName); }
		std::string					Filename(void) { return FileExport::Filename(); }
		bool						Enabled(void) { return FileExport::Enabled(); }
		bool						Started(void) { return FileExport::Started(); }
		void						Start(void) { FileExport::Start(); }
		void						Stop(void) { FileExport::Stop(); }
		void						Close(void) { FileExport::Close(); }


		/// <summary>	Export rates. </summary>
		///
		/// <param name="freqSample">	The location value. </param>
		/// <param name="bitrateFsk">	The FSK bitrate. </param>
		/// <param name="upsample">  	The upsample factor. </param>
		void						ExportRates(float freqSample, float bitrateFsk, uint8_t upsample)
		{ 
			Rates_t data = Rates_t(freqSample, bitrateFsk, upsample);
			Export(CreateTag(Rates, 0xff), (void*) &data, sizeof(Rates_t));
		}


		/// <summary>	Export mod new packet. </summary>
		///
		/// <param name="pData">	  	[in] Ptr to the packet data. </param>
		/// <param name="lengthX8">		Packet length in bytes. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportModNewPacket(uint8_t* pData, int lengthX8, uint8_t chanNumber) { Export(CreateTag(ModNewPacket, chanNumber), pData, lengthX8); }


		/// <summary>	Export encapsulated packet. </summary>
		///
		/// <param name="pData">	  	[in] Ptr to the packet data. </param>
		/// <param name="lengthX8">		Packet length in bytes. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportEncapsulatedPacket(uint8_t* pData, int lengthX8, uint8_t chanNumber) { Export(CreateTag(EncapsulatedPacket, chanNumber), pData, lengthX8); }


		/// <summary>	Export pulse shaping. </summary>
		///
		/// <param name="pData">	  	[in] Ptr to the data. </param>
		/// <param name="count">		Number of data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportPulseShaping(float* pData, int count, uint8_t chanNumber) { Export(CreateTag(PulseShaping, chanNumber), pData, count); }


		/// <summary>	Export iq pre-decimated. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the IQ data. </param>
		/// <param name="count">	 	Number of IQ data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqPreDecimation(IqPair_t* pData, int count, uint8_t chanNumber) { Export(CreateTag(IqPreDecimation, chanNumber), pData, count); }


		/// <summary>	Export iq generated. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the IQ data. </param>
		/// <param name="count">	 	Number of IQ data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqGenerated(IqPair_t* pData, int count, uint8_t chanNumber) { Export(CreateTag(IqGenerated, chanNumber), pData, count); }


		/// <summary>	Export iq channel mixed. </summary>
		///
		/// <param name="pData">	 	[in] Ptr to the IQ data. </param>
		/// <param name="count">	 	Number of IQ data entries. </param>
		/// <param name="chanNumber">	The channel number. </param>
		void						ExportIqChannelMixed(IqPair_t* pData, int count, uint8_t chanNumber) { Export(CreateTag(IqChannelMixed, chanNumber), pData, count); }

		/// <summary>	Export iq channel combined. </summary>
		///
		/// <param name="pData">	[in] Ptr to the IQ data. </param>
		/// <param name="count">	Number of IQ data entries. </param>
		void						ExportIqCombined(IqPair_t* pData, int count) { Export(CreateTag(IqCombined, 0xff), pData, count); }

	protected:

		/// <summary>	Creates a tag. </summary>
		///
		/// <param name="type">	The type. </param>
		/// <param name="chan">	The channel. </param>
		///
		/// <returns>	The tag value. </returns>
		inline uint16_t				CreateTag(DiagType_t type, uint8_t chan) { return (((uint16_t)chan << 8) | type); }
	};
}