// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemEvents.hpp				     				   :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <mutex>
#include <queue>
#include "SvcFskModemCmdRespApi.hpp"

namespace svc_fsk_modem
{
	namespace events
	{
		/// <summary>	Enum: Event types. </summary>
		enum EventType_t
		{
			Empty = 0,
			StatusRequest,
			ControlsRequest,
			ConfigurationChanged,
			ModConfigured,
			ModControlsUpdate,
			ModDiagnosticsUpdate,
			ModDiagnosticsUpdateComplete,
			ModStatusReport,
			DemodConfigured,
			DemodDiagnosticsUpdate,
			DemodDiagnosticsUpdateComplete,
			DemodStatusReport
		};


		/// <summary>	Struct: Event. </summary>
		struct Event_t
		{
			Event_t(void) : type(EventType_t::Empty) {}
			Event_t(EventType_t type) : type(type) {}
			bool Valid(void) { return (type != EventType_t::Empty); }
			EventType_t					type;
		};


		/// <summary>	Struct: Event : Status Request. </summary>
		struct EventStatusRequest_t : Event_t
		{
			EventStatusRequest_t(void) : Event_t(EventType_t::StatusRequest) {}
			EventStatusRequest_t(bool clearCounters) 
				: Event_t(EventType_t::StatusRequest), clearCounters(clearCounters) {}
			bool										clearCounters;
		};


		/// <summary>	Struct: Event : Controls Request. </summary>
		struct EventControlsRequest_t : Event_t
		{
			EventControlsRequest_t(void) : Event_t(EventType_t::ControlsRequest) {}
		};


		/// <summary>	Struct: Event : Configuration Changed. </summary>
		struct EventConfigurationChanged_t : Event_t
		{
			EventConfigurationChanged_t(void) : Event_t(EventType_t::ConfigurationChanged) {}
		};


		/// <summary>	Struct: Event : Mod Configured. </summary>
		struct EventModConfigured_t : Event_t
		{
			EventModConfigured_t(void) : Event_t(EventType_t::ModConfigured) {}
		};


		/// <summary>	Struct: Event : Mod Diagnostics Update. </summary>
		struct EventModDiagnosticsUpdate_t : Event_t
		{
			EventModDiagnosticsUpdate_t(void) : Event_t(EventType_t::ModDiagnosticsUpdate) {}
			EventModDiagnosticsUpdate_t(user_api::ModulationDiagnostics_t diags)
				: Event_t(EventType_t::ModDiagnosticsUpdate), diags(diags) {}
			user_api::ModulationDiagnostics_t			diags;
		};


		/// <summary>	Struct: Event : Mod Diagnostics Update complete. </summary>
		struct EventModDiagnosticsUpdateComplete_t : Event_t
		{
			EventModDiagnosticsUpdateComplete_t(void) : Event_t(EventType_t::ModDiagnosticsUpdateComplete) {}
		};


		/// <summary>	Struct: Event : Mod Controls Update. </summary>
		struct EventModControlsUpdate_t : Event_t
		{
			EventModControlsUpdate_t(void) : Event_t(EventType_t::ModControlsUpdate) {}
			EventModControlsUpdate_t(user_api::ModulationControls_t controls) 
				: Event_t(EventType_t::ModControlsUpdate), controls(controls) {}
			user_api::ModulationControls_t				controls;
		};


		/// <summary>	Struct: Event : Mod Status Report. </summary>
		struct EventModStatusReport_t : Event_t
		{
			EventModStatusReport_t(void) : Event_t(EventType_t::ModStatusReport) {}
			EventModStatusReport_t(bool countersCleared, user_api::ModulationStatus_t status) 
				: Event_t(EventType_t::ModStatusReport), countersCleared(countersCleared), status(status) {}
			bool										countersCleared;
			user_api::ModulationStatus_t				status;
		};


		/// <summary>	Struct: Event : Demod Configured. </summary>
		struct EventDemodConfigured_t : Event_t
		{
			EventDemodConfigured_t(void) : Event_t(EventType_t::DemodConfigured) {}
		};


		/// <summary>	Struct: Event : Demod Diagnostics Update. </summary>
		struct EventDemodDiagnosticsUpdate_t : Event_t
		{
			EventDemodDiagnosticsUpdate_t(void) : Event_t(EventType_t::DemodDiagnosticsUpdate) {}
			EventDemodDiagnosticsUpdate_t(user_api::DemodulationDiagnostics_t diags)
				: Event_t(EventType_t::DemodDiagnosticsUpdate), diags(diags) {}
			user_api::DemodulationDiagnostics_t			diags;
		};


		/// <summary>	Struct: Event : Demod Diagnostics Update complete. </summary>
		struct EventDemodDiagnosticsUpdateComplete_t : Event_t
		{
			EventDemodDiagnosticsUpdateComplete_t(void) : Event_t(EventType_t::DemodDiagnosticsUpdateComplete) {}
		};


		/// <summary>	Struct: Event : Demod Status Report. </summary>
		struct EventDemodStatusReport_t : Event_t
		{
			EventDemodStatusReport_t(void) : Event_t(EventType_t::DemodStatusReport) {}
			EventDemodStatusReport_t(bool countersCleared, user_api::DemodulationStatus_t status)
				: Event_t(EventType_t::DemodStatusReport), countersCleared(countersCleared), status(status) {}
			bool										countersCleared;
			user_api::DemodulationStatus_t				status;
		};
	}
}