// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModChannel.cpp                                      :
// Date Created       : 2024 JAN 11                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK modulation functionality for a given channel.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <cmath>
#include <map>
#include <boost/format.hpp>
#include "FskModChannel.hpp"
#include "Utilities.hpp"

namespace svc_fsk_modem
{
	using namespace std;


	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">			 	[in] The application instance. </param>
	/// <param name="diag">			 	[in] The diagnostic exporter. </param>
	/// <param name="statusCounters">	[in,out] The status counters. </param>
	/// <param name="chanNumber">	 	The channel number. </param>
	/// <param name="pParams">		 	[in] Ptr to the channel's parameters. </param>
	FskModChannel::FskModChannel(FskModemApp& app, DiagnosticsMod& diag, user_api::ModulationCounters_t& statusCounters, 
		int chanNumber, Parameters_t* pParams) : app(app), diag(diag), statusCounters(statusCounters), chanNumber(chanNumber), 
		pParams(pParams)
	{
		// lookup the channel center freq
		chanCenterFreq = AppConfiguration::ChannelCenterFreq(pParams->parmCommon.chanMap, chanNumber);

		app.log.LockedLog() << "FSK-ModChan available channel " << chanNumber << " (" << chanCenterFreq / 1000.0 << " kHz)" << endl;

		// create our buffers
		pNcoSinChanSamples = new float[pParams->iqSamplesPerFrame];
		pNcoCosChanSamples = new float[pParams->iqSamplesPerFrame];

		// determine the sample freq for waveform generation
		float genSampleFreq = pParams->parmMod.upsample * pParams->parmCommon.sampleRate;

		// create sin and cos NCOs, set to baseband with freqDeviation adjustment
		float basebandFreqCenter = 0;
		pNcoSinGen = new Nco(genSampleFreq);
		pNcoSinGen->Frequency(basebandFreqCenter);
		pNcoSinGen->FineAdjustRange(pParams->parmCommon.frequencyDeviation);
		pNcoCosGen = new Nco(genSampleFreq);
		pNcoCosGen->Frequency(basebandFreqCenter, 90);
		pNcoCosGen->FineAdjustRange(pParams->parmCommon.frequencyDeviation);

		// log if NCOs are not exact
		if ((pNcoSinGen->Frequency() != basebandFreqCenter) || (pNcoSinGen->FineAdjustRange() != pParams->parmCommon.frequencyDeviation))
		{
			app.log.LockedLog() << "WARN FSK-ModChan channel " << chanNumber << " nco_gen=" << pNcoSinGen->Frequency() << 
				"," << pNcoSinGen->FineAdjustRange() << " (requested = " <<
				basebandFreqCenter << "," << pNcoSinGen->FrequencyResolution() << "," << pParams->parmCommon.frequencyDeviation << ")" << endl;
		}

		// create sin and cos NCOs, set to the channel freq 
		pNcoSinChan = new Nco(pParams->parmCommon.sampleRate);
		pNcoSinChan->Frequency(abs(chanCenterFreq));
		pNcoCosChan = new Nco(pParams->parmCommon.sampleRate);
		pNcoCosChan->Frequency(abs(chanCenterFreq), 90);

		// set our output level to default, set the channel NCOs
		outputLevel = pParams->parmMod.defaultOutputLevel;
		pNcoSinChan->Level(outputLevel);
		pNcoCosChan->Level(outputLevel);

		// log if NCOs are not exact
		if (pNcoSinChan->Frequency() != abs(chanCenterFreq))
		{
			app.log.LockedLog() << "WARN FSK-ModChan channel " << chanNumber << " nco_chan=" << pNcoSinChan->Frequency() << " (requested=" <<
				abs(chanCenterFreq) << "," << pNcoSinChan->FrequencyResolution() << ")" << endl;
		}

		// create a buffer for a fully encapsulated packet (preamble + sync word + length byte + payload (ie packet) + CRC). Prefill the
		// preamble and sync word, they are static. Set ptr to the length byte.
		pEncapPkt = new uint8_t[pParams->maxPacketLengthX8 + 1 + ((pParams->parmCommon.syncWordLengthBits + pParams->parmCommon.preambleLengthBits + crc.CrcSizeBits())/8)];
		pEncapPktLengthLoc = pEncapPkt;
		for (int idx = 0; idx < pParams->parmCommon.preambleLengthBits / 8; idx++)
		{
			*pEncapPktLengthLoc++ = 0xAA;
		}
		uint32_t syncWord = pParams->parmCommon.syncWord << (32 - pParams->parmCommon.syncWordLengthBits);
		for (int idx = 0; idx < pParams->parmCommon.syncWordLengthBits / 8; idx++)
		{
			*pEncapPktLengthLoc++ = syncWord>>24;
			syncWord <<= 8;
		}

		// create frame buffers. Note that while we only return pParams->iqSamplesPerFrame 
		// samples, but we generate pParams->parmMod.upsample times as many while we work.
		frameBufrs.bufrLength = pParams->iqSamplesPerFrame * pParams->parmMod.upsample;
		for (int idx = 0; idx < IqBufrId_t::BufrCount; idx++)
		{
			frameBufrs.pBufr[idx] = new IqPair_t[frameBufrs.bufrLength];
		}
		frameBufrs.activeBufr = IqBufrId_t::BufrA;
		frameBufrs.pActiveBufrPos = frameBufrs.pBufr[frameBufrs.activeBufr];
		frameBufrs.pActiveBufrEnd = frameBufrs.pActiveBufrPos + frameBufrs.bufrLength - 1;

		// create a working buffer of frameBufrs.bufrLength depth
		pWorkingBuffer = new IqPair_t[frameBufrs.bufrLength];

		// init the frame descriptor (we will reuse). We return pParams->iqSamplesPerFrame samples.
		frameDesc.pIqData = nullptr;
		frameDesc.sampleCount = pParams->iqSamplesPerFrame;

		// instantiate the pre-decimation filter
		pFilterPreDecimation = new FilterIir(&pParams->parmMod.preDecimationLpf);

		// init gen state
		genState = GenState_t::Idle;
	}


	/// <summary>	Destructor. </summary>
	FskModChannel::~FskModChannel(void)
	{
		// delete buffers
		if (pNcoSinChanSamples) delete pNcoSinChanSamples;
		if (pNcoCosChanSamples) delete pNcoCosChanSamples;
		for (int idx = 0; idx < IqBufrId_t::BufrCount; idx++)
		{
			if (frameBufrs.pBufr[idx]) delete frameBufrs.pBufr[idx];
		}
		if (pEncapPkt) delete pEncapPkt;
		if (pWorkingBuffer) delete pWorkingBuffer;
	}


	/// <summary>	Set the output level. </summary>
	///
	/// <param name="level">	The output level. </param>
	void FskModChannel::OutputLevel(float level)
	{
		// update our channel NCOs output level
		outputLevel = level;
		pNcoSinChan->Level(outputLevel);
		pNcoCosChan->Level(outputLevel);
	}


	/// <summary>
	/// 	The modulation operation for the channel. Packets in and IQ sample data out.
	/// </summary>
	///
	/// <param name="pPktData">   	[in] Ptr to the packet data. </param>
	/// <param name="pktLengthX8">	Packet length in bytes. </param>
	void FskModChannel::Modulate(uint8_t* pPktData, int pktLengthX8)
	{
		// if we are not idle it is an error, log and discard.
		if (genState != GenState_t::Idle)
		{
			app.log.LockedLog() << "FSK-ModChan discarding packet, not ready (" << chanNumber << ")" << endl;
			return;
		}

		// export for remote diagnostics
		diag.ExportModNewPacket(pPktData, pktLengthX8, chanNumber);

		// encapsulate packet
		*pEncapPktLengthLoc = pktLengthX8;
		for (int idx = 0; idx < pktLengthX8; idx++)
		{
			pEncapPktLengthLoc[idx+1] = pPktData[idx];
		}

		// crc
		uint16_t crc16 = crc.Calculate(pEncapPktLengthLoc, pktLengthX8 + 1);
		pEncapPktLengthLoc[pktLengthX8 + 1] = crc16 >> 8;
		pEncapPktLengthLoc[pktLengthX8 + 2] = crc16 & 0xff;

		// calc pkt length with length byte and crc (for whitening)
		int whitenLength = pktLengthX8 + 3;

		// whiten
		whitener.Whiten(pEncapPktLengthLoc, whitenLength);

		// calc fully encapsulated pkt length
		int encapPktLength = (int)(pEncapPktLengthLoc - pEncapPkt) + whitenLength;

		// export for remote diagnostics
		diag.ExportEncapsulatedPacket(pEncapPkt, encapPktLength, chanNumber);
		
		// prepare to generate the IQ samples. Prepend a duplicate of the first bit
		// bucause our algorithm looks at the last bit.
		pIqGenWorkingEncapPkt = pEncapPkt;
		pIqGenWorkingEncapPktEnd = pEncapPkt + (encapPktLength - 1);
		iqGenWorkingBits = (*pIqGenWorkingEncapPkt++ << 8);
		iqGenWorkingBits = (iqGenWorkingBits & WorkingBitFirstBitMask) | (iqGenWorkingBits >> 1);
		iqGenWorkingBitsAvail = 9;

		// set state to generate preroll to kick off
		genState = GenState_t::Preroll;

		// count
		utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.packets);
	}


	/// <summary>
	/// 	Return the next iq frame.
	/// 	
	/// 	NOTE: the returned frame descriptor is valid only until the next call to this function,
	/// 	as the buffer is reused.
	/// </summary>
	///
	/// <returns>	Ptr to a frame descriptor when there is one, null if no frames. </returns>
	FskModChannel::FrameDesc_t* FskModChannel::NextIqFrame(void)
	{

		// if not idle, process gen state until we get a frame
		if (genState != GenState_t::Idle)
		{
			IqPair_t* pBufrReturn = nullptr;
			while (!pBufrReturn)
			{
				switch (genState)
				{
				case GenState_t::Preroll:

					pBufrReturn = GeneratePreRoll((uint8_t)((iqGenWorkingBits & WorkingBitFirstBitMask) >> WorkingBitFirstBitShift));
					genState = GenState_t::Bits;
					break;

				case GenState_t::Bits:
					while ((iqGenWorkingBitsAvail > 0) && !pBufrReturn)
					{
						pBufrReturn = GenerateBit((iqGenWorkingBits & WorkingBitLastCurNextGroupMask) >> WorkingBitLastCurNextGroupShift);
						iqGenWorkingBits <<= 1;
						iqGenWorkingBitsAvail -= 1;
						if ((iqGenWorkingBitsAvail < 8) && (pIqGenWorkingEncapPkt <= pIqGenWorkingEncapPktEnd))
						{
							iqGenWorkingBits |= *pIqGenWorkingEncapPkt++ << (8 - iqGenWorkingBitsAvail);
							iqGenWorkingBitsAvail += 8;
						}
					}

					// done?
					if (!(iqGenWorkingBitsAvail > 0))
					{
						genState = GenState_t::Postroll;
					}
					break;

				case GenState_t::Postroll:

					// the preroll will always start from 0. Because our algorithm in the BITS state
					// looks at the last bit and the cur bit, we will always end up postpending a 0 bit 
					// as the last data bit moves from cur to last. 
					pBufrReturn = GeneratePostRoll(0);
					genState = GenState_t::FlushIqBufr;
					break;

				case GenState_t::FlushIqBufr:

					// if there is any IQ data in the active buffer, zero the rest and send it
					if (frameBufrs.pActiveBufrPos != frameBufrs.pBufr[frameBufrs.activeBufr])
					{
						// fill out the buffer
						do
						{
							frameBufrs.pActiveBufrPos++->Clear();
						} while (frameBufrs.pActiveBufrPos != frameBufrs.pActiveBufrEnd);

						// return this buffer for sending
						pBufrReturn = frameBufrs.pBufr[frameBufrs.activeBufr];

						// activate next buffer
						ActivateNextBuffer();
					}

					genState = GenState_t::Idle;
					break;
				}
			}

			// pre-decimation filter, place in working buffer
			pFilterPreDecimation->Filter(pBufrReturn, pWorkingBuffer, frameBufrs.bufrLength);

			// export for remote diagnostics
			diag.ExportIqPreDecimation(pWorkingBuffer, frameBufrs.bufrLength, chanNumber);

			// decimate in place if upsample != 1. After, there will only be pParams->iqSamplesPerFrame
			// IQ samples in the buffer. 
			if (pParams->parmMod.upsample != 1)
			{
				for (int rdIdx = 0, wrIdx = 0; rdIdx < frameBufrs.bufrLength; rdIdx += pParams->parmMod.upsample)
				{
					pWorkingBuffer[wrIdx++] = pWorkingBuffer[rdIdx];
				}
			}

			// export for remote diagnostics
			diag.ExportIqGenerated(pWorkingBuffer, pParams->iqSamplesPerFrame, chanNumber);

			// complex mix to channel, place back in pBufrReturn
			float mixedISubI, mixedISubQ, mixedQSubI, mixedQSubQ;
			pNcoSinChan->GetSamples(pParams->iqSamplesPerFrame, pNcoSinChanSamples);
			pNcoCosChan->GetSamples(pParams->iqSamplesPerFrame, pNcoCosChanSamples);
			for (int idx = 0; idx < pParams->iqSamplesPerFrame; idx++)
			{
				mixedISubI = pWorkingBuffer[idx].i * pNcoCosChanSamples[idx];
				mixedISubQ = pWorkingBuffer[idx].i * pNcoSinChanSamples[idx];
				mixedQSubI = pWorkingBuffer[idx].q * pNcoCosChanSamples[idx];
				mixedQSubQ = pWorkingBuffer[idx].q * pNcoSinChanSamples[idx];
				if (chanCenterFreq < 0)
				{
					pBufrReturn[idx].i = mixedISubI + mixedQSubQ;
					pBufrReturn[idx].q = mixedQSubI - mixedISubQ;
				}
				else
				{
					pBufrReturn[idx].i = mixedISubI - mixedQSubQ;
					pBufrReturn[idx].q = mixedQSubI + mixedISubQ;
				}
			}

			// export for remote diagnostics
			diag.ExportIqChannelMixed(pBufrReturn, pParams->iqSamplesPerFrame, chanNumber);

			// update the reused frame desc and return it.
			frameDesc.pIqData = pBufrReturn;
			return &frameDesc;
		}

		return nullptr;
	}


	/// <summary>	Generates pre-roll IQ samples. </summary>
	///
	/// <param name="firstDataBit">	The first data bit. </param>
	///
	/// <returns>
	/// 	When IQ buffer is full a ptr to that buffer to send. If the IQ buffer is not full nullptr
	/// 	will be returned.
	/// </returns>
	IqPair_t* FskModChannel::GeneratePreRoll(uint8_t firstDataBit)
	{
		// select pre-roll to 0 or pre-roll to 1 based on the first data bit
		float* pTransition = pParams->pulseTransitions.preRoll.ppTransitions[firstDataBit];
		float* pTransitionEnd = pTransition + pParams->pulseTransitions.preRoll.entryLength - 1;
		return GenerateTransition(pTransition, pTransitionEnd);
	}


	/// <summary>	Generates data bit using the last/current/next bit group sequence. </summary>
	///
	/// <param name="lastCurNextDataBitSeq">	The last/current/next data bit group sequence. </param>
	///
	/// <returns>
	/// 	When IQ buffer is full a ptr to that buffer to send. If the IQ buffer is not full nullptr
	/// 	will be returned.
	/// </returns>
	IqPair_t* FskModChannel::GenerateBit(uint8_t lastCurNextDataBitSeq)
	{
		// select shape of the bit based on the last/cur/next group sequence
		float* pTransition = pParams->pulseTransitions.bit.ppTransitions[lastCurNextDataBitSeq];
		float* pTransitionEnd = pTransition + pParams->pulseTransitions.bit.entryLength - 1;
		return GenerateTransition(pTransition, pTransitionEnd);
	}


	/// <summary>	Generates post-roll IQ samples. </summary>
	///
	/// <param name="firstDataBit">	The first data bit. </param>
	///
	/// <returns>
	/// 	When IQ buffer is full a ptr to that buffer to send. If the IQ buffer is not full nullptr
	/// 	will be returned.
	/// </returns>
	IqPair_t* FskModChannel::GeneratePostRoll(uint8_t lastDataBit)
	{
		// select post-roll to 0 or post-roll to 1 based on the last data bit
		float* pTransition = pParams->pulseTransitions.postRoll.ppTransitions[lastDataBit];
		float* pTransitionEnd = pTransition + pParams->pulseTransitions.postRoll.entryLength - 1;
		return GenerateTransition(pTransition, pTransitionEnd);
	}


	/// <summary>	Generates IQ samples for a transition. </summary>
	///
	/// <param name="pTransition">   	[in] Ptr to the transition. </param>
	/// <param name="pTransitionEnd">	[in] Ptr to the transition end. </param>
	///
	/// <returns>
	/// 	When IQ buffer is full a ptr to that buffer to send. If the IQ buffer is not full nullptr
	/// 	will be returned.
	/// </returns>
	IqPair_t* FskModChannel::GenerateTransition(float* pTransition, float* pTransitionEnd)
	{
		// export for remote diagnostics
		diag.ExportPulseShaping(pTransition, pTransitionEnd - pTransition + 1, chanNumber);

		// init our return
		IqPair_t* pFullBufr = nullptr;
		do
		{
			// generate sample
			pNcoCosGen->FineAdjust(*pTransition);
			frameBufrs.pActiveBufrPos->i = pNcoCosGen->GetSamples();
			pNcoSinGen->FineAdjust(*pTransition);
			frameBufrs.pActiveBufrPos->q = pNcoSinGen->GetSamples();

			// check for full buffer. If not full, advance. If full,
			// move to next buffer and set our return ptr to the
			// full buffer.
			if (frameBufrs.pActiveBufrPos != frameBufrs.pActiveBufrEnd)
			{
				frameBufrs.pActiveBufrPos++;
			}
			else
			{
				pFullBufr = frameBufrs.pBufr[frameBufrs.activeBufr];
				ActivateNextBuffer();
			}
		} while (++pTransition <= pTransitionEnd);

		return pFullBufr;
	}


	/// <summary>	Activates the next buffer. </summary>
	void FskModChannel::ActivateNextBuffer(void)
	{
		frameBufrs.activeBufr = (frameBufrs.activeBufr == IqBufrId_t::BufrA) ? IqBufrId_t::BufrB : IqBufrId_t::BufrA;
		frameBufrs.pActiveBufrPos = frameBufrs.pBufr[frameBufrs.activeBufr];
		frameBufrs.pActiveBufrEnd = frameBufrs.pActiveBufrPos + frameBufrs.bufrLength - 1;
	}
}