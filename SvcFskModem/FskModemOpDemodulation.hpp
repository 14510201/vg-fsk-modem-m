// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpDemodulation.hpp                             :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemApp.hpp"
#include "FskModemConnectionOwner.hpp"
#include "FskModemOpEvtNode.hpp"
#include "FskModemEvents.hpp"
#include "IqPair.hpp"
#include "FskDemodChannel.hpp"


namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem demodulation functionality. </summary>
	class FskModemOpDemodulation : public FskModemOpEvtNode
	{
	private:
		/// <summary>	(Immutable) General constants. </summary>
		const int								EncIqBufferSizeBytes = 640;


		uint8_t*								pIqEncFrameBufr;
		IqPair_t*								pIqBufr;
		std::map<int, FskDemodChannel*>			chanDemodMap;
		FskDemodChannel::Parameters_t			chanParams;
		bool									diagGlobalEnable;
		user_api::DemodulationCounters_t		statusCounters;

	public:
		FskModemOpDemodulation(void) = delete;
		FskModemOpDemodulation(FskModemApp& app, FskModemConnectionOwner& qConnect, FskModemEventChannel* pEvtChan);
		~FskModemOpDemodulation(void);

		void								Run(void) override;

	protected:
		void								HandleEvtConfigurationChanged(events::EventConfigurationChanged_t* pEvt, DiagnosticsDemod& diagnostics);
		void								HandleEvtDiagnosticsUpdate(events::EventDemodDiagnosticsUpdate_t* pEvt, DiagnosticsDemod& diagnostics);
		void								HandleEvtStatusRequest(events::EventStatusRequest_t* pEvt, DiagnosticsDemod& diagnostics);
		void								UpdateConfiguration(DiagnosticsDemod& diagnostics);
		void								ReleaseChannels(void);
		int									DecodeIqFrame(uint32_t* pEncFrame, int lengthX32, IqPair_t* pDecFrame);
		inline float						DecodeS36(uint16_t value);
	};
}