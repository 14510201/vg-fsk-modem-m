// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemConnectionUser.cpp                             :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Attach to the ipc queues as a user (not owner/creator).
//
// -----------------------------------------------------------------------------

#include "FskModemConnectionUser.hpp"
#include <boost/interprocess/mapped_region.hpp>


using namespace std;
using namespace boost::interprocess;

namespace svc_fsk_modem
{
	/// <summary>	Constructor. </summary>
	///
	/// <param name="connectionName">	Name of the connection. </param>
	FskModemConnectionUser::FskModemConnectionUser(const char* connectionName) : connectionName(connectionName), FskModemIpcQueues()
	{}


	/// <summary>	Destructor. </summary>
	FskModemConnectionUser::~FskModemConnectionUser(void)
	{
		if (pShmMappedRegion)
		{
			delete pShmMappedRegion;
		}
	}


	/// <summary>	Attach to the FSK modem service. </summary>
	///
	/// <returns>	Returns 0 upon success, -EFAULT if unable to attach. </returns>
	int	FskModemConnectionUser::Attach()
	{
		try
		{
			// attach to the shared memory object.
			shared_memory_object shm(open_only, connectionName, read_write);

			// map the whole shared memory in this process
			pShmMappedRegion = new mapped_region(shm, read_write);

			// attach to the Qs
			FskModemIpcQueues::Attach(pShmMappedRegion->get_address());
		}
		catch (interprocess_exception& ex)
		{
			return -EFAULT;
		}

		// success
		return 0;
	}


	/// <summary>	Get the service version. </summary>
	///
	/// <returns>	The version string. </returns>
	std::string FskModemConnectionUser::GetVersion(void)
	{
		return FskModemIpcQueues::GetVersion();
	}


	/// <summary>	Sets the service's version. </summary>
	///
	/// <returns>	The version string. </returns>
	std::string FskModemConnectionUser::GetConnectionVersion(void)
	{
		return FskModemIpcQueues::GetConnectionVersion();
	}


	/// <summary>	Send a command. </summary>
	///
	/// <param name="cmd"> 	[in] Ref to the command. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int FskModemConnectionUser::SendCommand(user_api::SvcFskModemCommand_t& cmd, bool wait)
	{
		FskModemIpcQueues::AddCommandEntry(cmd, wait);
	}


	/// <summary>	Receive a response. </summary>
	///
	/// <param name="resp">	[out] Ref for the response. </param>
	/// <param name="wait">	True to wait, false to return immediately. Default = wait. </param>
	///
	/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
	int	FskModemConnectionUser::ReceiveResponse(user_api::SvcFskModemResponse_t& resp, bool wait)
	{
		return FskModemIpcQueues::GetNextResponseEntry(resp, wait);
	}


	/// <summary>
	/// 	Send an I/Q data frame for demodulation.
	/// </summary>
	///
	/// <param name="pFrame">  	[in] Pointer to the frame data. </param>
	/// <param name="lengthX8">	The length of the frame in bytes. </param>
	/// <param name="wait">	   	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout when waiting.
	/// </returns>
	int	FskModemConnectionUser::SendDemodIqFrame(void* pFrame, uint16_t lengthX8, bool wait)
	{
		// add to the correct Q
		return AddModDemodEntry(QueueId_t::DemodIqFramesToModem, pFrame, lengthX8, wait);
	}


	/// <summary>	Receive a demod packet. </summary>
	///
	/// <param name="pPacket">	  	[out] Pointer to store the received packet. </param>
	/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pPacket. </param>
	/// <param name="pChanNumber">	[out] If non-null, the channel number will be stored. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT if
	/// 	bad  parameters.
	/// </returns>
	int	FskModemConnectionUser::ReceiveDemodPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait)
	{
		uint16_t			tag;

		// get from the correct Q.  The tag is used to pass the channel number.
		int retn = GetNextModDemodEntry(QueueId_t::DemodPktsFromModem, pPacket, maxLengthX8, &tag, wait);
		if (pChanNumber)
		{
			*pChanNumber = (uint8_t)tag;
		}
		return retn;
	}


	/// <summary>
	/// 	Send a packet for modulation.
	/// </summary>
	///
	/// <param name="pPacket">   	[in] Pointer to the packet data. </param>
	/// <param name="lengthX8">  	The length of the packet in bytes. </param>
	/// <param name="chanNumber">	The channel number to modulate on. </param>
	/// <param name="wait">		 	True to wait, false to return immediately. Default = true. </param>
	///
	/// <returns>
	/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
	/// 	after timeout when waiting.
	/// </returns>
	int	FskModemConnectionUser::SendModPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait)
	{
		// add to the correct Q.   The tag is used to pass the channel number.
		return AddModDemodEntry(QueueId_t::ModPktsToModem, pPacket, lengthX8, (uint16_t)chanNumber, wait);
	}


	/// <summary>	Receive a modulated I/Q frame for transmission. </summary>
	///
	/// <param name="pFrame">	  	[out] Pointer to store the received frame. </param>
	/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pFrame. </param>
	/// <param name="wait">		  	True to wait, false to return immediately. Default = true.</param>
	///
	/// <returns>
	/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
	/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT
	/// 	if bad  parameters.
	/// </returns>
	int	FskModemConnectionUser::ReceiveModIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait)
	{
		// get from the correct Q
		return GetNextModDemodEntry(QueueId_t::ModIqFramesFromModem, pFrame, maxLengthX8, wait);
	}
}