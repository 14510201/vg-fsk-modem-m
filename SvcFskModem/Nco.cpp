// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : Nco.cpp				                                   :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Numerically Controlled Oscillator.
//
// -----------------------------------------------------------------------------

#include <cmath>
#include <boost/format.hpp>
#include "Nco.hpp"


namespace svc_fsk_modem
{
    using namespace std;


	/// <summary>	(Immutable) nco sine table, no attenuation. </summary>
	#include "NcoSinTable.h"
	const int			Nco::SinTableLength = NCOSINTABLE_LENGTH;
	const int			Nco::MaxAccum = NCOSINTABLE_MAX_ACCUM;
	const float*		Nco::pSinTable1_0 = sinTable;


    /// <summary>	A map of output level tables for all instances. </summary>
    std::map<float, Nco::OutputLevelSinTable_t>     Nco::levelTableMap;


	/// <summary>	Constructor. </summary>
	///
	/// <param name="sampleRate">	The sample rate in samples per second. </param>
	Nco::Nco(float sampleRate)
	{
        // set freq resolution
        freqResolution = sampleRate / MaxAccum;

        // default is level 1.0
        pSinTable = (float*) pSinTable1_0;
        level = 1.0;
    }


    /// <summary>	Destructor. </summary>
    Nco::~Nco(void)
    {
        // release our level
        ReleaseLevelTable(level);
    }
    

    /// <summary>	Nco set frequency. </summary>
    ///
    /// <param name="targetFrequency">	Target frequency in Hz. </param>
    /// <param name="startPhase">	  	The start phase shift in degrees. Default is 0. </param>
    ///
    /// <returns>	The actual freqeuncy set. </returns>
    float Nco::Frequency(float targetFrequency, int startPhase)
    {
        // get start phase shift from 0 <= phase < 360, savefreqActual
        startingPhaseShift = startPhase % 360;

        // get closest phase inc for the desired freq
        phaseInc = round(targetFrequency / freqResolution);

        // determine the actual freq based on the selection of phaseInc
        freqActual = phaseInc * freqResolution;

        // set the accum starting point based on our starting phase
        accum = floor((float(startingPhaseShift) / 360) * (float)MaxAccum);

        // clear fine adjust & range
        fineAdjust = 0.0;
        fineAdjustSteps = 0;

        return freqActual;
    }


    /// <summary>
    /// 	Sets fine adjustment range. Fine adjust is signed, actual range is +/- frequency range with
    /// 	respect to the set frequency. Once set, adjusting fineAdjust between -1.0 and +1.0 will
    /// 	alter the frequency from -frequencyRange -> +frequencyRange, in steps of the frequency
    /// 	resolution.
    /// </summary>
    ///
    /// <param name="frequencyRange">	The frequency range. </param>
    void Nco::FineAdjustRange(float frequencyRange)
    {
        // determine the number of steps we can take in either direction
        fineAdjustSteps = frequencyRange / freqResolution;
        fineAdjustSteps = floor(fineAdjustSteps);

        // clear fine adjust
        fineAdjust = 0.0;
    }


    /// <summary>	Apply fine adjustment. </summary>
    ///
    /// <param name="adjust">	The adjustment, range from -1.0 to +1.0. </param>
    void Nco::FineAdjust(float adjust)
    {
        // determine our fine adjust amount, limit -1.0 to +1.0
        if (adjust > 1.0)
        {
            adjust = 1.0;
        }
        else if (adjust < -1.0)
        {
            adjust = -1.0;
        }
        fineAdjust = round(adjust * fineAdjustSteps);
    }


    /// <summary>	Gets NCO samples. This overload returns a single sample. </summary>
    ///
    /// <returns>	The sample. </returns>
    float Nco::GetSamples(void)
    {
        // the phase inc to apply is our phaseInc + fine adjust
        int lclPhaseInc = phaseInc + fineAdjust;

        // get the table index then inc accum
        int tableIdx = (accum % MaxAccum);
        accum += lclPhaseInc;

        // return one sample
        return pSinTable[tableIdx];
    }


    /// <summary>	Gets NCO samples. This overload gets 'count' samples. </summary>
    ///
    /// <exception cref="std::runtime_error">	Raised when bad params found. </exception>
    ///
    /// <param name="count">  	Number of samples to get. </param>
    /// <param name="pBuffer">	[out] The buffer to put the samples. </param>
    ///
    /// <returns>	pBuffer. </returns>
    float* Nco::GetSamples(int count, float* pBuffer)
    {
        // check params
        if (!pBuffer)
        {
            throw std::runtime_error(str(boost::format("Bad params %1%[%2%]") % (strrchr("/" __FILE__, '/') + 1) % __LINE__));
        }
        
        // the phase inc to apply is our phaseInc + fine adjust
        uint32_t lclPhaseInc = phaseInc + fineAdjust;

        // get samples
        for (int idx = 0; idx < count; idx++)
        {
            // get the table index then inc accum
            uint16_t tableIdx = (accum % MaxAccum);
            accum += lclPhaseInc;
            pBuffer[idx] = pSinTable[tableIdx];
        }
        return pBuffer;
    }


    /// <summary>	Set the ouput level. </summary>
    ///
    /// <param name="atten">	The level. The default level is 1.0. </param>
    void Nco::Level(float newLevel)
    {
        // change the level if different
        if (newLevel != level)
        {
            // release the current level
            ReleaseLevelTable(level);

            // see if the new level already exists. If so use it. Else
            // create and use.
            auto attenTable = levelTableMap.find(newLevel);
            if (attenTable != levelTableMap.end())
            {
                attenTable->second.useCount++;
                pSinTable = attenTable->second.pSinTable;
            }
            else
            {
                auto newAttenTable = levelTableMap.emplace(newLevel, SinTableLength);
                newAttenTable.first->second.useCount = 1;
                newAttenTable.first->second.level = newLevel;
                for (int idx = 0; idx < SinTableLength; idx++)
                {
                    newAttenTable.first->second.pSinTable[idx] = newLevel * pSinTable1_0[idx];
                }
                pSinTable = newAttenTable.first->second.pSinTable;
            }
            level = newLevel;
        }
    }


    /// <summary>
    /// 	Release use of the level table by rmvLevel. If this was the last use of it, the
    /// 	table will be removed and destroyed.
    /// </summary>
    ///
    /// <param name="rmvLevel">	The level to remove. </param>
    void Nco::ReleaseLevelTable(float rmvLevel)
    {
        // if the rmvLevel is 1.0, nothing to do, it is a fixed table. Otherwise,
        // update the atten map entry.
        if (rmvLevel != 1.0)
        {
            auto entry = levelTableMap.find(rmvLevel);
            if (--entry->second.useCount == 0)
            {
                levelTableMap.erase(rmvLevel);
            }
        }
    }
}