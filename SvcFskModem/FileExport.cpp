// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FileExport.cpp     		                               :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Export data to a file.
//
// -----------------------------------------------------------------------------

#include <boost/format.hpp>
#include "FileExport.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Constructor. </summary>
	///
	/// <param name="pFilename">	Name of the file. </param>
	FileExport::FileExport(const char* pFilename)
	{
		Filename(pFilename);
		pFile = nullptr;
		enabled = false;
		userStarted = false;
		fileWriteCount = 0;
	}


	/// <summary>	Destructor. </summary>
	FileExport::~FileExport(void)
	{
		Close();
	}


	/// <summary>	Start exporting. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised when unable to open the file. </exception>
	///
	/// <param name="overwrite">	True to overwrite, false to preserve. Default=false. </param>
	void FileExport::Start(void)
	{
		// if the file is not yet open, do so now.
		if (!pFile)
		{
			pFile = fopen(filename.c_str(), "w+");
			fileWriteCount = 0;

			if (!pFile)
			{
				throw std::runtime_error(str(boost::format("Unable to open file '%1%' for IQ export.") % filename));
			}
		}

		// enable (also mark that we were started)
		enabled = true;
		userStarted = true;
	}


	/// <summary>	Stops exporting. </summary>
	void FileExport::Stop(void)
	{
		// clear user started flag
		userStarted = false;

		// internal stop
		InternalStop();
	}


	/// <summary>	Internal stop. This stops without clearing the user started flag. </summary>
	void FileExport::InternalStop(void)
	{
		// disable
		enabled = false;

		// flush
		if (pFile)
		{
			fflush(pFile);
		}
	}


	/// <summary>	Closes this export. </summary>
	void FileExport::Close(void)
	{
		if (pFile)
		{
			fflush(pFile);
			fclose(pFile);
			pFile = nullptr;
			enabled = false;
			userStarted = false;
		}
	}


	/// <summary>
	/// 	File write. Will update the write count and stop when we reach the soft
	/// 	limit.
	/// </summary>
	///
	/// <param name="pBufr">	[in] Ptr to the data. </param>
	/// <param name="size"> 	The size of an object. </param>
	/// <param name="count">	Number of objects. </param>
	inline void FileExport::FileWrite(void* pBufr, size_t size, size_t count)
	{
		// if we hit the soft limit, stop.
		if (fileWriteCount > FileWriteSoftLimit)
		{
			InternalStop();
			return;
		}
		fwrite(pBufr, size, count, pFile);
		fileWriteCount += (count * size);
	}


	/// <summary>	Exports an IQ pair. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The IQ pair. </param>
	void FileExport::Export(uint16_t tag, IqPair_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of IQ pairs. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the IQ data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, IqPair_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescIqData_t desc = ContainerDescIqData_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescIqData_t), 1);

			// data
			FileWrite(pData, 2*sizeof(float), count);
		}
	}


	/// <summary>	Exports a float. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, float data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of floats. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, float* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescFloat_t desc = ContainerDescFloat_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescFloat_t), 1);

			// data
			FileWrite(pData, sizeof(float), count);
		}
	}


	/// <summary>	Exports a uint32_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, uint32_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of uint32_t's. </summary>
	///
	/// <param name="tag">		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, uint32_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescUint32_t desc = ContainerDescUint32_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescUint32_t), 1);

			// data
			FileWrite(pData, sizeof(uint32_t), count);
		}
	}


	/// <summary>	Exports a uint16_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, uint16_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of uint16_t's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, uint16_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescUint16_t desc = ContainerDescUint16_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescUint16_t), 1);

			// data
			FileWrite(pData, sizeof(uint16_t), count);
		}
	}


	/// <summary>	Exports a uint8_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, uint8_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of uint8_t's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, uint8_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescUint8_t desc = ContainerDescUint8_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescUint8_t), 1);

			// data
			FileWrite(pData, sizeof(uint8_t), count);
		}
	}


	/// <summary>	Exports a int32_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, int32_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of int32_t's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, int32_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescInt32_t desc = ContainerDescInt32_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescInt32_t), 1);

			// data
			FileWrite(pData, sizeof(int32_t), count);
		}
	}


	/// <summary>	Exports a int16_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, int16_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of int16_t's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, int16_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescInt16_t desc = ContainerDescInt16_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescInt16_t), 1);

			// data
			FileWrite(pData, sizeof(int16_t), count);
		}
	}


	/// <summary>	Exports a int8_t. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, int8_t data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of int8_t's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, int8_t* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescInt8_t desc = ContainerDescInt8_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescInt8_t), 1);

			// data
			FileWrite(pData, sizeof(int8_t), count);
		}
	}


	/// <summary>	Exports a bool. </summary>
	///
	/// <param name="tag"> 	The user tag applied to the container. </param>
	/// <param name="data">	The data. </param>
	void FileExport::Export(uint16_t tag, bool data)
	{
		Export(tag, &data, 1);
	}


	/// <summary>	Exports a series of bool's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pData">	[in] Ptr to the data. </param>
	/// <param name="count">	Number of samples to export. </param>
	void FileExport::Export(uint16_t tag, bool* pData, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescBool_t desc = ContainerDescBool_t(tag, count);
			FileWrite(&desc, sizeof(ContainerDescBool_t), 1);

			// data
			FileWrite(pData, sizeof(bool), count);
		}
	}


	/// <summary>	Exports a custom. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pCustom">	[in] Ptr to the custom data. </param>
	/// <param name="size">	    Size of one custom element. </param>
	void FileExport::Export(uint16_t tag, void* pCustom, int size)
	{
		Export(tag, pCustom, size, 1);
	}


	/// <summary>	Exports a series of custom's. </summary>
	///
	/// <param name="tag"> 		The user tag applied to the container. </param>
	/// <param name="pCustom">	[in] Ptr to the custom data. </param>
	/// <param name="size">	    Size of one custom element. </param>
	/// <param name="count">	Number of elements to export. </param>
	void FileExport::Export(uint16_t tag, void* pCustom, int size, int count)
	{
		if (enabled)
		{
			// container descriptor
			ContainerDescCustom_t desc = ContainerDescCustom_t(tag, size, count);
			FileWrite(&desc, sizeof(ContainerDescCustom_t), 1);

			// data
			FileWrite(pCustom, size, count);
		}
	}
}