﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : SvcFskModemCmdRespApi.hpp                              :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file. Defines the command / response interface to the service.
//
// -----------------------------------------------------------------------------

#pragma once

#include <cstring>

namespace svc_fsk_modem
{
	namespace user_api
	{
		// -----------------------------------------------
		// \/\/\/ Common Definitions and Structures \/\/\/
		// -----------------------------------------------

		/// <summary>
		/// 	(Immutable) The string lengths in bytes. All strings must be NULL terminated. These
		/// 	counts include the NULL char.
		/// </summary>
		const int											ConfigNameStringLengthX8 = 64;
		const int											ConfigDescriptionStringLengthX8 = 128;


		/// <summary>	Modulation counters. </summary>
		struct ModulationCounters_t
		{
			ModulationCounters_t(void)
			{
				Reset();
			}
			void Reset(void)
			{
				memset(this, 0, sizeof(ModulationCounters_t));
			}
			uint32_t										packets;
			uint32_t										badChannels;
			uint32_t										framesDropped;
		};


		/// <summary>	Modulation diagnostics. </summary>
		struct ModulationDiagnostics_t
		{
			ModulationDiagnostics_t(void)
			{
				memset(this, 0, sizeof(ModulationDiagnostics_t));
			}
			bool											enable;
		};


		/// <summary>	Modulation controls. </summary>
		struct ModulationControls_t
		{
			ModulationControls_t(void)
			{
				outputLevel = 1.0;
			}
			float											outputLevel;
		};


		/// <summary>	Modulation status. </summary>
		struct ModulationStatus_t
		{
			ModulationControls_t							controls;
			ModulationDiagnostics_t							diagnostics;
			ModulationCounters_t							counters;
		};


		/// <summary>	Modulation configuration. </summary>
		struct ModulationConfiguration_t
		{
			ModulationConfiguration_t(void)
			{
				pulseShapingBt = 0.0;
			}
			float											pulseShapingBt;
		};


		/// <summary>	Demodulation counters. </summary>
		struct DemodulationCounters_t
		{
			DemodulationCounters_t(void)
			{
				Reset();
			}
			void Reset(void)
			{
				memset(this, 0, sizeof(DemodulationCounters_t));
			}
			uint32_t										packetsGood;
			uint32_t										packetsBad;
			uint32_t										packetsDropped;
		};


		/// <summary>	Demodulation diagnostics. </summary>
		struct DemodulationDiagnostics_t
		{
			DemodulationDiagnostics_t(void)
			{
				memset(this, 0, sizeof(DemodulationDiagnostics_t));
			}
			bool											enable;
		};


		/// <summary>	Demodulation status. </summary>
		struct DemodulationStatus_t
		{
			DemodulationDiagnostics_t						diagnostics;
			DemodulationCounters_t							counters;
		};


		/// <summary>	Configuration. </summary>
		struct Configuration_t
		{
			Configuration_t()
			{
				memset(name, 0, ConfigNameStringLengthX8);
			}
			void Name(std::string newName) 
			{ 
				strncpy(name, newName.c_str(), ConfigNameStringLengthX8); 
				name[ConfigNameStringLengthX8 - 1] = '\0';
			}
			std::string Name(void)
			{
				return std::string(name);
			}
			char											name[ConfigNameStringLengthX8];
			ModulationConfiguration_t						mod;
		};



		// ----------------------
		// \/\/\/ Commands \/\/\/
		// ----------------------


		/// <summary>	Command types </summary>
		enum CommandType_t
		{
			Ping = 0x01,
			GetStatus,
			SetDiagnostics,
			ListConfigurations,
			GetConfiguration,
			SetConfiguration,
			SetControls,
			ListChannels
		};


		/// <summary>	Base of all commands. </summary>
		struct CmdBase_t
		{
			CmdBase_t(void) = delete;
			CmdBase_t(CommandType_t type) : type(type) {}
			CommandType_t									type;
		};

		
		/// <summary>	Command : Ping. </summary>
		struct CmdPing_t : CmdBase_t
		{
			CmdPing_t(void) : CmdBase_t(CommandType_t::Ping) {}
		};


		/// <summary>	Command : Get status. </summary>
		struct CmdGetStatus_t : CmdBase_t
		{
			CmdGetStatus_t(void) : CmdBase_t(CommandType_t::GetStatus)
			{
				clearCounters = false;
			}
			bool											clearCounters;
		};


		/// <summary>	Command : Set diagnostics. </summary>
		struct CmdSetDiagnostics_t : CmdBase_t
		{
			CmdSetDiagnostics_t(void) : CmdBase_t(CommandType_t::SetDiagnostics) {}
			ModulationDiagnostics_t							mod;
			DemodulationDiagnostics_t						demod;
		};


		/// <summary>	Command : List configurations. </summary>
		struct CmdListConfigurations_t : CmdBase_t
		{
			CmdListConfigurations_t(void) : CmdBase_t(CommandType_t::ListConfigurations) {}
		};


		/// <summary>	Command : Get configuration. </summary>
		struct CmdGetConfiguration_t : CmdBase_t
		{
			CmdGetConfiguration_t(void) : CmdBase_t(CommandType_t::GetConfiguration) {}
		};


		/// <summary>	Command : Set configuration. </summary>
		struct CmdSetConfiguration_t : CmdBase_t
		{
			CmdSetConfiguration_t(void) : CmdBase_t(CommandType_t::SetConfiguration) {}
			Configuration_t									config;
		};


		/// <summary>	Command : Set controls. </summary>
		struct CmdSetControls_t : CmdBase_t
		{
			CmdSetControls_t(void) : CmdBase_t(CommandType_t::SetControls) {}
			ModulationControls_t							mod;
		};


		/// <summary>	Command : List channels for active configuration. </summary>
		struct CmdListChannels_t : CmdBase_t
		{
			CmdListChannels_t(void) : CmdBase_t(CommandType_t::ListChannels) {}
		};


		/// <summary>
		/// 	SvcFskModem Command. All commands are containted within this structure.
		/// </summary>
		struct SvcFskModemCommand_t
		{
			SvcFskModemCommand_t(void) {}
			SvcFskModemCommand_t(CmdPing_t cmd) { this->ping = cmd; }
			SvcFskModemCommand_t(CmdGetStatus_t cmd) { this->getStatus = cmd; }
			SvcFskModemCommand_t(CmdSetDiagnostics_t cmd) { this->setDiagnostics = cmd; }
			SvcFskModemCommand_t(CmdListConfigurations_t cmd) { this->listConfigurations = cmd; }
			SvcFskModemCommand_t(CmdGetConfiguration_t cmd) { this->getConfiguration = cmd; }
			SvcFskModemCommand_t(CmdSetConfiguration_t cmd) { this->setConfiguration = cmd; }
			SvcFskModemCommand_t(CmdSetControls_t cmd) { this->setControls = cmd; }
			SvcFskModemCommand_t(CmdListChannels_t cmd) { this->listChannels = cmd; }
			CommandType_t Type(void) { return ((CmdBase_t*)this)->type; }
			union
			{
				CmdPing_t					    			ping;
				CmdGetStatus_t								getStatus;
				CmdSetDiagnostics_t							setDiagnostics;
				CmdListConfigurations_t						listConfigurations;
				CmdGetConfiguration_t						getConfiguration;
				CmdSetConfiguration_t						setConfiguration;
				CmdSetControls_t							setControls;
				CmdListChannels_t							listChannels;
			};
		};
		


		// -----------------------
		// \/\/\/ Responses \/\/\/
		// -----------------------


		/// <summary>	Response types </summary>
		enum ResponseType_t
		{
			Ready = 0x81,
			StatusReport,
			AvailConfigurationsList,
			ConfigurationReport,
			SetConfigurationComplete,
			SetDiagnosticsComplete,
			ActiveChannelsList
		};


		/// <summary>	Base of all responses. </summary>
		struct RespBase_t
		{
			RespBase_t(void) = delete;
			RespBase_t(ResponseType_t type) : type(type) {}
			ResponseType_t									type;
		};


		/// <summary>	Response : Ready </summary>
		struct RespReady_t : RespBase_t
		{
			RespReady_t(void) : RespBase_t(ResponseType_t::Ready) {}
		};


		/// <summary>	Response : Status report. </summary>
		struct RespStatusReport_t : RespBase_t
		{
			RespStatusReport_t(void) : RespBase_t(ResponseType_t::StatusReport)
			{
				countersCleared = false;
			}
			bool											countersCleared;
			ModulationStatus_t								mod;
			DemodulationStatus_t							demod;
		};


		/// <summary>	Response : Available configurations list entry. </summary>
		struct RespAvailConfigurationsListEntry_t : RespBase_t
		{
			RespAvailConfigurationsListEntry_t(void) : RespBase_t(ResponseType_t::AvailConfigurationsList)
			{
				first = false;
				last = false;
				memset(name, 0, ConfigNameStringLengthX8);
				memset(description, 0, ConfigDescriptionStringLengthX8);
			}
			void Name(std::string newName)
			{
				strncpy(name, newName.c_str(), ConfigNameStringLengthX8);
				name[ConfigNameStringLengthX8 - 1] = '\0';
			}
			std::string Name(void)
			{
				return std::string(name);
			}
			void Description(std::string newDescription)
			{
				strncpy(description, newDescription.c_str(), ConfigDescriptionStringLengthX8);
				description[ConfigDescriptionStringLengthX8 - 1] = '\0';
			}
			std::string Description(void)
			{
				return std::string(description);
			}
			bool											first;
			bool											last;
			char											name[ConfigNameStringLengthX8];
			char											description[ConfigDescriptionStringLengthX8];
		};


		/// <summary>	Response : Configuration report. </summary>
		struct RespConfigurationReport_t : RespBase_t
		{
			RespConfigurationReport_t(void) : RespBase_t(ResponseType_t::ConfigurationReport) {}
			void Description(std::string newDescription)
			{
				strncpy(description, newDescription.c_str(), ConfigDescriptionStringLengthX8);
				description[ConfigDescriptionStringLengthX8 - 1] = '\0';
			}
			std::string Description(void)
			{
				return std::string(description);
			}
			Configuration_t									config;
			char											description[ConfigDescriptionStringLengthX8];
		};


		/// <summary>	Response : Set configuration complete </summary>
		struct RespSetConfigurationComplete_t : RespBase_t
		{
			RespSetConfigurationComplete_t(void) : RespBase_t(ResponseType_t::SetConfigurationComplete) {}
		};


		/// <summary>	Response : Set diagnostics complete </summary>
		struct RespSetDiagnosticsComplete_t : RespBase_t
		{
			RespSetDiagnosticsComplete_t(void) : RespBase_t(ResponseType_t::SetConfigurationComplete) {}
		};


		/// <summary>	Response : Active channels list entry. </summary>
		struct RespActiveChannelsListEntry_t : RespBase_t
		{
			RespActiveChannelsListEntry_t(void) : RespBase_t(ResponseType_t::ActiveChannelsList)
			{
				first = false;
				last = false;
				number = 0;
				centerFrequency = 0.0;
			}
			bool											first;
			bool											last;
			int												number;
			float											centerFrequency;
		};


		/// <summary>
		/// 	SvcFskModem Response. All responses are containted within this structure.
		/// </summary>
		struct SvcFskModemResponse_t
		{
			SvcFskModemResponse_t(void) {}
			SvcFskModemResponse_t(RespReady_t resp) { this->ready = resp; }
			SvcFskModemResponse_t(RespStatusReport_t resp) { this->statusReport = resp; }
			SvcFskModemResponse_t(RespAvailConfigurationsListEntry_t resp) { this->availConfiguration = resp; }
			SvcFskModemResponse_t(RespConfigurationReport_t resp) { this->configurationReport = resp; }
			SvcFskModemResponse_t(RespSetConfigurationComplete_t resp) { this->setConfigurationComplete = resp; }
			SvcFskModemResponse_t(RespSetDiagnosticsComplete_t resp) { this->setDiagnosticsComplete = resp; }
			SvcFskModemResponse_t(RespActiveChannelsListEntry_t resp) { this->activeChannel = resp; }
			ResponseType_t Type(void) { return ((RespBase_t*)this)->type; }
			union
			{
				RespReady_t									ready;
				RespStatusReport_t							statusReport;
				RespAvailConfigurationsListEntry_t			availConfiguration;
				RespConfigurationReport_t					configurationReport;
				RespSetConfigurationComplete_t				setConfigurationComplete;
				RespSetDiagnosticsComplete_t				setDiagnosticsComplete;
				RespActiveChannelsListEntry_t				activeChannel;
			};
		};
	}
}