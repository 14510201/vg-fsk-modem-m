// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemConnectionUser.hpp                             :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <boost/interprocess/shared_memory_object.hpp>
#include "FskModemIpcQueues.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: Fsk modem connection, user (not owner/creator) of the queues and shared memory.
	/// </summary>
	class FskModemConnectionUser : FskModemIpcQueues
	{
	private:
		const char*								connectionName;
		boost::interprocess::mapped_region*		pShmMappedRegion;

	public:
		FskModemConnectionUser(void) = delete;
		FskModemConnectionUser(const char* connectionName);
		~FskModemConnectionUser(void);

		int							Attach(void);
		std::string					GetVersion(void);
		std::string					GetConnectionVersion(void);
		int							SendCommand(user_api::SvcFskModemCommand_t& cmd, bool wait = true);
		int							ReceiveResponse(user_api::SvcFskModemResponse_t& resp, bool wait = true);
		int							SendDemodIqFrame(void* pFrame, uint16_t lengthX8, bool wait = true);
		int							ReceiveDemodPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait = true);
		int							SendModPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait = true);
		int							ReceiveModIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait = true);
	};
}