// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskChannelMap.cpp                                      :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Contains the map of channel numbers to center frequency.
//
// -----------------------------------------------------------------------------

#include "FilterIir.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Default constructor. </summary>
	///
	/// <param name="pDefinition">	[in] Ptr to filter definition. </param>
	FilterIir::FilterIir(FilterDefinition_t* pDefinition)
	{ 
		// copy the def
		pFiltDef = new FilterDefinition_t();
		*pFiltDef = *pDefinition;
		pB = pA = nullptr;
		pZ[0] = pZ[1] = nullptr;
		filterLength = 0;

		// reset filter
		Reset();
	}


	/// <summary>	Destructor. </summary>
	FilterIir::~FilterIir(void)
	{
		if (pB) delete pB;
		if (pA) delete pA;
		if (pZ[0]) delete pZ[0];
		if (pZ[1]) delete pZ[1];
		if (pFiltDef) delete pFiltDef;
	}


	/// <summary>	Resets the filter. </summary>
	void FilterIir::Reset(void)
	{
		// flush old arrays (if any)
		if (pB) delete pB;
		if (pA) delete pA;
		if (pZ[0]) delete pZ[0];
		if (pZ[1]) delete pZ[1];

		// setup working arrays
		filterLength = pFiltDef->b.size();
		pB = new float[filterLength];
		pA = new float[filterLength];
		pZ[0] = new float[filterLength];
		pZ[1] = new float[filterLength];

		// init a and b to their defined values, normalize. Init z to 0.
		for (int idx = 0; idx < filterLength; idx++)
		{
			pB[idx] = pFiltDef->b[idx] / pFiltDef->a[0];
			pA[idx] = pFiltDef->a[idx] / pFiltDef->a[0];
			pZ[0][idx] = 0.0;
			pZ[1][idx] = 0.0;
		}
	}


	/// <summary>	Filter data array. </summary>
	///
	/// <param name="pData">			[in] Ptr to the data to filter. </param>
	/// <param name="pFilteredData">	[out] Ptr to filtered data storage. </param>
	/// <param name="length">			Length of the data to filter. </param>
	///
	/// <returns>	pFilteredData. </returns>
	float* FilterIir::Filter(float* pData, float* pFilteredData, int length)
	{
		for (int idx = 0; idx < length; idx++)
		{
			// output term
			pFilteredData[idx] = (pB[0] * pData[idx]) + pZ[0][0];

			// process the filter. Last broken out of the loop since pZ[*][filterLength]=0;
			for (int filtIdx = 1; filtIdx < filterLength - 1; filtIdx++)
			{
				pZ[0][filtIdx - 1] = (pB[filtIdx] * pData[idx]) + pZ[0][filtIdx] - (pA[filtIdx] * pFilteredData[idx]);
			}
			pZ[0][filterLength - 2] = (pB[filterLength - 1] * pData[idx]) - (pA[filterLength - 1] * pFilteredData[idx]);
		}
		return pFilteredData;
	}


	/// <summary>	Filter data, one value at a time. </summary>
	///
	/// <param name="data">	Next data value to filter. </param>
	///
	/// <returns>	Next filter output. </returns>
	float FilterIir::Filter(float data)
	{
		// output term
		float filteredData = (pB[0] * data) + pZ[0][0];

		// process the filter. Last broken out of the loop since pZ[*][filterLength]=0;
		for (int filtIdx = 1; filtIdx < filterLength - 1; filtIdx++)
		{
			pZ[0][filtIdx - 1] = (pB[filtIdx] * data) + pZ[0][filtIdx] - (pA[filtIdx] * filteredData);
		}
		pZ[0][filterLength - 2] = (pB[filterLength - 1] * data) - (pA[filterLength - 1] * filteredData);
		return filteredData;
	}


	/// <summary>	Filter IQ data array. </summary>
	///
	/// <param name="pData">			[in] Ptr to the IQ data to filter. </param>
	/// <param name="pFilteredData">	[out] Ptr to filtered IQ data storage. </param>
	/// <param name="length">			Length of the IQ data to filter. </param>
	///
	/// <returns>	pFilteredData. </returns>
	IqPair_t* FilterIir::Filter(IqPair_t* pData, IqPair_t* pFilteredData, int length)
	{
		for (int idx = 0; idx < length; idx++)
		{
			// output terms
			pFilteredData[idx].i = (pB[0] * pData[idx].i) + pZ[0][0];
			pFilteredData[idx].q = (pB[0] * pData[idx].q) + pZ[1][0];

			// process the filter. Last broken out of the loop since pZ[*][filterLength]=0;
			for (int filtIdx = 1; filtIdx < filterLength-1; filtIdx++)
			{
				pZ[0][filtIdx - 1] = (pB[filtIdx] * pData[idx].i) + pZ[0][filtIdx] - (pA[filtIdx] * pFilteredData[idx].i);
				pZ[1][filtIdx - 1] = (pB[filtIdx] * pData[idx].q) + pZ[1][filtIdx] - (pA[filtIdx] * pFilteredData[idx].q);
			}
			pZ[0][filterLength - 2] = (pB[filterLength-1] * pData[idx].i) - (pA[filterLength-1] * pFilteredData[idx].i);
			pZ[1][filterLength - 2] = (pB[filterLength-1] * pData[idx].q) - (pA[filterLength-1] * pFilteredData[idx].q);
		}
		return pFilteredData;
	}


	/// <summary>	Filter data, one value at a time. </summary>
	///
	/// <param name="data">	Next data value to filter. </param>
	///
	/// <returns>	Next filter output. </returns>
	IqPair_t FilterIir::Filter(IqPair_t data)
	{
		IqPair_t filteredData;

		// output term
		filteredData.i = (pB[0] * data.i) + pZ[0][0];
		filteredData.q = (pB[0] * data.q) + pZ[1][0];

		// process the filter. Last broken out of the loop since pZ[*][filterLength]=0;
		for (int filtIdx = 1; filtIdx < filterLength - 1; filtIdx++)
		{
			pZ[0][filtIdx - 1] = (pB[filtIdx] * data.i) + pZ[0][filtIdx] - (pA[filtIdx] * filteredData.i);
			pZ[1][filtIdx - 1] = (pB[filtIdx] * data.q) + pZ[1][filtIdx] - (pA[filtIdx] * filteredData.q);
		}
		pZ[0][filterLength - 2] = (pB[filterLength - 1] * data.i) - (pA[filterLength - 1] * filteredData.i);
		pZ[1][filterLength - 2] = (pB[filterLength - 1] * data.q) - (pA[filterLength - 1] * filteredData.q);
		return filteredData;
	}
}