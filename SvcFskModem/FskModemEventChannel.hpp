// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemEventChannel.hpp		    				   :
// Date Created       : 2024 JAN 30                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <mutex>
#include <queue>
#include <chrono>
#include <condition_variable>
#include "FskModemEvents.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem event channel. This event channel is thread-safe. </summary>
	class FskModemEventChannel
	{
	protected:

		/// <summary>	Struct: Event Queue. </summary>
		struct EventQueue_t
		{
			std::mutex									mutAccess;
			std::queue<events::Event_t*>				qEvents;
		};

		EventQueue_t									qEvtsToNode;
		std::condition_variable							condToNodeEvts;
		EventQueue_t									qEvtsFromNode;
		std::condition_variable							condFromNodeEvts;

	public:
		FskModemEventChannel(void) {};
		~FskModemEventChannel(void) {};

		void						PostToNode(events::Event_t* pEvt);
		bool						NextEventFromNode(events::Event_t*& pEvt, bool wait = true);
		bool						NextEventFromNode(events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait = true);
		void						PostFromNode(events::Event_t* pEvt);
		bool						NextEventForNode(events::Event_t*& pEvt, bool wait = true);
		bool						NextEventForNode(events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait = true);
	};
}