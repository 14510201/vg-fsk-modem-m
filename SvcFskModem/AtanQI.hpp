// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : AtanQI.hpp			                                   :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemApp.hpp"
#include "IqPair.hpp"


namespace svc_fsk_modem
{
	/// <summary>	Class: Atan(Q/I) by lookup. </summary>
	class AtanQI
	{
	protected:
				
		/// <summary>	(Immutable) General constants. </summary>
		static const int			MaxI;
		static const int			MaxQ;
		const int					S3p6Multiplier = 64;

		inline float				AtanLookup(IqPair_t iq);

	public:
		AtanQI(void) {}
		~AtanQI(void) {}

		float						Atan(IqPair_t iq);
		void						Atan(IqPair_t* pIq, float* pBufr, int length);
	};
}