// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FilterIir.hpp                                          :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>
#include "IqPair.hpp"

namespace svc_fsk_modem
{
	/// <summary>
	/// 	Class: IIR Filter. Filtering IQ data, there is a separate history (z) for I and Q. When
	/// 	only filtering a regular data array, only one of the histories will be used. Do not
	/// 	mix filtering IQ and filtering regular array with the same filter instance.
	/// </summary>
	class FilterIir
	{
	public:

		/// <summary>	Filter Definition. </summary>
		struct FilterDefinition_t
		{
			std::string				name;
			std::vector<float>		b;
			std::vector<float>		a;
		};

	protected:
		FilterDefinition_t*				pFiltDef;
		float*							pB;
		float*							pA;
		float*							pZ[2];
		int								filterLength;

	public:
		FilterIir(FilterDefinition_t* pDefinition);
		~FilterIir(void);

		float*							Filter(float* pData, float* pFilteredData, int length);
		float							Filter(float data);
		IqPair_t*						Filter(IqPair_t* pData, IqPair_t* pFilteredData, int length);
		IqPair_t						Filter(IqPair_t data);
		void							Reset(void);
	};
}