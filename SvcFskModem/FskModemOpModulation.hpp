// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpModulation.hpp                               :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <queue>
#include "FskModemConnectionOwner.hpp"
#include "FskModemApp.hpp"
#include "FskModemOpEvtNode.hpp"
#include "FskModemEvents.hpp"
#include "FskModChannel.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem modulation functionality. </summary>
	class FskModemOpModulation : public FskModemOpEvtNode
	{
	private:
		/// <summary>
		/// 	(Immutable) Number of IQ samples per frame. Each IQ sample is 20b packed into 32b words,
		/// 	the byte size of the encoded frame buffer must be a multiple of size(uint32_t).
		/// </summary>
		const int												IqSamplesPerFrame = 256;


		/// <summary>	(Immutable) General constants. </summary>
		const int												MaxPacketLengthX8 = 255;
		const int												PacketContainerPoolSize = 16;

		
		/// <summary>	A queued packet container. </summary>
		struct QueuedPacketContainer_t
		{
			QueuedPacketContainer_t(int MaxPacketLengthX8) 
			{
				pPktData = new uint8_t[MaxPacketLengthX8];
				Reset();
			}
			~QueuedPacketContainer_t(void)
			{
				delete pPktData;
			}
			void Reset(void)
			{
				pktLengthX8 = 0;
			}
			uint8_t*											pPktData;
			int													pktLengthX8;
		};


		/// <summary>	Channel Packet Qs container. </summary>
		struct ChannelPacketQueuesContainer_t
		{
			ChannelPacketQueuesContainer_t(void)
			{
				pendingPacketCount = 0;
			}
			std::map<int, std::queue<QueuedPacketContainer_t*>>	qChanMap;
			int													pendingPacketCount;
		};


		std::map<int, FskModChannel*>							chanModMap;
		uint8_t*												pPktBufr;
		uint8_t*												pIqEncFrameBufr;
		FskModChannel::Parameters_t								chanParams;
		std::queue<QueuedPacketContainer_t*>					qPktContainerPool;
		ChannelPacketQueuesContainer_t							chanPkts;
		QueuedPacketContainer_t*								pReceivedPkt;
		float													outputLevel;
		bool													diagGlobalEnable;
		user_api::ModulationCounters_t							statusCounters;
		user_api::ModulationConfiguration_t						cfg;


	public:
		FskModemOpModulation(void) = delete;
		FskModemOpModulation(FskModemApp& app, FskModemConnectionOwner& qConnect, FskModemEventChannel* pEvtChan);
		~FskModemOpModulation(void);

		void								Run(void) override;

	protected:
		void								HandleEvtConfigurationChanged(events::EventConfigurationChanged_t* pEvt, DiagnosticsMod& diagnostics);
		void								HandleEvtDiagnosticsUpdate(events::EventModDiagnosticsUpdate_t* pEvt, DiagnosticsMod& diagnostics);
		void								HandleEvtStatusRequest(events::EventStatusRequest_t* pEvt, DiagnosticsMod& diagnostics);
		void								HandleEvtControlsUpdateRequest(events::EventModControlsUpdate_t* pEvt, DiagnosticsMod& diagnostics);
		void								UpdateConfiguration(DiagnosticsMod& diagnostics);
		void								CreateTransitionsArray(FskModChannel::TransitionsContainer_t* pContainer, std::vector<std::vector<float>>& config);
		void								ReleaseTransitionsArray(FskModChannel::TransitionsContainer_t* pContainer);
		void								ReleaseChannels(void);
		void								ComplexSum(IqPair_t* pIqDataA, IqPair_t* pIqDataB, int count);
		int									EncodeIqFrame(IqPair_t* pIqData, int count, uint8_t* pEncBufr);
		void								FillChannelQueues(bool wait = true);
	};
}