// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : StringUtilities.hpp                                    :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <stdint.h>

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>

namespace svc_fsk_modem
{
    namespace string_utilities
    {
        /// <summary>	Class: Split string support. </summary>
        class SplitString
        {
        private:
            SplitString() { }

        public:

            ///-------------------------------------------------------------------------------------------------
            /// <summary>   Splits. </summary>
            ///
            /// <param name="s">        The std::string to process. </param>
            /// <param name="delim">    The delimiter. </param>
            /// <param name="result">   The result. </param>
            template<typename T>
            static void split(const std::string& s, char delim, T result)
            {
                std::stringstream ss;
                ss.str(s);
                std::string item;
                while (std::getline(ss, item, delim))
                {
                    *(result++) = item;
                }
            }


            ///-------------------------------------------------------------------------------------------------
            /// <summary>   Splits. </summary>
            ///
            /// <param name="s">        The std::string to process. </param>
            /// <param name="delim">    The delimiter. </param>
            ///
            /// <returns>   vector of split strings. Back to back delimeters will result in an
            ///             empty string in the vector at that location. </returns>
            static std::vector<std::string> split(const std::string& s, char delim)
            {
                std::vector<std::string> elems;
                split(s, delim, std::back_inserter(elems));
                return elems;
            }
        };

        class TrimString
        {
        private:
            TrimString() { }

        public:

            /// <summary>	Left selects the given string, removing whitespace. </summary>
            ///
            /// <param name="str">	[in] The string. </param>
            ///
            /// <returns>	A reference to a std::string. </returns>
            static std::string& left(std::string& str)
            {
                auto it2 = std::find_if(str.begin(),
                    str.end(),
                    [](char ch) {return !std::isspace<char>(ch, std::locale::classic()); });
                str.erase(str.begin(), it2);
                return str;
            }


            /// <summary>	Right selects the given string, removing whitespace. </summary>
            ///
            /// <param name="str">	[in] The string. </param>
            ///
            /// <returns>	A reference to a std::string. </returns>
            static std::string& right(std::string& str)
            {
                auto it1 = std::find_if(str.rbegin(),
                    str.rend(),
                    [](char ch) {return !std::isspace<char>(ch, std::locale::classic()); });
                str.erase(it1.base(), str.end());
                return str;
            }


            /// <summary>
            /// 	Slects left and right of the given string, removing whitespace on both ends.
            /// </summary>
            ///
            /// <param name="str">	[in] The string. </param>
            ///
            /// <returns>	A reference to a std::string. </returns>
            static std::string& both(std::string& str)
            {
                return left(right(str));
            }

            static std::string both_copy(std::string const& str)
            {
                auto s = str;
                return both(s);
            }
        };
    }
}

