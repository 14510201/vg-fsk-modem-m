// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpCmdResp.hpp                                  :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <mutex>
#include "SvcFskModemCmdRespApi.hpp"
#include "FskModemConnectionOwner.hpp"
#include "FskModemApp.hpp"
#include "FskModemOpEvtHost.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem modulation functionality. </summary>
	class FskModemOpCmdResp : public FskModemOpEvtHost
	{
	public:

		/// <summary>	Event channel Ids </summary>
		enum EventChannelId_t : FskModemOpEvtHost::EventChannelId_t
		{
			Modulation = 1,
			Demodulation
		};

	public:
		FskModemOpCmdResp(void) = delete;
		FskModemOpCmdResp(FskModemApp& app, FskModemConnectionOwner& qConnect);
		~FskModemOpCmdResp(void);

		void							Run(void) override;
		FskModemEventChannel*			RegisterForEvents(EventChannelId_t evtChanId);

	protected:
		void							HandleCmdPing(user_api::CmdPing_t& cmd);
		void							HandleCmdGetStatus(user_api::CmdGetStatus_t& cmd);
		void							HandleCmdSetDiagnostics(user_api::CmdSetDiagnostics_t& cmd);
		void							HandleCmdListConfigurations(user_api::CmdListConfigurations_t& cmd);
		void							HandleCmdGetConfiguration(user_api::CmdGetConfiguration_t& cmd);
		void							HandleCmdSetConfiguration(user_api::CmdSetConfiguration_t& cmd);
		void							HandleCmdSetControls(user_api::CmdSetControls_t& cmd);
		void							HandleCmdListChannels(user_api::CmdListChannels_t& cmd);
	};
}