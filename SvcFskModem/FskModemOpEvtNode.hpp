// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpEvtNode.hpp                                  :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file. Base class for event-node ops.
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemOp.hpp"
#include "FskModemEvents.hpp"
#include "FskModemEventChannel.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem base event-node op functionality. </summary>
	class FskModemOpEvtNode : public FskModemOp

	{
	public:
		FskModemOpEvtNode(void) = delete;
		FskModemOpEvtNode(FskModemApp& app, FskModemConnectionOwner& qConnect, FskModemEventChannel* pEvtChan)
			: FskModemOp(app, qConnect), pEvtChan(pEvtChan) {}

	protected:

		/// <summary>	Posts an event from node. </summary>
		///
		/// <param name="evt">	[in] Ptr to the event. </param>
		void			PostEvent(events::Event_t* pEvt) { pEvtChan->PostFromNode(pEvt); }


		/// <summary>	Next event for node. </summary>
		///
		/// <param name="pEvt">	[out] Ref to event ptr. </param>
		/// <param name="wait">	(Optional) True to wait. </param>
		///
		/// <returns>	True if it succeeds, false if it fails. </returns>
		bool			NextEvent(events::Event_t*& pEvt, bool wait = true) { return pEvtChan->NextEventForNode(pEvt, wait); }


		/// <summary>
		/// 	Next event for node. If the event container passed in is 'empty' the next event will be
		/// 	returned. If it is a specific type, events will be flushed until the requested type
		/// 	occurs.
		/// </summary>
		///
		/// <param name="pEvt">		 	[out] Ref to event ptr. </param>
		/// <param name="reqEvtType">	Type of the event requested. </param>
		/// <param name="wait">		 	(Optional) True to wait. </param>
		///
		/// <returns>	True if it succeeds, false if it fails. </returns>
		bool			NextEvent(events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait = true) { return pEvtChan->NextEventForNode(pEvt, reqEvtType, wait); }


	private:
		FskModemEventChannel*				pEvtChan;
	};
}