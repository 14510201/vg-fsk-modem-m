// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemEventChannel.cpp				  			   :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Thread-safe events channel between event host and node.
//
// -----------------------------------------------------------------------------

#include "FskModemEventChannel.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Posts event to node. </summary>
	///
	/// <param name="pEvt">	[in] Ptr to the event. </param>
	void FskModemEventChannel::PostToNode(events::Event_t* pEvt)
	{
		std::lock_guard<std::mutex> guard(qEvtsToNode.mutAccess);
		qEvtsToNode.qEvents.push(pEvt);
		condToNodeEvts.notify_one();
	}


	/// <summary>
	/// 	Next event from node.
	/// </summary>
	///
	/// <param name="pEvt">		 	[out] Ref to event ptr. </param>
	/// <param name="wait">		 	True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	bool FskModemEventChannel::NextEventFromNode(events::Event_t*& pEvt, bool wait)
	{
		return NextEventFromNode(pEvt, events::EventType_t::Empty, wait);
	}


	/// <summary>
	/// 	Next event from node. If reqEvtType is 'empty' the next event will be returned. If it is
	/// 	a specific type, events will be flushed until the requested type occurs.
	/// </summary>
	///
	/// <param name="pEvt">		 	[out] Ref to event ptr. </param>
	/// <param name="reqEvtType">	Type of the event requested. </param>
	/// <param name="wait">		 	True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	bool FskModemEventChannel::NextEventFromNode(events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait)
	{
		do
		{
			// lock
			std::unique_lock<std::mutex> lock(qEvtsFromNode.mutAccess);

			// check for event pending
			auto waitTime = (wait) ? std::chrono::duration(std::chrono::seconds(1)) : std::chrono::duration(std::chrono::nanoseconds::zero());
			if (!condFromNodeEvts.wait_for(lock, waitTime, [&] { return qEvtsFromNode.qEvents.size() > 0; }))
			{
				return false;
			}

			// get the event
			pEvt = qEvtsFromNode.qEvents.front();
			qEvtsFromNode.qEvents.pop();
		} while ((reqEvtType != events::EventType_t::Empty) && (reqEvtType != pEvt->type));

		return true;
	}


	/// <summary>	Posts event from node. </summary>
	///
	/// <param name="pEvt">	[in] Ptr to the event. </param>
	void FskModemEventChannel::PostFromNode(events::Event_t* pEvt)
	{
		std::lock_guard<std::mutex> guard(qEvtsFromNode.mutAccess);
		qEvtsFromNode.qEvents.push(pEvt);
		condFromNodeEvts.notify_one();
	}


	/// <summary>	Next event for node. </summary>
	///
	/// <param name="pEvt">	[out] Ref to event ptr. </param>
	/// <param name="wait">	True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	///
	/// ### <param name="reqEvtType">	Type of the request event. </param>
	bool FskModemEventChannel::NextEventForNode(events::Event_t*& pEvt, bool wait)
	{
		return NextEventForNode(pEvt, events::EventType_t::Empty, wait);
	}


	/// <summary>
	/// 	Next event for node. If the event container passed in is 'empty' the next event will be
	/// 	returned. If it is a specific type, events will be flushed until the requested type
	/// 	occurs.
	/// </summary>
	///
	/// <param name="pEvt">		 	[out] Ref to event ptr. </param>
	/// <param name="reqEvtType">	Type of the event requested. </param>
	/// <param name="wait">		 	True to wait, default = true. </param>
	///
	/// <returns>	True if it succeeds, false if it fails. </returns>
	bool FskModemEventChannel::NextEventForNode(events::Event_t*& pEvt, events::EventType_t reqEvtType, bool wait)
	{
		do
		{
			// lock
			std::unique_lock<std::mutex> lock(qEvtsToNode.mutAccess);

			// if no waiting, just check the count and return accordingly.
			if (!wait)
			{
				if (qEvtsToNode.qEvents.size() == 0)
				{
					return false;
				}
			}
			else
			{
				// check for event pending
				auto waitTime = std::chrono::duration(std::chrono::seconds(1));
				if (!condToNodeEvts.wait_for(lock, waitTime, [&] { return qEvtsToNode.qEvents.size() > 0; }))
				{
					return false;
				}
			}

			// get the event
			pEvt = qEvtsToNode.qEvents.front();
			qEvtsToNode.qEvents.pop();
		} while ((reqEvtType != events::EventType_t::Empty) && (reqEvtType != pEvt->type));

		return true;
	}
}