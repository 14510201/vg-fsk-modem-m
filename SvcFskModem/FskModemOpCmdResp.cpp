// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpCmdResp.cpp                                  :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK controls functionality.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <thread>
#include <boost/format.hpp>
#include "FskModemOpCmdResp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">	[in,out] The application. </param>
	FskModemOpCmdResp::FskModemOpCmdResp(FskModemApp& app, FskModemConnectionOwner& qConnect) : FskModemOpEvtHost(app, qConnect)
	{}


	/// <summary>	Destructor. </summary>
	FskModemOpCmdResp::~FskModemOpCmdResp(void)
	{}


	/// <summary>	Registers this object for events. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised if this evtChanId already registered. </exception>
	///
	/// <param name="evtChanId">	Identifier for the event channel. </param>
	///
	/// <returns>	Pointer to the FskModemEventChannel_t. </returns>
	FskModemEventChannel* FskModemOpCmdResp::RegisterForEvents(EventChannelId_t evtChanId)
	{
		// create the event channel for the id
		return CreateEventChannel(evtChanId);
	}


	/// <summary>	The controls/status operation. </summary>
	void FskModemOpCmdResp::Run(void)
	{
		user_api::SvcFskModemCommand_t		cmdFromUser;

		// set running and enter run loop
		running = true;
		while (!stop)
		{ 
			// wait for next command
			if (qConnect.ReceiveCommand(cmdFromUser, true) == 0)
			{
				// handle
				switch (cmdFromUser.Type())
				{
				case user_api::CommandType_t::Ping:
					HandleCmdPing((user_api::CmdPing_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::GetStatus:
					HandleCmdGetStatus((user_api::CmdGetStatus_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::SetDiagnostics:
					HandleCmdSetDiagnostics((user_api::CmdSetDiagnostics_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::ListConfigurations:
					HandleCmdListConfigurations((user_api::CmdListConfigurations_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::GetConfiguration:
					HandleCmdGetConfiguration((user_api::CmdGetConfiguration_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::SetConfiguration:
					HandleCmdSetConfiguration((user_api::CmdSetConfiguration_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::SetControls:
					HandleCmdSetControls((user_api::CmdSetControls_t&) cmdFromUser);
					break;

				case user_api::CommandType_t::ListChannels:
					HandleCmdListChannels((user_api::CmdListChannels_t&)cmdFromUser);
					break;

				}
			}
		}
		running = false;
	}


	/// <summary>	Handles the Ping command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdPing(user_api::CmdPing_t& cmd)
	{
		// respond with ready
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespReady_t()) };
		while(qConnect.SendResponse(resp) < 0);
	}


	/// <summary>	Handles the Get Status command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdGetStatus(user_api::CmdGetStatus_t& cmd)
	{
		// post to mod/demod, request status report
		PostEvent(EventChannelId_t::Modulation, new events::EventStatusRequest_t(cmd.clearCounters));
		PostEvent(EventChannelId_t::Demodulation, new events::EventStatusRequest_t(cmd.clearCounters));

		// wait for the report from each, fill in our reponse
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespStatusReport_t()) };
		events::EventModStatusReport_t* pEvtFromMod;
		events::EventDemodStatusReport_t* pEvtFromDemod;
		while (!NextEvent(EventChannelId_t::Modulation, (events::Event_t*&) pEvtFromMod, events::EventType_t::ModStatusReport));
		while (!NextEvent(EventChannelId_t::Demodulation, (events::Event_t*&) pEvtFromDemod, events::EventType_t::DemodStatusReport));
		resp.statusReport.mod = pEvtFromMod->status;
		resp.statusReport.demod = pEvtFromDemod->status;
		resp.statusReport.countersCleared = (pEvtFromMod->countersCleared && pEvtFromDemod->countersCleared);
		delete pEvtFromMod;
		delete pEvtFromDemod;

		// respond
		while (qConnect.SendResponse(resp) < 0);
	}


	/// <summary>	Handles the Set Diagnostics command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdSetDiagnostics(user_api::CmdSetDiagnostics_t& cmd)
	{
		// post to mod/demod, update diagnostics
		PostEvent(EventChannelId_t::Modulation, new events::EventModDiagnosticsUpdate_t(cmd.mod));
		PostEvent(EventChannelId_t::Demodulation, new events::EventDemodDiagnosticsUpdate_t(cmd.demod));

		// wait for confirmation
		events::EventModDiagnosticsUpdateComplete_t* pEvtFromMod;
		events::EventDemodDiagnosticsUpdateComplete_t* pEvtFromDemod;
		while (!NextEvent(EventChannelId_t::Modulation, (events::Event_t*&)pEvtFromMod, events::EventType_t::ModDiagnosticsUpdateComplete));
		while (!NextEvent(EventChannelId_t::Demodulation, (events::Event_t*&)pEvtFromDemod, events::EventType_t::DemodDiagnosticsUpdateComplete));

		// respond with confirmation
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespSetDiagnosticsComplete_t()) };
		while (qConnect.SendResponse(resp) < 0);
	}


	/// <summary>	Handles the List Configurations command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdListConfigurations(user_api::CmdListConfigurations_t& cmd)
	{
		// response setup
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespAvailConfigurationsListEntry_t()) };

		// send all we have
		auto lstCfgs = app.pAppCfg->ConfigurationList();
		resp.availConfiguration.first = true;
		int remainingCfgs = lstCfgs.size();
		if (remainingCfgs)
		{
			for (auto cfg : lstCfgs)
			{
				// fill in resp for this config
				resp.availConfiguration.Name(cfg.name);
				resp.availConfiguration.Description(cfg.description);
				resp.availConfiguration.last = (--remainingCfgs == 0);

				// respond
				while (qConnect.SendResponse(resp) < 0);
				resp.availConfiguration.first = false;
			}
		}
		else
		{
			// respond with defaults, marked first and last.
			resp.availConfiguration.last = true;
			while (qConnect.SendResponse(resp) < 0);
		}
	}


	/// <summary>	Handles the Get Configuration command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdGetConfiguration(user_api::CmdGetConfiguration_t& cmd)
	{
		// get the config
		std::string cfgName;
		std::string cfgDesc;
		user_api::ModulationConfiguration_t modCfg;
		app.pAppCfg->Configuration(cfgName, cfgDesc, modCfg);

		//response
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespConfigurationReport_t()) };
		resp.configurationReport.config.mod = modCfg;
		resp.configurationReport.config.Name(cfgName);
		resp.configurationReport.Description(cfgDesc);

		// respond
		while (qConnect.SendResponse(resp) < 0);
	}


	/// <summary>	Handles the Set Configuration command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdSetConfiguration(user_api::CmdSetConfiguration_t& cmd)
	{
		// update the configuration
		app.pAppCfg->Configuration(cmd.config.name, cmd.config.mod);
		app.ReportActiveConfiguration();

		// let mod/demod know the config has changed
		PostEvent(EventChannelId_t::Modulation, new events::EventConfigurationChanged_t());
		PostEvent(EventChannelId_t::Demodulation, new events::EventConfigurationChanged_t());

		// wait for confirmation
		events::EventModConfigured_t* pEvtFromMod;
		events::EventDemodConfigured_t* pEvtFromDemod;
		while (!NextEvent(EventChannelId_t::Modulation, (events::Event_t*&)pEvtFromMod, events::EventType_t::ModConfigured));
		while (!NextEvent(EventChannelId_t::Demodulation, (events::Event_t*&)pEvtFromDemod, events::EventType_t::DemodConfigured));

		// respond with confirmation
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespSetConfigurationComplete_t()) };
		while (qConnect.SendResponse(resp) < 0);
	}


	/// <summary>	Handles the Set Controls command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdSetControls(user_api::CmdSetControls_t& cmd)
	{
		// post to mod, update controls
		PostEvent(EventChannelId_t::Modulation, new events::EventModControlsUpdate_t(cmd.mod));
	}


	/// <summary>	Handles the List Channels command. </summary>
	///
	/// <param name="cmd">	[in] Ref to the command. </param>
	void FskModemOpCmdResp::HandleCmdListChannels(user_api::CmdListChannels_t& cmd)
	{
		// response setup
		user_api::SvcFskModemResponse_t resp{ user_api::SvcFskModemResponse_t(user_api::RespActiveChannelsListEntry_t()) };

		// send all active channels
		auto lstChans = app.pAppCfg->CommonParameters().chanMap;
		resp.activeChannel.first = true;
		int remainingChans = lstChans.size();
		if (remainingChans)
		{
			for (auto cfg : lstChans)
			{
				// fill in resp for this config
				resp.activeChannel.number = cfg.first;
				resp.activeChannel.centerFrequency = cfg.second;
				resp.activeChannel.last = (--remainingChans == 0);

				// respond
				while (qConnect.SendResponse(resp) < 0);
				resp.activeChannel.first = false;
			}
		}
		else
		{
			// respond with defaults, marked first and last.
			resp.activeChannel.last = true;
			while (qConnect.SendResponse(resp) < 0);
		}
	}
}