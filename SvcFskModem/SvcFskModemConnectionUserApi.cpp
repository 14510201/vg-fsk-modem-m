// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : SvcFskModemConnectionUserApi.cpp                       :
// Date Created       : 2024 JAN 29                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Wrapper for attaching to the ipc queues as a user (not owner/creator).
//
// -----------------------------------------------------------------------------

#include "FskModemApp.hpp"
#include "FskModemConnectionUser.hpp"
#include "SvcFskModemConnectionUserApi.hpp"

namespace svc_fsk_modem
{
	namespace user_api
	{
		using namespace std;

		/// <summary>	Default constructor. </summary>
		SvcFskModemConnectionUserApi::SvcFskModemConnectionUserApi(void)
		{
			pUserConnection = nullptr;
		}


		/// <summary>	Destructor. </summary>
		SvcFskModemConnectionUserApi::~SvcFskModemConnectionUserApi(void)
		{
			if (pUserConnection)
			{
				delete pUserConnection;
			}
		}


		/// <summary>	Attach to the FSK modem service. </summary>
		///
		/// <returns>	Returns 0 upon success, -EFAULT if unable to attach. </returns>
		int SvcFskModemConnectionUserApi::Attach(void)
		{
			// instantiate SvcFskModemConnectionUser
			pUserConnection = new svc_fsk_modem::FskModemConnectionUser(FskModemApp::AppConnectionName.c_str());

			// attach
			return pUserConnection->Attach();
		}


		/// <summary>	Get the service version. </summary>
		///
		/// <returns>	The version string. </returns>
		std::string SvcFskModemConnectionUserApi::GetVersion(void)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->GetVersion();
		}


		/// <summary>	Get the connection version. </summary>
		///
		/// <returns>	The version string. </returns>
		std::string SvcFskModemConnectionUserApi::GetConnectionVersion(void)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->GetConnectionVersion();
		}


		/// <summary>	Send a command. </summary>
		///
		/// <param name="cmd"> 	[in] Ref to the command. </param>
		/// <param name="wait">	(Optional) True to wait, false to return immediately. Default = wait. </param>
		///
		/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
		int SvcFskModemConnectionUserApi::SendCommand(user_api::SvcFskModemCommand_t& cmd, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->SendCommand(cmd, wait);
		}


		/// <summary>	Receive a response. </summary>
		///
		/// <param name="resp">	[out] Ref for the response. </param>
		/// <param name="wait">	(Optional) True to wait, false to return immediately. Default = wait. </param>
		///
		/// <returns>	0 if successful, -EAGAIN no entry returned or after timeout. </returns>
		int SvcFskModemConnectionUserApi::ReceiveResponse(user_api::SvcFskModemResponse_t& resp, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->ReceiveResponse(resp, wait);
		}


		/// <summary>
		/// 	Send an I/Q data frame for demodulation.
		/// </summary>
		///
		/// <exception cref="runtime_error">	Raised if not attached. </exception>
		///
		/// <param name="pFrame">  	[in] Pointer to the frame data. </param>
		/// <param name="lengthX8">	The length of the frame in bytes. </param>
		/// <param name="wait">	   	True to wait, false to return immediately. Default = true. </param>
		///
		/// <returns>
		/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
		/// 	after timeout when waiting.
		/// </returns>
		int SvcFskModemConnectionUserApi::SendDemodIqFrame(void* pFrame, uint16_t lengthX8, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->SendDemodIqFrame(pFrame, lengthX8, wait);
		}


		/// <summary>	Receive a demod packet. </summary>
		///
		/// <exception cref="runtime_error">	Raised if not attached. </exception>
		///
		/// <param name="pPacket">	  	[out] Pointer to store the received packet. </param>
		/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pPacket. </param>
		/// <param name="pChanNumber">	[out] If non-null, the channel number will be stored. </param>
		/// <param name="wait">		  	True to wait, false to return immediately. Default = true.</param>
		///
		/// <returns>
		/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
		/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT
		/// 	if bad  parameters.
		/// </returns>
		int SvcFskModemConnectionUserApi::ReceiveDemodPacket(void* pPacket, uint16_t maxLengthX8, uint8_t* pChanNumber, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->ReceiveDemodPacket(pPacket, maxLengthX8, pChanNumber, wait);
		}


		/// <summary>
		/// 	Send a packet for modulation.
		/// </summary>
		///
		/// <exception cref="runtime_error">	Raised if not attached. </exception>
		///
		/// <param name="pPacket">		[in] Pointer to the packet data. </param>
		/// <param name="lengthX8">		The length of the packet in bytes. </param>
		/// <param name="chanNumber">	The channel number to modulate on. </param>
		/// <param name="wait">	   		True to wait, false to return immediately. Default = true.</param>
		///
		/// <returns>
		/// 	Returns 0 upon success, -ENOMEM if the Q is full, -EFAULT if bad parameters, or -EAGAIN
		/// 	after timeout when waiting.
		/// </returns>
		int SvcFskModemConnectionUserApi::SendModPacket(void* pPacket, uint16_t lengthX8, uint8_t chanNumber, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->SendModPacket(pPacket, lengthX8, chanNumber, wait);
		}


		/// <summary>	Receive a modulated I/Q frame for transmission. </summary>
		///
		/// <exception cref="runtime_error">	Raised if not attached. </exception>
		///
		/// <param name="pFrame">	  	[out] Pointer to store the received frame. </param>
		/// <param name="maxLengthX8">	The maximum size of the buffer pointed to by pFrame. </param>
		/// <param name="wait">		  	True to wait, false to return immediately. Default = true.</param>
		///
		/// <returns>
		/// 	The length of the data returned in bytes if successful, -EAGAIN no entry returned or
		/// 	after timeout of waiting, -E2BIG if the entry length exceeds maxLengthX8, or -EFAULT
		/// 	if bad  parameters.
		/// </returns>
		int SvcFskModemConnectionUserApi::ReceiveModIqFrame(void* pFrame, uint16_t maxLengthX8, bool wait)
		{
			if (!pUserConnection) throw new runtime_error("Fsk modem connection not attached");
			return pUserConnection->ReceiveModIqFrame(pFrame, maxLengthX8, wait);
		}
	}
}