// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : StreamLog.hpp                                          :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once
#include <cstdint>
#include <mutex> 
#include <iostream>
#include <sstream>
#include <fstream>
#include <functional>


namespace svc_fsk_modem
{
	namespace log
	{
        /// <summary>	Class: A simple log stream. </summary>
        class StreamLog
        {
        public:

            /// <summary>	Values that represent log flush options. </summary>
            enum class LogFlush_t
            {
                No = 0,
                Yes = 1
            };

            /// <summary>	A lock mechanism for locked access to the log. </summary>
            class Locked_t
            {
            private:
                static std::mutex                       mutAccess;

                std::unique_lock<std::mutex>            mutLock;
                StreamLog*                              pLog;

            public:
                Locked_t(StreamLog* pLog) : mutLock(mutAccess), pLog(pLog) {}
                friend Locked_t&& operator<<(Locked_t&& ls, const int input) { (*ls.pLog) << input; return std::move(ls); }
                friend Locked_t&& operator<<(Locked_t&& ls, const std::string input) { (*ls.pLog) << input; return std::move(ls); }
                friend Locked_t&& operator<<(Locked_t&& ls, const std::ostream input) { (*ls.pLog) << &input; return std::move(ls); }
                friend Locked_t&& operator<<(Locked_t&& ls, std::ostream& (*manipulator)(std::ostream&)) { (*ls.pLog) << manipulator; return std::move(ls); }
            };

        private:
            std::ofstream                           fileStream;
            std::reference_wrapper<std::ostream>    outStream;
            std::uint64_t                           lastTimestamp;
            bool                                    addTimestamp;
            bool                                    lastLineEndedWithLF;
                
            void                                    Log(const std::string& text, const LogFlush_t doFlush = LogFlush_t::No) const;
            void                                    LogWithTimestamp(const std::string& text, const LogFlush_t doFlush = LogFlush_t::No);
            uint64_t                                GetClockTickUS(void);


        public:

            StreamLog();
            StreamLog(std::ostream& stream);
            StreamLog(std::string filename);
            StreamLog(const StreamLog& other) = delete;         // non construction-copyable
            StreamLog& operator=(const StreamLog&) = delete;    // non copyable

            Locked_t                                LockedLog() { return Locked_t(this); }

            void                                    Assign(const std::string filename);
            void                                    Assign(std::ostream& stream);

            StreamLog&                              AddTimestamp(bool add);
            bool                                    AddTimestamp() const { return (addTimestamp); }

            std::string                             DeltaStr(const int64_t delta) const;
            std::string                             TimeStr(const uint64_t timestamp) const;

            friend StreamLog& operator<<(StreamLog& log, const int input);
            friend StreamLog& operator<<(StreamLog& log, const std::string input);
            friend StreamLog& operator<<(StreamLog& log, const std::ostream* pInput);
            friend StreamLog& operator<<(StreamLog& log, std::ostream& (*manipulator)(std::ostream&));
        };
	}
}