// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModemOpModulation.cpp                               :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Provides the FSK modulation functionality.
//
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstring>
#include <limits>
#include <algorithm>
#include <iterator>
#include <boost/format.hpp>
#include "FskModemOpModulation.hpp"
#include "DiagnosticsMod.hpp"
#include "Utilities.hpp"


namespace svc_fsk_modem
{
	using namespace std;

	/// <summary>	Constructor. </summary>
	///
	/// <param name="app">	[in,out] The application. </param>
	FskModemOpModulation::FskModemOpModulation(FskModemApp& app, FskModemConnectionOwner& qConnect, 
		FskModemEventChannel* pEvtChan) : FskModemOpEvtNode(app, qConnect, pEvtChan)
    {
		// init
		pReceivedPkt = nullptr;
		pPktBufr = nullptr;
		pIqEncFrameBufr = nullptr;
		diagGlobalEnable = false;
	}


	/// <summary>	Destructor. </summary>
	FskModemOpModulation::~FskModemOpModulation(void)
	{
		// release mod chans
		ReleaseChannels();

		// delete buffers
		if (pPktBufr) delete pPktBufr;
		if (pIqEncFrameBufr) delete pIqEncFrameBufr;

		// release the packet container pool. If we are holding a received packet
		// container, return it to the pool first.
		if (pReceivedPkt)
		{
			qPktContainerPool.push(pReceivedPkt);
		}
		while (!qPktContainerPool.empty())
		{
			auto pContainter = qPktContainerPool.front();
			qPktContainerPool.pop();
			delete pContainter;
		}
	}


	/// <summary>	The modulation opeation. </summary>
	void FskModemOpModulation::Run(void)
	{
		FskModChannel::FrameDesc_t*				pFrameDesc;


		// log start
		app.log.LockedLog() << "FSK Modulation started" << endl;

		// create our buffers. 
		pPktBufr = new uint8_t[MaxPacketLengthX8];
		pIqEncFrameBufr = new uint8_t[(IqSamplesPerFrame * 20) / 8];

		// create our packet container pool. 
		for (int idx = 0; idx < PacketContainerPoolSize; idx++)
		{
			qPktContainerPool.push(new QueuedPacketContainer_t(MaxPacketLengthX8));
		}

		// create dianostic exporter
		DiagnosticsMod diagnostics("/tmp/diagMod.xs");

		// init pulse transition containers and set the initial configuration
		memset(&chanParams.pulseTransitions, 0, sizeof(FskModChannel::PulseShapingTransitions_t));
		UpdateConfiguration(diagnostics);

		// set running and enter run loop
		running = true;
		events::Event_t* pEvtPending = nullptr;
		while (!stop)
		{
			// if we don't have an event pending, check for events without waiting. Handle
			// all.
			do {
				if (pEvtPending)
				{
					switch (pEvtPending->type)
					{
					case events::EventType_t::ConfigurationChanged:
						HandleEvtConfigurationChanged((events::EventConfigurationChanged_t*)pEvtPending, diagnostics);
						break;

					case events::EventType_t::ModDiagnosticsUpdate:
						HandleEvtDiagnosticsUpdate((events::EventModDiagnosticsUpdate_t*)pEvtPending, diagnostics);
						break;

					case events::EventType_t::StatusRequest:
						HandleEvtStatusRequest((events::EventStatusRequest_t*)pEvtPending, diagnostics);
						break;

					case events::EventType_t::ModControlsUpdate:
						HandleEvtControlsUpdateRequest((events::EventModControlsUpdate_t*)pEvtPending, diagnostics);
						break;
					}

					// destroy event
					delete pEvtPending;
					pEvtPending = nullptr;
				}
				if (!pEvtPending)
				{
					NextEvent(pEvtPending, false);
				}
			} while (pEvtPending);

			// if no packets pending in chan Qs, fill channel Qs and wait if no packets available
			if (!chanPkts.pendingPacketCount)
			{
				FillChannelQueues();
			}

			// get next iq frames from all channels, continue until none from any channel or config changes.
			FskModChannel::FrameDesc_t* pSumFrameDesc;
			do {
				// sum together an IQ frame from each channels. Note that we use the buffer for the 
				// first channel to return data to sum in place.
				pSumFrameDesc = nullptr;
				for (auto chan : chanModMap)
				{
					// get next IQ frame. If none, see if there is a new packet to modulate for this channel
					if ((pFrameDesc = chan.second->NextIqFrame()) == nullptr)
					{
						// if not already pending, check for an event (do not wait)
						if (!pEvtPending)
						{
							NextEvent(pEvtPending, false);
						}

						// any more packets to modulate for this channel? If so, modulate it. If not, fill the channel q's.
						// Stop modulating new packets if we got an event.
						if (!chanPkts.qChanMap[chan.first].empty() && !pEvtPending)
						{
							// get the next pkt for this chan, modulate, then return the container to the pool
							auto pPktContainer = chanPkts.qChanMap[chan.first].front();
							chanPkts.qChanMap[chan.first].pop();
							chanPkts.pendingPacketCount--;
							chan.second->Modulate(pPktContainer->pPktData, pPktContainer->pktLengthX8);
							pPktContainer->Reset();
							qPktContainerPool.push(pPktContainer);

							// we modulated a new packet, get the next iq frame
							pFrameDesc = chan.second->NextIqFrame();
						}
						else
						{
							FillChannelQueues(false);
						}
					}

					// if there is a frame desc, combine
					if (pFrameDesc)
					{
						if (pSumFrameDesc)
						{
							ComplexSum(pSumFrameDesc->pIqData, pFrameDesc->pIqData, pSumFrameDesc->sampleCount);
						}
						else
						{
							pSumFrameDesc = pFrameDesc;
						}
					}
				}

				// if we have a summed frame desc, encode and send
				if (pSumFrameDesc)
				{
					// export for remote diagnostics
					diagnostics.ExportIqCombined(pSumFrameDesc->pIqData, pSumFrameDesc->sampleCount);

					// encode
					int encFrameLengthX8 = EncodeIqFrame(pSumFrameDesc->pIqData, pSumFrameDesc->sampleCount, pIqEncFrameBufr);

					// send
					if (qConnect.SendModIqFrame(pIqEncFrameBufr, encFrameLengthX8, false))
					{
						utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.framesDropped);
					}
				}
			} while (pSumFrameDesc);
		}

		// close diagnostics
		diagnostics.Close();

		// no longer running
		running = false;
	}


	/// <summary>	Event Handler: configuration changed. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpModulation::HandleEvtConfigurationChanged(events::EventConfigurationChanged_t* pEvt, DiagnosticsMod& diagnostics)
	{
		// update our config to the latest
		UpdateConfiguration(diagnostics);

		// post our confirmation
		PostEvent(new events::EventModConfigured_t());
	}


	/// <summary>	Event Handler: diagnostics update. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpModulation::HandleEvtDiagnosticsUpdate(events::EventModDiagnosticsUpdate_t* pEvt, DiagnosticsMod& diagnostics)
	{
		// update our diagnotics enable(s).
		diagGlobalEnable = pEvt->diags.enable;

		//  apply if changed
		if (diagGlobalEnable ^ diagnostics.Started())
		{
			if (diagGlobalEnable)
			{
				diagnostics.Start();
				app.log.LockedLog() << "FSK-Mod diagnostics started" << endl;

				// export for remote diagnostics here as well as when the config changes because 
				// diags may not be enabled when the config changes.
				diagnostics.ExportRates(chanParams.parmCommon.sampleRate, chanParams.parmCommon.bitrate, chanParams.parmMod.upsample);
			}
			else
			{
				diagnostics.Close();
				app.log.LockedLog() << "FSK-Mod diagnostics stopped" << endl;
			}
		}

		// post our confirmation
		PostEvent(new events::EventModDiagnosticsUpdateComplete_t());
	}


	/// <summary>	Event Handler: status request. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpModulation::HandleEvtStatusRequest(events::EventStatusRequest_t* pEvt, DiagnosticsMod& diagnostics)
	{
		// create our status
		user_api::ModulationStatus_t status;
		status.diagnostics.enable = diagGlobalEnable;
		status.counters = statusCounters;
		status.controls.outputLevel = outputLevel;

		// if instructed to clear counters, do so now.
		if (pEvt->clearCounters)
		{
			statusCounters.Reset();
		}

		// post our report
		PostEvent(new events::EventModStatusReport_t(pEvt->clearCounters, status));
	}


	/// <summary>	Event Handler: controls update. </summary>
	///
	/// <param name="pEvt">		  	[in] Ptr to the event. </param>
	/// <param name="diagnostics">	[in] Ref to diagnostics. </param>
	void FskModemOpModulation::HandleEvtControlsUpdateRequest(events::EventModControlsUpdate_t* pEvt, DiagnosticsMod& diagnostics)
	{
		// update output level if it changed
		if (outputLevel != pEvt->controls.outputLevel)
		{
			// log it
			outputLevel = pEvt->controls.outputLevel;

			// set output level for channels
			for (auto chan : chanModMap)
			{
				chan.second->OutputLevel(outputLevel);
			}
		}
	}


	/// <summary>	Fill channel queues. </summary>
	///
	/// <param name="wait">	True to wait. false to return immediately. Default = wait. </param>
	void FskModemOpModulation::FillChannelQueues(bool wait)
	{
		uint8_t									chanNumber;
		bool									receiveWait;


		// as long as we are receiving packets, keep going until the pool is empty
		receiveWait = false;
		while (!qPktContainerPool.empty())
		{
			// if we aren't holding a received packet container, get one
			if (!pReceivedPkt)
			{
				pReceivedPkt = qPktContainerPool.front();
				qPktContainerPool.pop();
			}

			// receive packets and distribute to the channel q. Stop when there are no more packets received or our pool is empty
			if ((pReceivedPkt->pktLengthX8 = qConnect.ReceiveModPacket(pReceivedPkt->pPktData, MaxPacketLengthX8, &chanNumber, receiveWait)) > 0)
			{
				// received a packet
				receiveWait = false;

				// feed the packet to the specified channel q (if it's good)
				auto qChan = chanPkts.qChanMap.find(chanNumber);
				if (qChan != chanPkts.qChanMap.end())
				{
					qChan->second.push(pReceivedPkt);
					chanPkts.pendingPacketCount++;
					pReceivedPkt = nullptr;
				}
				else
				{
					utilities::IncAndSaturate::Inc<uint32_t>(statusCounters.badChannels);
					app.log.LockedLog() << "FSK-Mod bad channel (" << chanNumber << ")" << endl;
				}
			}
			else
			{
				// no packets to receive. If we were told to wait and have no packets in channel Qs, wait now.
				if (wait && !chanPkts.pendingPacketCount)
				{
					// ok to wait now
					receiveWait = true;

					// clear the wait flag so we do not wait more than one time
					wait = false;
				}
				else
				{
					return;
				}
			}
		}
	}


	/// <summary>	Updates the configuration. </summary>
	///
	/// <param name="diagnostics">	[in] The diagnostics instance. </param>
	void FskModemOpModulation::UpdateConfiguration(DiagnosticsMod& diagnostics)
	{
		// release mod chans
		ReleaseChannels();

		// update our configuration and params
		chanParams.parmCommon = app.pAppCfg->CommonParameters();
		chanParams.parmMod = app.pAppCfg->ModulationParameters();
		outputLevel = chanParams.parmMod.defaultOutputLevel;

		app.log.LockedLog() << "FSK-Mod using sps=" << chanParams.parmCommon.sampleRate << " fd=" << chanParams.parmCommon.frequencyDeviation <<
			" br=" << chanParams.parmCommon.bitrate << " up=" << chanParams.parmMod.upsample << endl;

		// export for remote diagnostics
		diagnostics.ExportRates(chanParams.parmCommon.sampleRate, chanParams.parmCommon.bitrate, chanParams.parmMod.upsample);

		// setup the remaining channel params. Now that we know how big the pulse shaping config vectors are, we 
		// will create direct access arrays rather than vectors for speed of access.
		chanParams.maxPacketLengthX8 = MaxPacketLengthX8;
		chanParams.iqSamplesPerFrame = IqSamplesPerFrame;
		auto pulseShaping = AppConfiguration::ModulationPulseShaping(chanParams.parmMod.pulseShapingMap, chanParams.parmMod.pulseShapingBt);
		CreateTransitionsArray(&chanParams.pulseTransitions.preRoll, pulseShaping.preRollTransitions);
		CreateTransitionsArray(&chanParams.pulseTransitions.postRoll, pulseShaping.postRollTransitions);
		CreateTransitionsArray(&chanParams.pulseTransitions.bit, pulseShaping.bitTransitions);

		// create the per-channel mod map and pkt Q based on the active configuration
		for (auto entry : chanParams.parmCommon.chanMap)
		{
			chanModMap[entry.first] = new FskModChannel(app, diagnostics, statusCounters, entry.first, &chanParams);
			chanPkts.qChanMap[entry.first] = std::queue<QueuedPacketContainer_t*>();
		}
	}


	/// <summary>	Release all channel instances (if any). </summary>
	void FskModemOpModulation::ReleaseChannels(void)
	{
		// destroy mod chans
		for (auto pChMod : chanModMap)
		{
			delete pChMod.second;
		}
		chanModMap.clear();

		// if we have any pacekts in the channel q's, flush them back to the pool,
		// then clear the qChanMap.
		for (auto qChan : chanPkts.qChanMap)
		{
			while (!qChan.second.empty())
			{
				auto pContainer = qChan.second.front();
				qChan.second.pop();
				pContainer->Reset();
				qPktContainerPool.push(pContainer);
			}
		}
		chanPkts.pendingPacketCount = 0;
		chanPkts.qChanMap.clear();

		// release chan params pulse transitions arrays 
		ReleaseTransitionsArray(&chanParams.pulseTransitions.preRoll);
		ReleaseTransitionsArray(&chanParams.pulseTransitions.postRoll);
		ReleaseTransitionsArray(&chanParams.pulseTransitions.bit);
	}


	/// <summary>	Creates a pulse shaping transitions direct access array container. </summary>
	///
	/// <exception cref="std::runtime_error">	Raised if we were not able to create. </exception>
	///
	/// <param name="pContainer">	[out] Ptr to the container to create . </param>
	/// <param name="config">	 	[in] The configuration to use. </param>
	void FskModemOpModulation::CreateTransitionsArray(FskModChannel::TransitionsContainer_t* pContainer, std::vector<std::vector<float>>& config)
	{
		try
		{
			// note that each entry is expected to be the same length.
			pContainer->entryCount = config.size();
			pContainer->entryLength = config[0].size();
			pContainer->ppTransitions = new float* [pContainer->entryCount];
			for (int idx = 0; idx < pContainer->entryCount; idx++)
			{
				pContainer->ppTransitions[idx] = new float[pContainer->entryLength];
				for (int jdx = 0; jdx < pContainer->entryLength; jdx++)
				{
					pContainer->ppTransitions[idx][jdx] = config[idx][jdx];
				}
			}
		}
		catch (...)
		{
			throw std::runtime_error("Unable to create pulse shaping array.");
		}
	}


	/// <summary>	Releases a transitions array container. </summary>
	///
	/// <param name="pContainer">	[out] Ptr to the container to release. </param>
	void FskModemOpModulation::ReleaseTransitionsArray(FskModChannel::TransitionsContainer_t* pContainer)
	{
		// release the transitions arrays
		for (int idx = 0; idx < pContainer->entryCount; idx++)
		{
			delete pContainer->ppTransitions[idx];
		}

		// release the array of transitions array ptrs
		if(pContainer->ppTransitions) delete pContainer->ppTransitions;
		
		// zero out the container (ie the count and length)
		memset(pContainer, 0, sizeof(FskModChannel::TransitionsContainer_t));
	}


	/// <summary>
	/// 	Complex sum. Sum each IQ point in buffers A and B, put the result in buffer A.
	/// </summary>
	///
	/// <param name="pIqDataA">	[in,out] Ptr to data buffer A. </param>
	/// <param name="pIqDataB">	[in] Ptr to data buffer B. </param>
	/// <param name="count">   	Number of IQ points to sum. </param>
	void FskModemOpModulation::ComplexSum(IqPair_t* pIqDataA, IqPair_t* pIqDataB, int count)
	{
		for (int idx = 0; idx < count; idx++)
		{
			pIqDataA[idx].i = pIqDataA[idx].i + pIqDataB[idx].i;
			pIqDataA[idx].q = pIqDataA[idx].q + pIqDataB[idx].q;
		}
	}


	/// <summary>	Encode iq frame in S3.6 format. </summary>
	///
	/// <param name="pIqData"> 	[in] IQ data to encode. </param>
	/// <param name="count">   	Number of IQ points. </param>
	/// <param name="pEncBufr">	[out] Ptr to the buffer to put the encoded frame. </param>
	///
	/// <returns>	Returns the encoded buffer length in bytes. </returns>
	int FskModemOpModulation::EncodeIqFrame(IqPair_t* pIqData, int count, uint8_t* pEncBufr)
	{
		int encodeCount = (count > IqSamplesPerFrame) ? IqSamplesPerFrame : count;
		uint32_t* pEncBufrX32 = (uint32_t*)pEncBufr;
		uint64_t working = 0;
		int workingBitsAvail = 0;
		int encodedWdCountAsBytes = 0;
		for (int idx = 0; idx < encodeCount; idx++)
		{
			// if we have at least 32b, write an encoded word
			if (workingBitsAvail >= 32)
			{
				*pEncBufrX32++ = (uint32_t)(working >> 32);
				working <<= 32;
				workingBitsAvail -= 32;
				encodedWdCountAsBytes += (sizeof(uint32_t) / sizeof(uint8_t));
			}

			// pull in more to encode
			working |= (uint64_t)(((int64_t)((*pIqData).i * 64.0)) & 0x3ff) << (54 - workingBitsAvail) |
				(((int64_t)((*pIqData++).q * 64.0)) & 0x3ff) << (44 - workingBitsAvail);
			workingBitsAvail += 20;
		}

		// kick out the last
		*pEncBufrX32++ = (uint32_t)(working >> 32);
		return encodedWdCountAsBytes + (sizeof(uint32_t) / sizeof(uint8_t));
	}
}