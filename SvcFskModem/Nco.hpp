// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : Nco.hpp				                                   :
// Date Created       : 2024 JAN 2                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemApp.hpp"

namespace svc_fsk_modem
{
	/// <summary>	Class: Nco. </summary>
	class Nco
	{
	private:

		/// <summary>	(Immutable) General constants. </summary>
		static const int			SinTableLength;
		static const int			MaxAccum;
		static const float*			pSinTable1_0;


		/// <summary>	Output level sin table. Used for all but level = 1.0, which is hardcoded. </summary>
		struct OutputLevelSinTable_t
		{
			OutputLevelSinTable_t(void)
			{
				level = 0.0;
				useCount = 0;
				pSinTable = nullptr;
			}
			OutputLevelSinTable_t(int tableSize)
			{
				level = 0.0;
				useCount = 0;
				pSinTable = new float[tableSize];
			}
			~OutputLevelSinTable_t(void)
			{
				if (!useCount)
				{
					if (pSinTable) delete pSinTable;
				}
			}
			float					level;
			float*					pSinTable;
			int						useCount;
		};


		/// <summary>	A map of output level tables for all instances. </summary>
		static std::map<float, OutputLevelSinTable_t>	levelTableMap;


	protected:
		float						freqResolution;
		uint32_t					startingPhaseShift;
		uint32_t 					phaseInc;
		float						freqActual;
		uint32_t					accum;
		uint32_t					fineAdjustSteps;
		uint32_t 					fineAdjust;
		float*						pSinTable;
		float						level;

	public:
		Nco(void) = delete;
		Nco(float sampleRate);
		~Nco(void);

		float						Frequency(float targetFrequency, int startPhase = 0);
		float						Frequency(void) { return freqActual; }
		void						Level(float level);
		float						Level(void) { return level; }
		float						FrequencyResolution(void) { return freqResolution; }
		void						FineAdjustRange(float frequencyRange);
		float						FineAdjustRange(void) { return fineAdjustSteps * freqResolution; }
		void						FineAdjust(float adjust);
		float						GetSamples(void);
		float*						GetSamples(int count, float* pBuffer);

	protected:
		void						ReleaseLevelTable(float rmvLevel);
	};
}