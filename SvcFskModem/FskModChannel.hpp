// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : FskModChannel.hpp                                      :
// Date Created       : 2024 JAN 11                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include "FskModemApp.hpp"
#include "IqPair.hpp"
#include "FilterIir.hpp"
#include "Nco.hpp"
#include "Whitening.hpp"
#include "FskCrc.hpp"
#include "DiagnosticsMod.hpp"


namespace svc_fsk_modem
{
	/// <summary>	Class: Fsk modem modulation functionality for a single channel. </summary>
	class FskModChannel
	{
	public:

		/// <summary>	Transitions container. </summary>
		struct TransitionsContainer_t
		{
			int											entryCount;
			int											entryLength;
			float**										ppTransitions;
		};


		/// <summary>	Gaussian pulse shaping transitions. </summary>
		struct PulseShapingTransitions_t
		{
			TransitionsContainer_t						preRoll;
			TransitionsContainer_t						postRoll;
			TransitionsContainer_t						bit;
		};


		/// <summary>	Parameters. </summary>
		struct Parameters_t
		{
			AppConfiguration::CommonParameters_t		parmCommon;
			AppConfiguration::ModulationParameters_t	parmMod;
			int											maxPacketLengthX8;
			int											iqSamplesPerFrame;
			PulseShapingTransitions_t					pulseTransitions;
		};


		/// <summary>	Frame descriptor. </summary>
		struct FrameDesc_t
		{
			IqPair_t*									pIqData;
			int											sampleCount;
		};

	private:

		/// <summary>	Generation states. </summary>
		enum GenState_t
		{
			Idle = 0,
			Preroll,
			Bits,
			Postroll,
			FlushIqBufr
		};


		/// <summary>	IQ Buffer ID. </summary>
		enum IqBufrId_t
		{
			BufrA = 0,
			BufrB,

			// must be last
			BufrCount
		};


		/// <summary>	IQ Frame buffer container. </summary>
		struct IqFrameBuffersContainer_t
		{
			IqPair_t*									pBufr[IqBufrId_t::BufrCount];
			int											bufrLength;
			IqBufrId_t									activeBufr;
			IqPair_t*									pActiveBufrPos;
			IqPair_t*									pActiveBufrEnd;
		};


		/// <summary>	(Immutable) Working bit selection constants. </summary>
		const uint16_t									WorkingBitLastCurNextGroupMask = 0xe000;
		const int										WorkingBitLastCurNextGroupShift = 13;
		const uint16_t									WorkingBitFirstBitMask = 0x8000;
		const int										WorkingBitFirstBitShift = 15;

		FskModemApp&									app;
		DiagnosticsMod&									diag;
		user_api::ModulationCounters_t&					statusCounters;
		int												chanNumber;
		Parameters_t*									pParams;
		float											chanCenterFreq;
		Nco*											pNcoSinGen;
		Nco*											pNcoSinChan;
		float*											pNcoSinChanSamples;
		Nco*											pNcoCosGen;
		Nco*											pNcoCosChan;
		float*											pNcoCosChanSamples;
		IqFrameBuffersContainer_t						frameBufrs;
		FrameDesc_t										frameDesc;
		Whitening										whitener;
		FskCrc											crc;
		uint8_t*										pEncapPkt;
		uint8_t*										pEncapPktLengthLoc;
		FilterIir*										pFilterPreDecimation;
		GenState_t										genState;
		uint8_t*										pIqGenWorkingEncapPkt;
		uint8_t*										pIqGenWorkingEncapPktEnd;
		uint16_t										iqGenWorkingBits;
		int												iqGenWorkingBitsAvail;
		float											outputLevel;
		IqPair_t*										pWorkingBuffer;

	public:
		FskModChannel(void) = delete;
		FskModChannel(FskModemApp& app, DiagnosticsMod& diag, user_api::ModulationCounters_t& statusCounters,
			int chanNumber, Parameters_t* pParams);
		~FskModChannel(void);

		void											Modulate(uint8_t* pPktData, int pktLengthX8);
		FrameDesc_t*									NextIqFrame(void);
		void											OutputLevel(float level);
		float											OutputLevel(void) { return outputLevel; }

	protected:
		IqPair_t*										GeneratePreRoll(uint8_t firstDataBit);
		IqPair_t*										GenerateBit(uint8_t lastCurNextDataBitSeq);
		IqPair_t*										GeneratePostRoll(uint8_t lastDataBit);
		IqPair_t*										GenerateTransition(float* pTransition, float* pTransitionEnd);
		void											ActivateNextBuffer(void);
	};
}