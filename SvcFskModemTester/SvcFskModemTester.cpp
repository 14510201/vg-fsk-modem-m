﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2023 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : SvcFskModemTester.cpp                                  :
// Date Created       : 2023 DEC 16                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Entry point for the service tester.
//
// -----------------------------------------------------------------------------


#include <cstring>
#include <iostream> 
#include <thread>
#include <mutex>
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include "SvcFskModemConnectionUserApi.hpp"
#include "SvcFskModemTester.hpp"
#include "PspOobPcapng.hpp"

using namespace std;
using namespace svc_fsk_modem;
using namespace svc_fsk_modem_tester;



/// <summary>	Globals. </summary>
bool			stopTestThreadListenForDemodPkts;
bool			stopTestThreadListenForModIq;
std::mutex		mutCout;



/// <summary>	Test thread : Send demod IQ frames. </summary>
///
/// <param name="pUserConnect">	[in] User connection pointer. </param>
void TestThreadSendDemodIq(user_api::SvcFskModemConnectionUserApi* pUserConnect)
{
	uint8_t				bufr[2048];
	int					pktCount;
	bool				firstPkt = true;


	// open the pcap with PSP OOB data (within L2TP)
	PspOobPcapng pcap("/home/xleradev/pcapng/ndr_join_only_3_pkts.pcapng", "112.97.0.38", "112.97.0.6");


	#define __SELECT_PCAPNG_PKT_WINDOW__
	#ifdef __SELECT_PCAPNG_PKT_WINDOW__

		// advance to the packet window we want. Note, this counts only packets
		// that match our filter. If no specific window desired, skip this.
 		pcap += 8905;
		pktCount = 50;

	#else
		pktCount = 100000;
	#endif


	// send packets of IQ frames
	vector<uint32_t> encodedIqData;
	int pktIdx = 0;
	auto start = std::chrono::high_resolution_clock::now();
	for (; pktIdx < pktCount; pktIdx++)
	{
		// get next matching packet
		encodedIqData = pcap(); 

		// if there was another packet, send
		if (encodedIqData.size())
		{
			// copy into our buffer
			std::copy(encodedIqData.begin(), encodedIqData.end(), (uint32_t*)bufr);

			// if this is the first packet, dump a few encoded IQ 32b words and how they look in 
			// the byte buffer we are sending.
			if (firstPkt)
			{
				std::lock_guard<std::mutex> guard(mutCout);
				std::cout << endl << "First 4 encoded IQ 32b words: ";
				string line = "";
				for (int idx = 0; idx < 4; idx++)
				{
					line += str(boost::format("0x%|08X|  ") % encodedIqData[idx]);
				}
				cout << line << endl;
				std::cout << "In the byte buffer sent: " << std::hex;
				line = "";
				for (int idx = 0; idx < 4*sizeof(uint32_t); idx++)
				{
					line += str(boost::format("0x%|02X| ") % (int)bufr[idx]);
				}
				cout << line << endl << endl;

				firstPkt = false;
			}

			// send
			while (pUserConnect->SendDemodIqFrame((void*)bufr, encodedIqData.size() * sizeof(uint32_t)) < 0);

			// show progress
			if (((pktIdx % 2500) == 0) && pktIdx)
			{
				std::lock_guard<std::mutex> guard(mutCout);
				std::cout << "IQ frames send: " << std::dec << pktIdx << endl;
			}
		}
		else
		{
			break;
		}
	}
	auto duration = std::chrono::high_resolution_clock::now() - start;

	// sequence gaps?
	if (pcap.SequenceGapsDetected())
	{
		auto gaps = pcap.SequenceGaps();
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << endl << "GAPS DETECTED! " << std::dec << (int)gaps.size() << " gaps detected." << endl << endl;
	}

	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << endl << "Total packets: " << std::dec << pktIdx << str(boost::format(" in %1% seconds.") % (duration.count() / 1e9)) << endl << endl;
	}

	// short delay
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}


/// <summary>	Test thread : Listen for demod packets. </summary>
///
/// <param name="pUserConnect">	[in] User connection pointer. </param>
void TestThreadListenForDemodPkts(user_api::SvcFskModemConnectionUserApi* pUserConnect)
{
	uint8_t							packet[256];
	int								pktLength;
	int								pktCount = 0;
	uint8_t							chanNum;


	// listen for packets
	while (!stopTestThreadListenForDemodPkts)
	{
		if ((pktLength = pUserConnect->ReceiveDemodPacket((void*)packet, 256, &chanNum)) > 0)
		{
			std::lock_guard<std::mutex> guard(mutCout);
			std::cout << "Packet received (Len " << std::dec << pktLength << ", Channel " << (int)chanNum << "): ";
			for (int i = 0; i < pktLength; i++)
			{
				if ((i % 8) == 0)
				{
					std::cout << std::endl << "\t\t";
				}
				std::cout << std::hex << "0x" << std::setw(2) << std::setfill('0') << (int)packet[i];
				if (i != pktLength - 1)
				{
					std::cout << ", ";
				}
			}
			std::cout << std::endl << std::endl;
			pktCount++;
		}
	}
	std::cout << "Packet count: " << std::dec << pktCount << std::endl << std::endl;
}


/// <summary>	Test thread : Send packets to mod. </summary>
///
/// <param name="pUserConnect">	[in] User connection pointer. </param>
void TestThreadSendModPkts(user_api::SvcFskModemConnectionUserApi* pUserConnect)
{
	uint8_t							packet[256];
	uint8_t							chanNum;


	#define __ONE_DEFINED_PKT__
	#ifdef __ONE_DEFINED_PKT__

	int pktBytes = 24;
	for (int j = 0; j < pktBytes; j++)
	{
		packet[j] = std::rand() % 256;
	}

	// chan number
	chanNum = 0;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 3;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 2;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 2;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 4;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 1;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 4;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 5;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

	chanNum = 5;
	while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
	//std::this_thread::sleep_for(std::chrono::milliseconds(12));

#else

	// generate packets of random data and length (<=256)
	for (int i = 0; i < 50; i++)
	{
		int pktBytes = (std::rand() % 256) + 1;
		for (int j = 0; j < pktBytes; j++)
		{
			packet[j] = std::rand() % 256;
		}

		// randomly assign channel number
		chanNum = std::rand() % 4;

		while (pUserConnect->SendModPacket((void*)packet, pktBytes, chanNum) < 0);
		std::this_thread::sleep_for(std::chrono::milliseconds(12));
	}
	#endif
}


/// <summary>	Test thread : Listen for mod IQ frames. </summary>
///
/// <param name="pUserConnect">	[in] User connection pointer. </param>
void TestThreadListenForModIq(user_api::SvcFskModemConnectionUserApi* pUserConnect)
{
	uint8_t							frame[640];
	int								frameLength;
	int								frameCount = 0;


	// listen for IQ frames. Only report every 200 received.
	while (!stopTestThreadListenForModIq)
	{
		if ((frameLength = pUserConnect->ReceiveModIqFrame((void*)frame, 640)) > 0)
		{
			std::lock_guard<std::mutex> guard(mutCout);
			if((frameCount % 200) == 0)
			{
				std::cout << "IQ frame received (Len " << std::dec << frameLength << ") #" << frameCount << std::endl;
			}
			frameCount++;
		}
	}
	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "IQ frame count: " << std::dec << frameCount << std::endl << std::endl;
}


/// <summary>	Test mod and demod. </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
void RunTest(user_api::SvcFskModemConnectionUserApi& userConnect)
{

	{
		std::lock_guard<std::mutex> guard(mutCout);
		cout << "Test start" << endl;
	}

	// kickoff the threads
	stopTestThreadListenForDemodPkts = false;
	stopTestThreadListenForModIq = false;
	boost::thread t1(&TestThreadSendDemodIq, &userConnect);
	boost::thread t2(&TestThreadListenForDemodPkts, &userConnect);
	boost::thread t3(&TestThreadSendModPkts, &userConnect);
	boost::thread t4(&TestThreadListenForModIq, &userConnect);

	// stop listening for demod pkts when sending demod iq ends (after brief delay)
	t1.join();
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	stopTestThreadListenForDemodPkts = true;
	t2.join();
	
	// stop listening for mod iq when sending mod pkts ends (after brief delay)
	t3.join();
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	stopTestThreadListenForModIq = true;
	t4.join();

	{
		std::lock_guard<std::mutex> guard(mutCout);
		cout << "Test complete" << endl;
	}
}


/// <summary>	Set the configuration. This is an example of setting a configuration. </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
/// <param name="cfgName">	  	Name of the configuration. </param>
void SetConfiguration(user_api::SvcFskModemConnectionUserApi& userConnect, std::string cfgName)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetConfiguration_t()) };
	user_api::CmdSetConfiguration_t& cmdConfig = cmd.setConfiguration;
	cmdConfig.config.Name(cfgName);
	cmdConfig.config.mod.pulseShapingBt = 1.0;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);
}


/// <summary>
/// 	Show Status. This is an example of sending a command and waiting for the reponse in the
/// 	same thread. The reponse can also be retrieved by another thread instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
void ShowStatus(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdGetStatus_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);
	while (userConnect.ReceiveResponse(resp) != 0);
	user_api::RespStatusReport_t& statusReport = resp.statusReport;


	// demod diag
	string strDiagDemod = " DemodDiag:";
	if (statusReport.demod.diagnostics.enable)
	{
		strDiagDemod += "on ";
	}
	else
	{
		strDiagDemod += "off";
	}

	// mod diag
	string strDiagMod = " ModDiag:";
	if (statusReport.mod.diagnostics.enable)
	{
		strDiagMod += "on ";
	}
	else
	{
		strDiagMod += "off";
	}

	// display
 	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Status received: " << std::dec << statusReport.demod.counters.packetsGood << " GoodDemodPkts   " << statusReport.demod.counters.packetsBad <<
		" BadDemodPkts   " << statusReport.demod.counters.packetsDropped << " DroppedDemodPkts   " << statusReport.mod.counters.packets << " ModPkts" <<
		strDiagDemod << strDiagMod << " OutLvl: " << statusReport.mod.controls.outputLevel << std::endl;
}


/// <summary>	Diagnostics enable. </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
/// <param name="enable">	  	True to enable, false to disable. </param>
void DiagnosticsEnable(user_api::SvcFskModemConnectionUserApi& userConnect, bool enable)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetDiagnostics_t()) };
	user_api::CmdSetDiagnostics_t& cmdDiags = cmd.setDiagnostics;
	cmdDiags.demod.enable = enable;
	cmdDiags.mod.enable = enable;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);
}


/// <summary>
/// 	Show Active Configuration. This is an example of sending a command and waiting for the
/// 	reponse in the same thread. The reponse can also be retrieved by another thread instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowActiveConfiguration(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdGetConfiguration_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);
	while (userConnect.ReceiveResponse(resp) != 0);
	user_api::RespConfigurationReport_t configReport = resp.configurationReport;


	// display
	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Active Config: \x22" << configReport.config.Name() << "\x22  Mod BT = " << configReport.config.mod.pulseShapingBt << endl;
}


/// <summary>
/// 	Show Available Configurations. This is an example of sending a command and waiting for
/// 	the reponse in the same thread. The reponse can also be retrieved by another thread
/// 	instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowAvailableConfigurations(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response(s).
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdListConfigurations_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);

	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Available Configurations List:" << endl;
	bool done;
	do {
		while (userConnect.ReceiveResponse(resp) != 0);
		user_api::RespAvailConfigurationsListEntry_t availConfiguration = resp.availConfiguration;

		// display
		std::cout << "\tName: \x22" << availConfiguration.Name() << "\x22  Desc: " << availConfiguration.Description() << endl;

		done = availConfiguration.last;
	} while (!done);
}


/// <summary>
/// 	Show Channels for the Active Configuration. This is an example of sending a command and waiting for
/// 	the reponse in the same thread. The reponse can also be retrieved by another thread
/// 	instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowActiveChannels(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response(s).
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdListChannels_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);

	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Active Channels List:" << endl;
	bool done;
	do {
		while (userConnect.ReceiveResponse(resp) != 0);
		user_api::RespActiveChannelsListEntry_t activeChan = resp.activeChannel;

		// display
		std::cout << "\tChan Number: " << activeChan.number << "   Center Freqeuncy (kHz): " << (activeChan.centerFrequency / 1000.00) << endl;

		done = activeChan.last;
	} while (!done);
}


/// <summary>	Attach to the service. </summary>
///
/// <param name="userConnect">	[out] Ref to store the user connection. </param>
void ServiceAttach(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	while (userConnect.Attach() < 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	// continually ping the service, wait for reply.
	bool ready = false;
	user_api::SvcFskModemCommand_t cmd { user_api::SvcFskModemCommand_t(user_api::CmdPing_t()) };
	user_api::SvcFskModemResponse_t resp;
	do
	{
		userConnect.SendCommand(cmd);
		if (userConnect.ReceiveResponse(resp) == 0)
		{
			ready = (resp.Type() == user_api::ResponseType_t::Ready);
		}
	} while (!ready);
}


/// <summary>	Main entry-point for this application. </summary>
///
/// <param name="argc">	The number of command-line arguments provided. </param>
/// <param name="argv">	An array of command-line argument strings. </param>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
int main(int argc, char* argv[])
{
	user_api::SvcFskModemConnectionUserApi		userConnect;
	char										versionStr[32];
	char										connectionVersionStr[32];

	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "SvcFskModemTester v2.26" << std::endl;
	}

	// attach to the modem service
	ServiceAttach(userConnect);

	// Show versions
	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "Service Vers: v" << userConnect.GetVersion() << "  Connection Vers: v" << userConnect.GetConnectionVersion() << endl;
	}

	// Show the active config
	ShowActiveConfiguration(userConnect);

	// Show all available configurations
	ShowAvailableConfigurations(userConnect);

	// Set configuration. Note, there is no need to set the configuration if
	// the currently active (ie default) is good.
 	SetConfiguration(userConnect, "CFG_GFSK_40KBPS");
		
	// Show the active config
	ShowActiveConfiguration(userConnect);

	// List the channels for the active configuration
	ShowActiveChannels(userConnect);

	// Show status
	ShowStatus(userConnect);

	// Turn on diagnostics
	DiagnosticsEnable(userConnect, true);

	// Show status again
	ShowStatus(userConnect);

	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "Run..." << endl;
	}

	RunTest(userConnect);

	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "Done." << endl;
	}

	// Turn off diagnostics
	DiagnosticsEnable(userConnect, false);

	return 0;
}
