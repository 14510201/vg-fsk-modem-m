# List of subdirectories to iterate over


.PHONY: all clean SvcFskModem SvcFskModemTester SvcFskModemUtil

all: SvcFskModem SvcFskModemTester SvcFskModemUtil

DEST_DIR := $(HOME)/include


SvcFskModem:
	@mkdir -p $(HOME)/lib
	@echo "Building in $@"
	cd $@ && mkdir -p build && cd build && cmake .. && make && make install
	@cp $@/build/$@ ~/bin

SvcFskModemTester:
	@echo "Building in $@"
	cd $@ && mkdir -p build && cd build && cmake .. && make 
	@cp $@/build/$@ ~/bin

SvcFskModemUtil:
	@echo "Building in $@"
	cd $@ && mkdir -p build && cd build && cmake .. && make && make install
	@cp $@/build/$@ ~/bin

clean:
	echo "Cleaning in SvcFskModemTester"
	cd SvcFskModemTester/build && make clean
	rm -rf SvcFskModemTester/build
	echo "Cleaning in SvcFskModem"
	cd SvcFskModem/build && make clean
	rm -rf SvcFskModem/build
