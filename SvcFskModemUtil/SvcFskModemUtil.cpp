﻿#include <cstring>
#include <iostream> 
#include <thread>
#include <mutex>
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include "SvcFskModemConnectionUserApi.hpp"
#include "SvcFskModemUtil.hpp"
#include "PspOobPcapng.hpp"
#include <boost/program_options.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace svc_fsk_modem;
using namespace svc_fsk_modem_tester;

namespace po = boost::program_options;

/// <summary>	Globals. </summary>
bool			stopTestThreadListenForDemodPkts;
bool			stopTestThreadListenForModIq;
std::mutex		mutCout;

/// <summary>	Set the configuration. This is an example of setting a configuration. </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
/// <param name="cfgName">	  	Name of the configuration. </param>
void SetConfiguration(user_api::SvcFskModemConnectionUserApi& userConnect, std::string cfgName)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetConfiguration_t()) };
	user_api::CmdSetConfiguration_t& cmdConfig = cmd.setConfiguration;
	cmdConfig.config.Name(cfgName);
	cmdConfig.config.mod.pulseShapingBt = 1.0;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);
}

/// <summary>
/// 	Show Status. This is an example of sending a command and waiting for the reponse in the
/// 	same thread. The reponse can also be retrieved by another thread instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
void ShowStatus(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdGetStatus_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);
	while (userConnect.ReceiveResponse(resp) != 0);
	user_api::RespStatusReport_t& statusReport = resp.statusReport;


	// demod diag
	string strDiagDemod = " DemodDiag:";
	if (statusReport.demod.diagnostics.enable)
	{
		strDiagDemod += "on ";
	}
	else
	{
		strDiagDemod += "off";
	}

	// mod diag
	string strDiagMod = " ModDiag:";
	if (statusReport.mod.diagnostics.enable)
	{
		strDiagMod += "on ";
	}
	else
	{
		strDiagMod += "off";
	}

	// display
 	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Status received: " << std::dec << statusReport.demod.counters.packetsGood << " GoodDemodPkts   " << statusReport.demod.counters.packetsBad <<
		" BadDemodPkts   " << statusReport.demod.counters.packetsDropped << " DroppedDemodPkts   " << statusReport.mod.counters.packets << " ModPkts" <<
		strDiagDemod << strDiagMod << " OutLvl: " << statusReport.mod.controls.outputLevel << std::endl;
}


/// <summary>	Diagnostics enable. </summary>
///
/// <param name="userConnect">	[in] Ref to the user connection. </param>
/// <param name="enable">	  	True to enable, false to disable. </param>
void DiagnosticsEnable(user_api::SvcFskModemConnectionUserApi& userConnect, bool enable)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetDiagnostics_t()) };
	user_api::CmdSetDiagnostics_t& cmdDiags = cmd.setDiagnostics;
	cmdDiags.demod.enable = enable;
	cmdDiags.mod.enable = enable;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);
	if(enable){
		std::cout << "Diagnostics enabled" << std::endl;
	}else {
		std::cout << "Diagnostics disabled" << std::endl;
	}
}


/// <summary>
/// 	Show Active Configuration. This is an example of sending a command and waiting for the
/// 	reponse in the same thread. The reponse can also be retrieved by another thread instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowActiveConfiguration(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdGetConfiguration_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);
	while (userConnect.ReceiveResponse(resp) != 0);
	user_api::RespConfigurationReport_t configReport = resp.configurationReport;


	// display
	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Active Config: \x22" << configReport.config.Name() << "\x22  Mod BT = " << configReport.config.mod.pulseShapingBt << endl;
}


/// <summary>
/// 	Show Available Configurations. This is an example of sending a command and waiting for
/// 	the reponse in the same thread. The reponse can also be retrieved by another thread
/// 	instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowAvailableConfigurations(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response(s).
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdListConfigurations_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);

	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Available Configurations List:" << endl;
	bool done;
	do {
		while (userConnect.ReceiveResponse(resp) != 0);
		user_api::RespAvailConfigurationsListEntry_t availConfiguration = resp.availConfiguration;

		// display
		std::cout << "\tName: \x22" << availConfiguration.Name() << "\x22  Desc: " << availConfiguration.Description() << endl;

		done = availConfiguration.last;
	} while (!done);
}


/// <summary>
/// 	Show Channels for the Active Configuration. This is an example of sending a command and waiting for
/// 	the reponse in the same thread. The reponse can also be retrieved by another thread
/// 	instead.
/// </summary>
///
/// <param name="userConnect">	[in] Ref to store the user connection. </param>
void ShowActiveChannels(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	// send cmd, await response(s).
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdListChannels_t()) };
	user_api::SvcFskModemResponse_t resp;
	userConnect.SendCommand(cmd);

	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "Active Channels List:" << endl;
	bool done;
	do {
		while (userConnect.ReceiveResponse(resp) != 0);
		user_api::RespActiveChannelsListEntry_t activeChan = resp.activeChannel;

		// display
		std::cout << "\tChan Number: " << activeChan.number << "   Center Freqeuncy (kHz): " << (activeChan.centerFrequency / 1000.00) << endl;

		done = activeChan.last;
	} while (!done);
}

void SetOutputLevel(user_api::SvcFskModemConnectionUserApi& userConnect, float level)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetControls_t()) };
	user_api::CmdSetControls_t& cmdCtrls = cmd.setControls;
	cmdCtrls.mod.outputLevel = level;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);
	std::cout << "Output level set to " << level << std::endl;
}

/// <summary>	Attach to the service. </summary>
///
/// <param name="userConnect">	[out] Ref to store the user connection. </param>
void ServiceAttach(user_api::SvcFskModemConnectionUserApi& userConnect)
{
	while (userConnect.Attach() < 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	// continually ping the service, wait for reply.
	bool ready = false;
	user_api::SvcFskModemCommand_t cmd { user_api::SvcFskModemCommand_t(user_api::CmdPing_t()) };
	user_api::SvcFskModemResponse_t resp;
	do
	{
		userConnect.SendCommand(cmd);
		if (userConnect.ReceiveResponse(resp) == 0)
		{
			ready = (resp.Type() == user_api::ResponseType_t::Ready);
		}
	} while (!ready);
}



/// <summary>	Main entry-point for this application. </summary>
///
/// <param name="argc">	The number of command-line arguments provided. </param>
/// <param name="argv">	An array of command-line argument strings. </param>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
int main(int argc, char* argv[])
{
	user_api::SvcFskModemConnectionUserApi		userConnect;
	char										versionStr[32];
	char										connectionVersionStr[32];

	po::options_description desc("Options");
    desc.add_options()
			("conf,c", po::value<std::string>(), "Set the configuration")
			("active,a", "Show active configuration")
            ("stats,s", "Print the stats")
			("outputlevel,o", po::value<float>(), "Set the output level")
			("conflist,l", "List the available configurations")
			("diag,d", po::value<bool>(), "Enable/Disable Diagnostics")
			("chan,C", "Show active channels");

	po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

	// attach to the modem service
	ServiceAttach(userConnect);

	// Show versions
	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "Service Vers: v" << userConnect.GetVersion() << "  Connection Vers: v" << userConnect.GetConnectionVersion() << endl;
	}

	// Show the active config
	if(vm.count("active")) {
		ShowActiveConfiguration(userConnect);
		return 0;
	}

	// Show the stats
	if(vm.count("stats")) {
		ShowStatus(userConnect);
		return 0;
	}

	if(vm.count("conflist")) {
		ShowAvailableConfigurations(userConnect);
		return 0;
	}

	// Show diagnostics
	if(vm.count("diag")) {
		DiagnosticsEnable(userConnect, vm["diag"].as<bool>());
		return 0;
	}

	// Show active channels
	if(vm.count("chan")) {
		ShowActiveChannels(userConnect);
		return 0;
	}
	
	if(vm.count("conf")) {
		SetConfiguration(userConnect, vm["conf"].as<std::string>());
		return 0;
	}

	if(vm.count("outputlevel")) {
		SetOutputLevel(userConnect, vm["outputlevel"].as<float>());
		return 0;
	}

	std::cout << desc << "\n";
	return 1;

}
