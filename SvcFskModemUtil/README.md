# SvcFskModemUtil

A simple utility to process pcapng files. It can either perform modulation or demodulation. Example

    # test demod of FSK data
    SvcFskModemUtil -f ~/a1.pcapng -s 112.97.0.38 -d 112.97.0.6

    # test that the FSK modulation by the NDF agent is connect
    # the NDF Agent generates the FSK data and sends it to multicast IP
    SvcFskModemUtil -f ~/test_ndf_endian.pcapng -s 112.97.0.42 -d 239.1.2.3

