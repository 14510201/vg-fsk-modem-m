﻿#pragma once
#include <cstdlib>
#include <cstdio>

namespace svc_fsk_modem_tester
{
	/// <summary>	Error exit with error console print. </summary>
	///
	/// <param name="msg">	[in,out] If non-null, the message. </param>
	inline void ErrorExit(char* msg) { perror(msg); exit(EXIT_FAILURE); }
}