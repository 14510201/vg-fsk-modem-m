// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//     _  __ __                   _____       __      __  _                    :
//    | |/ // /__  _________ _   / ___/____  / /_  __/ /_(_)___  ____  _____   :
//    |   // / _ \/ ___/ __ `/   \__ \/ __ \/ / / / / __/ / __ \/ __ \/ ___/   :
//   /   |/ /  __/ /  / /_/ /   ___/ / /_/ / / /_/ / /_/ / /_/ / / / (__  )    :
//  /_/|_/_/\___/_/   \__,_/   /____/\____/_/\__,_/\__/_/\____/_/ /_/____/     :
//                                                                             :
//   (c) Copyright 2024 Xlera Solutions, LLC.  All rights reserved.            :
//                                                                             :
// This file contains confidential and proprietary information of Xlera        :
// Solutions, LLC and is protected under U.S. and international copyright and  :
// other intellectual property laws.                                           :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : Xlera Solutions                                        :
// Version            : 1.0                                                    :
// Application        : N/A                                                    :
// Filename           : PspOobPcapng.hpp                                       :
// Date Created       : 2024 JAN 23                                            :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
//      Header file
//
// -----------------------------------------------------------------------------

#pragma once

#include <string>

namespace svc_fsk_modem_tester
{
	/// <summary>
	/// 	Class: PspOobPcapng. Parse a PCAPNG file and extract the PSP OOB payload.
	/// </summary>
	class PspOobPcapng
	{
	public:

		/// <summary>	Structure to define a sequence gap. </summary>
		struct SequenceGap_t
		{
			SequenceGap_t(int packetLocation, uint16_t seqNumber, uint16_t gap) :
				packetLocation(packetLocation), seqNumber(seqNumber), gap(gap) {}
			int					packetLocation;
			uint16_t			seqNumber;
			uint16_t			gap;
		};


	protected:
		const int					PcapngSectionHeaderBlockType = 0x0a0d0d0a;
		const int					PcapngByteOrderMagicLittleEndian = 0x1a2b3c4d;
		const int					PcapngPacketBlockType = 6;
		const uint8_t				L2TpProtocolId = 0x73;
		const int					MaxIqFrameWords = 512;
		const int					PacketHeaderBytes = 38;
		const int					HeaderOffsetProtocolType = 23;
		const int					HeaderOffsetSrcIp = 26;
		const int					HeaderOffsetDestIp = 30;

		std::FILE*					pFile;
		uint32_t					srcIp;
		uint32_t					destIp;
		uint8_t						protocolId;
		uint32_t*					pWorkingBufr;
		bool						machineIsLittleEndian;
		bool						fileIsLittleEndian;
		std::vector<SequenceGap_t>	seqGaps;
		uint16_t					lastSeqNumber;
		int							pktLoc;


	public:
		PspOobPcapng(void) = delete;
		PspOobPcapng(std::string filename, std::string srcIpStr, std::string destIpStr);
		~PspOobPcapng(void);

		PspOobPcapng&				operator += (const int inc);
		std::vector<uint32_t>		operator () (void);

		bool						SequenceGapsDetected(void) { return (seqGaps.size() != 0); }
		std::vector<SequenceGap_t>& SequenceGaps(void) { return seqGaps; }

	protected:
		bool						FilterMatchPacket(uint32_t* pRemainingCapturedPktBytes, int* pRemainingBlockBytes);
		std::vector<uint32_t>		GetNextPacket(void);
		void						SkipPackets(int count);
		void						ParseSectionHeaderBlock(void);
		inline void					CorrectPcapngFormatEndianness(uint32_t* pValue);
		inline void					CorrectPcapngFormatEndianness(uint32_t* pValues, int count);
		inline void					CorrectPspOobEndianness(uint32_t* pValue);
		inline void					CorrectPspOobEndianness(uint32_t* pValues, int count);
	};
}