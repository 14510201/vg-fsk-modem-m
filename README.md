# SvcFskModem

    The SvcFskModem is a service and corresponding library that is used to perform IP<>FSK modem function.
    The release source files are in dist folder. These zip files are extracted in

        SvcFskModem
        SvcFskModemTester

    respectively. We use cmake to build the binaries using the following command in these folders:

    sudo apt install libboost-all-dev
    cmake -S . -B build
    cmake --build build

    To run the test

    ./SvcFskModemTester -f /home/atllab/ndr_join_only_3_pkts.pcapng -s 112.97.0.38 -d 112.97.0.6
    

## Pre-requisite

    Install boost libraries

    https://docs.gitlab.com/16.5/ee/user/packages/generic_packages/



## for the c++ client on Mac

    brew install pcapplusplus
    brew install libpcap

# Release


## v1.01.00
    At the links below is a full release of the service (v1.01.00) and associated Tester (v2.00). Be sure to rebuild the lib.

    Release notes:
    This has both demodulation and modulation running.  
    User side is now a Cmd/Resp API. The two header files changed names to represent this.
    Note that the interface to send and receive packets & IQ frames is the same.
    Support for getting a list of all configurations (1 currently)
    Support for setting a configuration
    Still supports getting status
    Setting diagnostics includes modulation now as well. There is a diagMod.xs file created for modulation under /tmp.
    The tester has been cleaned up and demonstrates all the above operations. It will attach, report the active configuration, list all configurations, demod packet from pcapng, send a handful of packets to modulate.